-- Record Gungeon

require "cocos.init"
require "script.Global"
require "script.Resources"
require "script.Manager.SceneMng"
require "script.Manager.WeaponMng"
require "script.Manager.ParticleMng"
require "script.Manager.StageMng"
require "script.Manager.UserDataMng"
require "script.Manager.MonsterMng"
require "script.Manager.ProjectileMng"
require "script.Manager.SoundMng"
require "script.UI.InGame.InGameUI"
require "script.UI.BaseWeaponSlot"
require "script.Monster.BT"
require "script.UI.PopUp"

require "script.Table.WeaponTable"
require "script.Table.StageTable"
require "script.Table.MonsterTable"


-- cclog
cclog = function(...)
    print(string.format(...))
end

-- for CCLuaEngine traceback
function __G__TRACKBACK__(msg)
    cclog("----------------------------------------")
    cclog("LUA ERROR: " .. tostring(msg) .. "\n")
    cclog(debug.traceback())
    cclog("----------------------------------------")
end

local function initGLView()
    local director = cc.Director:getInstance()
    local glView = director:getOpenGLView()
    if nil == glView then
        glView = cc.GLViewImpl:create("Record Gungeon")
        director:setOpenGLView(glView)
    end

    director:setOpenGLView(glView)
    --glView:setFrameSize(360,640)
    glView:setDesignResolutionSize(720, 1280, cc.ResolutionPolicy.SHOW_ALL)

    visibleSize = cc.Director:getInstance():getVisibleSize()
    origin = cc.Director:getInstance():getVisibleOrigin()

    --turn on display FPS
    --director:setDisplayStats(true)
    director:setAnimationInterval(1.0 / 60)
end

-- 앱 구동 main함수
local function main()
    -- avoid memory leak
    collectgarbage("setpause", 100)
    collectgarbage("setstepmul", 5000)

    initGLView()

    UserDataMng:init()
    SoundMng:init()
    popUpUI:init()
    SceneMng:changeScene(eScene.Title)
end

xpcall(main, __G__TRACKBACK__)