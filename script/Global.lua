ePlayerState = {
    Idle        = "Idle",
    Walk        = "Walk",
    MainAttack  = "MainAttack",
    SubAttack   = "SubAttack",
    Knockback   = "Nockback",
    Death       = "Death"
}

eMonsterState = {
    Idle    = "Idle",
    Walk    = "Walk",
    Attack  = "Attack",
    Ice     = "Ice",
    Death   = "Death"
}

eProjectileID = {
    Circle = "Circle",
    Cross  = "Cross"
}

eParticleTag = {
    MainWeapon  = "MainWeapon",
    SubWeapon   = "SubWeapon",
    Blood       = "Blood",
    BossDeath   = "BossDeath"
}

eDirection = {
    Up        = "Up",
    LeftUp    = "LeftUp",
    Left      = "Left",
    LeftDown  = "LeftDown",
    Down      = "Down",
    RightDown = "RightDown",
    Right     = "Right",
    RightUp   = "RightUp"
}

eScene = {
    Title       = "Title",
    FirstStage  = "FirstStage",
    SecondStage = "SecondStage",
    Lobby       = "Lobby"
}

eStageID = {
    S1 = "S1",
    S2 = "S2"
}

function eStageID:inStage(stageID)
    for i,v in pairs(eStageID) do
        if v == stageID then
            return true
        end
    end
    return false
end


eUserDataCategory = {
    MainWeapon          = "MainWeapon",
    SubWeapon           = "SubWeapon",
    MainWeaponList      = "MainWeaponList",
    SubWeaponList       = "SubWeaponList",
    AccessibleStageList = "AccessibleStageList",
    RecordTimeList      = "RecordTimeList"
}

-- 적이 공격하는 무기 포함
eAttackCategory = {
    MainWeapon     = "MainWeapon",
    SubWeapon      = "SubWeapon",
    BossProjectile = "BossProjectile"
}

eWeaponID = {
    -- Main
    NormalBomb      = "NormalBomb",
    DynamiteStick   = "DynamiteStick",
    -- Sub
    FireBall        = "FireBall",
    IceBall         = "IceBall"
}

eMonsterID = {
    GhostBoss = "GhostBoss",
    GoatBoss  = "GoatBoss"
}

eButtonID = {
    MainAttack = 0,
    SubAttack = 1,
    GoLobby = 2,
    Retry = 3,
    ChangeWeapon = 4
}

eWeaponSlotTag = {
    InGameMain = 0,
    InGameSub = 1,
    LobbyMain  = 2,
    LobbySub  = 3
}

eLobbyLayerID = {
    Home = 0,
    Shop = 1,
    Equipment = 2,
    Setting = 3
}

-- 매개변수로 넘어온 애니메이션 인포를 통해 애니메이션을 만드는 함수
function createAnimation(info,isRepeat)
    local animation = cc.Animation:create()
    animation:setDelayPerUnit(info.delay)
    for i = 0, info.totalIndex - 1 do
        animation:addSpriteFrameWithFile(string.format("%s%d.png",info.path,i))
    end 
    local animate = cc.Animate:create(animation) 
    if isRepeat then
        return cc.RepeatForever:create(animate)
    else
        return animate
    end          
end

-- 액션 전환 함수
function changeAction(sprite,action)
    sprite:stopAllActions()
    sprite:runAction(action)
end

-- 원하는 객체를 테이블에서 지우기 위한 해당 객체 인덱스를 얻어오는 함수
function tableFind(table,value)
    for i,v in pairs(table) do
        if v == value then
            return i
        end
    end
    print("doesn't have value in table")
    return nil
end

-- 2개의 Rect가 서로 겹쳐있는지 확인하는 함수 ( 폭발범위와 대상의 충돌여부 체크 때 사용 또는 플레이어와 몬스터가 겹쳐 넉백시킬 때 사용 )
function intersectsRect(explosionRect,objectRect)
    local explosion = {top = explosionRect.y + explosionRect.height, bottom = explosionRect.y, left = explosionRect.x, right = explosionRect.x + explosionRect.width}
    local object = {top = objectRect.y + objectRect.height, bottom = objectRect.y, left = objectRect.x, right = objectRect.x + objectRect.width}

    if (object.left <= explosion.right) and (object.right >= explosion.left) and (object.bottom <= explosion.top) and (object.top >= explosion.bottom) then
        return true
    else
        return false
    end
end

-- 몬스터와 충돌되었는지 확인 함수 ( 플레이어의 subWeapon의 맨 앞부분이 총돌 몬스터의 충돌범위 안에 들어가면 true 리턴 )
function collisionCheckToMonster(currentX,currentY)
    for i,v in pairs(MonsterMng.usingMonsterPool) do
        local cX,cY = v.sprite:getPosition()
        if v.pivotY ~= nil then
            cY = cY - v.pivotY
        end
        if (currentX > cX-v.subDamagedSizeX/2) and (currentX < cX+v.subDamagedSizeX/2) and 
            (currentY > cY-v.subDamagedSizeY/2) and (currentY < cY+v.subDamagedSizeY/2) then
            return true
        end
    end
    return false
end

-- 셰이더 변경 함수 ( 적이 타격 당했을 때 흰색으로 변하기 위한 함수 )
function changeWhiteShader(targetSprite,duration)

    local pProgram = cc.GLProgram:create("white.vsh","white.fsh.txt")
    pProgram:bindAttribLocation(cc.ATTRIBUTE_NAME_POSITION,cc.VERTEX_ATTRIB_POSITION)
    pProgram:bindAttribLocation(cc.ATTRIBUTE_NAME_COLOR,cc.VERTEX_ATTRIB_COLOR)
    pProgram:bindAttribLocation(cc.ATTRIBUTE_NAME_TEX_COORD,cc.VERTEX_ATTRIB_FLAG_TEX_COORDS)
    pProgram:link()
    pProgram:updateUniforms()
    local originShader = targetSprite:getGLProgram()
    targetSprite:setGLProgram(pProgram)
    local delay    = cc.DelayTime:create(duration)
    local callFunc = cc.CallFunc:create(
        function ()
            targetSprite:setGLProgram(originShader)
        end
    )
    local sequence = cc.Sequence:create(delay,callFunc)
    targetSprite:runAction(sequence)
end