require "script.Monster.BaseMonster"
require "script.Monster.GhostBoss"
require "script.Monster.GoatBoss"

MonsterMngClass = { 
    monsterPool      = {}, 
    usingMonsterPool = {}
}

-- 생성 함수
function MonsterMngClass:new(o)
    o = o or {}
    setmetatable(o,self)
    self.__index = self
    o:removeAll()
    return o
end

-- 몬스터 인스턴스를 얻어오는 함수
function MonsterMngClass:getMonsterInstance(monsterID)
    if monsterID == eMonsterID.GhostBoss then
        return GhostBossClass:new()
    elseif monsterID == eMonsterID.GoatBoss then
        return GoatBossClass:new()
    end
end

-- 몬스터 풀링 함수
function MonsterMngClass:addMonster(monsterID)
    local poolingCount = 5

    if monsterID == nil then
        do return end
    else
        if self.monsterID == nil then
            self.monsterID = {}    
        end
        
        for i = 1, poolingCount do
            local monsterIns = self:getMonsterInstance(monsterID)
            monsterIns.sprite:setVisible(false)            
            table.insert(self.monsterPool,monsterIns)
            StageMng.layer:addChild(monsterIns.sprite,9)
        end
    end
end

-- 몬스터 얻어오는 함수
function MonsterMngClass:getMonster(monsterID,x,y)
    for i,v in pairs(self.monsterPool) do
        if v.sprite:isVisible() == false then
            v:enrollAI()
            table.insert(self.usingMonsterPool,v)
            v.sprite:setPosition(x,y)
            v.sprite:setVisible(true)
            return v
        end
    end
    print("invalid")
end

-- 초기화 함수
function MonsterMngClass:removeAll()
    self.monsterPool      = {}
    self.usingMonsterPool = {}
end