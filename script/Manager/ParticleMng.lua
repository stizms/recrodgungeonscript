require "script.Particle.ParticleObject"

ParticleMngClass = {
    particlePool = {
        MainWeapon = {},
        SubWeapon = {},
        BossDeath = {},
        Blood = {}
    },
    usingParticlePool = {}
}

-- 파티클 매니저 생성 함수
function ParticleMngClass:new(o)
    o = o or {}
    setmetatable(o,self)
    self.__index = self
    o:removeAll()
    return o
end

-- 파티클 풀링
function ParticleMngClass:addParticle(tag,particleInfo)
    local poolingCount = 10
    if particleInfo == nil then
        do return end
    else
        for i = 1, poolingCount do
            local temp = ParticleObjectClass:new(particleInfo)
            temp.sprite:setVisible(false)
            table.insert(self.particlePool[tag],temp)
            StageMng.layer:addChild(temp.sprite,50)
        end
    end
end

-- 파티클 얻어오는 함수
function ParticleMngClass:getParticle(tag,x,y)
    for i,v in pairs(self.particlePool[tag]) do
        if v.sprite:isVisible() == false then
            table.insert(self.usingParticlePool,v)
            v.sprite:setPosition(x,y)
            v.sprite:setVisible(true)
            v.sprite:runAction(v.bodyAction)
            do return end
        end
    end

    -- 풀링한 객체가 모두 사용 중인 경우 더 만들어서 작동해준다.
    local index = #self.particlePool[tag] + 1
    self:addParticle(tag,self.particlePool[tag][1].particleInfo)
    local temp = self.particlePool[tag][index]
    table.insert(self.usingParticlePool,temp)
    temp.sprite:setPosition(x,y)
    temp.sprite:runAction(temp.bodyAction)
    temp.sprite:setVisible(true)
end

-- 초기화 함수
function ParticleMngClass:removeAll()
    self.particlePool.MainWeapon     = {}
    self.particlePool.SubWeapon      = {}
    self.particlePool.BossDeath      = {}
    self.particlePool.Blood          = {}
    self.usingParticlePool           = {}
end