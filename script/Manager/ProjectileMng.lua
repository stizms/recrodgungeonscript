require "script.Projectile.BaseProjectile"
require "script.Projectile.CircleProjectile"
require "script.Projectile.CrossProjectile"

ProjectileMngClass = {
    projectilePool      = {},
    usingProjectilePool = {}
}

-- 생성 함수
function ProjectileMngClass:new(o)
    o = o or {}
    setmetatable(o,self)
    self.__index = self
    o:removeAll()
    return o
end

-- 투사체의 인스턴스를 얻어오는 함수
function ProjectileMngClass:getProjectileInstance(projectileID)
    if projectileID == eProjectileID.Circle then
        return CircleProjectile:new()
    elseif projectileID == eProjectileID.Cross then
        return CrossProjectile:new()
    end
end

-- 투사체 풀링
function ProjectileMngClass:addProjectile(projectileID)
    local poolingCount = 100

    if projectileID == nil then
        do return end
    else
        local temp = nil
        for i=1,poolingCount do
            temp = self:getProjectileInstance(projectileID)
            temp.sprite:setVisible(false)
            table.insert(self.projectilePool,temp)
            StageMng.layer:addChild(temp.sprite,10)
        end
    end
end

-- 투사체 발사 함수 (시작 위치, 진행 벡터, 노멀라이즈 여부) [ 노멀라이즈 한 것과 안한 것의 모양이 다르기에 추가해놓음 ]
function ProjectileMngClass:getProjectile(startX,startY,dirX,dirY,doNormalize)
    if doNormalize == true then
        -- 벡터 정규화
        distance = math.sqrt((dirX*dirX) + (dirY*dirY))
        dirX     = dirX/distance
        dirY     = dirY/distance
    end
    
    for i,v in pairs(self.projectilePool) do
        if v.sprite:isVisible() == false then
            table.insert(self.usingProjectilePool,v)
            v.sprite:setPosition(startX,startY)
            v.dirVec.x = dirX
            v.dirVec.y = dirY
            v.sprite:runAction(v.bodyAction)
            v.sprite:setVisible(true)
            do return end
        end
    end

    -- 현재 풀링한 투사체를 모두 사용 중인 경우 추가적으로 생산
    local index = #self.projectilePool + 1
    self:addProjectile(StageMng.boss.projectile)
    local temp = self.projectilePool[index]
    table.insert(self.usingProjectilePool,temp)
    temp.sprite:setPosition(startX,startY)
    temp.dirVec.x = dirX
    temp.dirVec.y = dirY
    temp.sprite:runAction(temp.bodyAction)
    temp.sprite:setVisible(true)
end

-- 초기화 함수
function ProjectileMngClass:removeAll()
    self.projectilePool      = {}
    self.usingProjectilePool = {}
end