require "script.Scene.TitleScene"
require "script.Scene.StageScene"
require "script.Scene.LobbyScene"

SceneMng = {
    -- 모든 씬을 시작하는 함수를 담은 테이블
   allScene = {
        Title       =   { create_func = createTitleScene      },
        FirstStage  =   { create_func = createStageScene      },
        SecondStage =   { create_func = createStageScene      },
        Lobby       =   { create_func = createLobbyScene      }
   }
}

-- 씬 전환 함수
function SceneMng:changeScene(sceneName)
    cc.Director:getInstance():purgeCachedData()

    local stageID = nil

    if sceneName == eScene.FirstStage then
        stageID = eStageID.S1
    elseif sceneName == eScene.SecondStage then
        stageID = eStageID.S2
    end

    local scene = self.allScene[sceneName].create_func(stageID)
    if scene == nil then
        print("doesn't have that scene in allSceneList")
        do return end
    end

    if cc.Director:getInstance():getRunningScene() then
        cc.Director:getInstance():replaceScene(scene)
    else
        cc.Director:getInstance():runWithScene(scene)
    end
end

