SoundMng = {
    sound = nil, 
    currentBackgroundID = nil, 
    currentBGMVolume    = 100, 
    currentEffectVolume = 100
}

eBGMSoundID = {
    TitleBGM    = "TitleBGM.mp3",
    LobbyBGM    = "LobbyBGM.mp3",
    InGameBGM   = "InGameBGM.mp3",
    ClearUIBGM  = "ClearUIBGM.mp3",
}

eEffectSoundID = {
    BtnEffect           = "BtnClickSound.mp3",
    PopUpVisualize      = "PopUpVisualizeEffect.mp3",
    PopUpNonVisualize   = "PopUpNonVisualizeEffect.mp3",
    MainAttackEffect    = "MainAttackEffect.mp3",
    SubAttackGazeEffect = "SubAttackGazeEffect.mp3",
    SubAttackBombEffect = "SubAttackBombEffect.mp3",
    IntroEffect         = "IntroEffect.mp3",
    TitleClickEffect    = "TitleClickEffect.mp3",
    DeadEffect          = "DeadEffect.mp3",
    DeadUIEffect        = "DeadUIEffect.mp3",
    FootWalkEffect      = "FootWalkEffect.mp3",
    BossDeathEffect     = "BossDeathEffect.mp3",
    DynamiteBombEffect  = "DynamiteBombEffect.mp3",
    NormalBombEffect    = "NormalBombEffect.mp3",
    IceBallEffect       = "IceBallEffect.mp3"
}

function SoundMng:init()
    self.sound = cc.SimpleAudioEngine:getInstance()
    for i,v in pairs(eBGMSoundID) do
        self.sound:preloadMusic(v)
    end
    for i,v in pairs(eEffectSoundID) do
        self.sound:preloadEffect(v)
    end
end

function SoundMng:playBGM(soundID)
    if self.currentBackGroundID ~= nil then
        self.sound:stopMusic(self.currentBackGroundID)
    end
    self.currentBackgroundID = self.sound:playMusic(soundID, true)
end

function SoundMng:playEffect(soundID,loop)
    if loop == nil then
        loop = false
    end
    return self.sound:playEffect(soundID,loop)
end

function SoundMng:stopBGM()
    self.sound:stopMusic(self.currentBackGroundID)
    self.currentBackGroundID = nil
end

function SoundMng:stopEffect(soundID)
    self.sound:stopEffect(soundID)
end