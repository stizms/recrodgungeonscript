require "script.Player.Player"
require "script.Map.Map"

-- isGaming은 Update등을 관리하는 변수 
StageMngClass = {
    layer        = nil,     -- 스테이지 메인 레이어
    currentStage = nil,     -- 현재 스테이지
    map          = nil,     -- 현재 맵 객체
    player       = nil,     -- 플레이어 객체
    boss         = nil,     -- 보스 객체
    isGaming     = false,   -- 게임 중인 것의 여부 ( 터치 동작 제어 )
    elapsedTime  = 0,       -- 게임 경과 시간 ( 몇초만에 던전을 클리어하느지 기록 )
    rewardGold   = 0,       -- 현재 스테이지 보상 정보
    
    timeCheckFunc = nil,    -- 타임 체크 함수 timeCheck 함수를 담아 둠 
    shakeAction   = nil,    
    shakeDuration = 0,      -- 진동 기간
    isShaking     = false,  -- 진동 중인지 여부

    shakeSchedule       = nil, -- 진동 스케줄
    stageSchedule       = nil, -- 업데이트 스케줄
    stageStartSchedule  = nil, -- 시작 애니메이션 스케줄
    timeCheckSchedule   = nil  -- 타임 체크 스케줄
}

-- 생성함수
function StageMngClass:new(layer,o)
    o = o or {}
    setmetatable(o,self)
    self.__index = self
    if layer == nil then
        print("please insert stageLayer.")
        return nil
    end
    o:init(layer)
    return o
end

function StageMngClass:init(layer)
    self.layer = layer
    self.timeCheckFunc = timeCheck
end

-- 맵 생성 함수
function StageMngClass:createMap(stage)
    self.map = MapClass:new(stage)
    return self.map.tileMap
end

-- 플레이어 생성 함수
function StageMngClass:createPlayer(pX,pY,mainWeapon,subWeapon)
    self.player = PlayerClass:new(self.map,mainWeapon,subWeapon)
    self.player.sprite:setPosition(pX,pY)
    -- 공격 게이지 등록
    self.player.attackGaze.gazeLayer:setPosition(pX,pY - (self.player.sprite:getContentSize().height/2 + 10))
    return self.player.sprite,self.player.attackGaze.gazeLayer
end

-- 스테이지 구성요소 전체 세팅 함수
function StageMngClass:settingStage(stage,mainWeapon,subWeapon)
    -- 맵 생성 ( 스테이지 데이터 받는 과정 )
    self.currentStage = stage
    self.rewardGold   = stageTb:getStageInfo(self.currentStage).rewardGold
    self.layer:addChild(self:createMap(self.currentStage),0)
    self:objectPooling(mainWeapon,subWeapon)

    -- 플레이어 생성
    local playerSprite,gazeLayer = self:createPlayer(tonumber(self.map.spawnPoint.x)*4.5,tonumber(self.map.spawnPoint.y)*4,mainWeapon,subWeapon)
    self.layer:addChild(playerSprite,10)
    self.layer:addChild(gazeLayer,10)
    -- 보스 생성
    self.boss = MonsterMng:getMonster(self.map.monsterID,tonumber(self.map.bossSpawnPoint.x)*4.5,tonumber(self.map.bossSpawnPoint.y)*4)
    self.boss:actionIdle()
end

-- 오브젝트 풀링 함수
function StageMngClass:objectPooling(mainWeapon,subWeapon)
    WeaponMng:addWeapon(mainWeapon)
    WeaponMng:addWeapon(subWeapon)
    ParticleMng:addParticle(eParticleTag.MainWeapon,mainWeapon.particleInfo)
    ParticleMng:addParticle(eParticleTag.SubWeapon, subWeapon.particleInfo)
    ParticleMng:addParticle(eParticleTag.BossDeath, { delay = 0.015, particleDelay = 0.5, path = "bossDeathParticle_",  totalIndex = 29})
    ParticleMng:addParticle(eParticleTag.Blood,     { delay = 0.1,   particleDelay = 0.6, path = "Blood_",              totalIndex = 6})
    MonsterMng:addMonster(self.map.monsterID)

    ProjectileMng:addProjectile(MonsterMng:getMonsterInstance(self.map.monsterID).projectile)
end

-- 스테이지 클리어 함수
function StageMngClass:clearStage()
    inGameUI.clearUI:visualizeUI()
    stageInfo = stageTb:getStageInfo(self.currentStage)
    if UserDataMng:isAccessibleStage(stageInfo.nextStage) == false then
        table.insert(UserDataMng.accessibleStageList,stageInfo.nextStage)
    end
    if UserDataMng.recordTimeList[self.currentStage] ~= nil then
        if tonumber(UserDataMng.recordTimeList[self.currentStage]) > self.elapsedTime then
            inGameUI.clearUI:visualizeNewRecord()
            UserDataMng.recordTimeList[self.currentStage] = self.elapsedTime
        end
    else
        inGameUI.clearUI:visualizeNewRecord()
        UserDataMng.recordTimeList[self.currentStage] = self.elapsedTime
    end
    UserDataMng:saveUserData()
end

-- 던전 경과 시간 누적 스케줄함수
function timeCheck(dt)
    StageMng.elapsedTime = StageMng.elapsedTime + dt
    if StageMng.boss:isAlive() == false or StageMng.player.state == ePlayerState.Death then
        cc.Director:getInstance():getScheduler():unscheduleScriptEntry(StageMng.timeCheckSchedule)
    end
end

-- 진동 기간 체크 스케줄함수
function shakeDurationCheck(dt)
    StageMng.shakeDuration = StageMng.shakeDuration - dt
    if StageMng.shakeDuration < 0 then
        StageMng.layer:stopAction(StageMng.shakeAction)
        StageMng.isShaking = false
        cc.Director:getInstance():getScheduler():unscheduleScriptEntry(StageMng.shakeSchedule)
        StageMng.layer:setPosition(0,0)
        StageMng.shakeSchedule = nil
    end
end

-- 진동 함수
function StageMngClass:shake(amplitude,duration)
    if self.isShaking ~= true then
        local moveBy1       = cc.MoveBy:create(0.03,cc.p(amplitude,0))
        local moveBy2       = cc.MoveBy:create(0.03,cc.p(-amplitude,0))
        local moveBy3       = cc.MoveBy:create(0.03,cc.p(0,amplitude))
        local moveBy4       = cc.MoveBy:create(0.03,cc.p(0,-amplitude))
        local sequence1     = cc.Sequence:create(moveBy1,moveBy2,moveBy3,moveBy4)
        self.shakeAction    = cc.RepeatForever:create(sequence1)
        self.shakeDuration  = duration
        self.isShaking      = true
        self.layer:runAction(self.shakeAction)
        self.shakeSchedule  = cc.Director:getInstance():getScheduler():scheduleScriptFunc(shakeDurationCheck,0,false)
    end
end