UserDataMng = {
    currentMainWeapon   = nil,
    currentSubWeapon    = nil,
    haveMainWeaponList  = {},
    haveSubWeaponList   = {},
    accessibleStageList = {},
    recordTimeList      = {}
}

function UserDataMng:init()
    self:readUserData()
end

-- 접근 가능한 스테이지인지 확인 함수
function UserDataMng:isAccessibleStage(stageID)
    for i,v in pairs(self.accessibleStageList) do
        if v == stageID then
            return true
        end
    end
    return false
end


-- 유저데이터 불러오기
function UserDataMng:readUserData()
    -- 어떤 기기에서든 동일한 폴더 영역 가져오기
    local path = cc.FileUtils:getInstance():getWritablePath() .. "userData.csv"

    -- 테스트용 데이터 삭제 코드 ( 매 게임 새로운 데이터를 만듬 )
    --cc.FileUtils:getInstance():removeFile(path)

    local dataFile = cc.FileUtils:getInstance():getStringFromFile(path)

    -- 불러들어온 데이터가 없다면
    if dataFile == "" then
        -- 첫 시작 세팅
        self.currentMainWeapon   = eWeaponID.NormalBomb
        self.currentSubWeapon    = eWeaponID.FireBall
        self.haveMainWeaponList  = { 
            eWeaponID.NormalBomb, eWeaponID.DynamiteStick
        }
        self.haveSubWeaponList   = {
            eWeaponID.FireBall, eWeaponID.IceBall
        }
        self.accessibleStageList = {
            eStageID.S1
        }
        self.recordTimeList      = {}

        self:saveUserData()
    else
        local dataCategory = nil
        local nowRecordStage = nil
        -- 토큰 단위로 분할
        local splitLines = {}
        for line in string.gmatch(dataFile,"[^,\n]+")do
            table.insert(splitLines,line)
        end

        for i,v in pairs(splitLines) do
            print(v)
        end
        
        -- 카테고리에 맞게 데이터 적재
        for i,v in pairs(splitLines) do
            if v == eUserDataCategory.MainWeapon then
                dataCategory = eUserDataCategory.MainWeapon
            elseif v == eUserDataCategory.SubWeapon then
                dataCategory = eUserDataCategory.SubWeapon
            elseif v == eUserDataCategory.MainWeaponList then
                dataCategory = eUserDataCategory.MainWeaponList
            elseif v == eUserDataCategory.SubWeaponList then
                dataCategory = eUserDataCategory.SubWeaponList
            elseif v == eUserDataCategory.AccessibleStageList then
                dataCategory = eUserDataCategory.AccessibleStageList
            elseif v == eUserDataCategory.RecordTimeList then
                dataCategory = eUserDataCategory.RecordTimeList
            else
                if dataCategory == eUserDataCategory.MainWeapon then
                    self.currentMainWeapon = v
                elseif dataCategory == eUserDataCategory.SubWeapon then
                    self.currentSubWeapon  = v
                elseif dataCategory == eUserDataCategory.MainWeaponList then
                    table.insert(self.haveMainWeaponList,v)
                elseif dataCategory == eUserDataCategory.SubWeaponList then
                    table.insert(self.haveSubWeaponList,v)
                elseif dataCategory == eUserDataCategory.AccessibleStageList then
                    table.insert(self.accessibleStageList,v)
                elseif dataCategory == eUserDataCategory.RecordTimeList then
                    if eStageID:inStage(v) == true then
                        nowRecordStage = v
                    elseif nowRecordStage ~= nil then
                        self.recordTimeList[nowRecordStage] = v
                        nowRecordStage = nil
                    end
                end
            end
        end
    end
end

-- 데이터 저장 함수
function UserDataMng:saveUserData()
    local dataFile = string.format("%s,\n",eUserDataCategory.MainWeapon)
    dataFile = dataFile .. string.format("%s,\n",self.currentMainWeapon)
    dataFile = dataFile .. string.format("%s,\n",eUserDataCategory.SubWeapon)
    dataFile = dataFile .. string.format("%s,\n",self.currentSubWeapon)
    dataFile = dataFile .. string.format("%s,\n",eUserDataCategory.MainWeaponList)
    for i,v in pairs(self.haveMainWeaponList) do
        dataFile = dataFile .. string.format("%s,",v)
    end
    dataFile = dataFile .. "\n"

    dataFile = dataFile .. string.format("%s,\n",eUserDataCategory.SubWeaponList)
    for i,v in pairs(self.haveSubWeaponList) do
        dataFile = dataFile .. string.format("%s,",v)
    end
    dataFile = dataFile .. "\n"

    dataFile = dataFile .. string.format("%s,\n",eUserDataCategory.AccessibleStageList)
    for i,v in pairs(self.accessibleStageList) do
        dataFile = dataFile .. string.format("%s,",v)
    end
    dataFile = dataFile .. "\n"

    dataFile = dataFile .. string.format("%s,\n",eUserDataCategory.RecordTimeList)
    for i,v in pairs(self.recordTimeList) do
        dataFile = dataFile .. string.format("%s,%s,\n",i,v)
    end
    dataFile = dataFile .. "\n"

    local path = cc.FileUtils:getInstance():getWritablePath() .. "userData.csv"
    cc.FileUtils:getInstance():writeStringToFile(dataFile,path)
end