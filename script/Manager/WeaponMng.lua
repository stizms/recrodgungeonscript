require "script.Weapon.BaseWeapon"
require "script.Weapon.BaseMainWeapon"
require "script.Weapon.BaseSubWeapon"
require "script.Weapon.DynamiteStick"
require "script.Weapon.NormalBomb"
require "script.Weapon.IceBall"
require "script.Weapon.FireBall"

WeaponMngClass = {
    mainWeaponPool  = {},
    subWeaponPool   = {},
    usingWeaponPool = {}
}

-- 생성 함수
function WeaponMngClass:new(o)
    o = o or {}
    setmetatable(o,self)
    self.__index = self
    o:removeAll()
    return o
end

-- 무기의 열거형을 통해 실질 무기 인스턴스를 얻어오는 함수 (열거형과 클래스를 매칭하기 위해 만듦)
function WeaponMngClass:getWeaponInstance(weaponID)
    if weaponID == eWeaponID.NormalBomb then
        return NormalBombClass:new()
    elseif weaponID == eWeaponID.DynamiteStick then
        return DynamiteStickClass:new()
    elseif weaponID == eWeaponID.IceBall then
        return IceBallClass:new()
    elseif weaponID == eWeaponID.FireBall then
        return FireBallClass:new()
    end
end

-- 풀링 함수
function WeaponMngClass:addWeapon(weaponInstance)
    local poolingCount = 10
    
    if weaponInstance == nil then
        do return end
    else
        for i = 1, poolingCount do
            local temp = weaponInstance:new()
            temp.sprite:setVisible(false)
            if weaponInstance.tag == eAttackCategory.MainWeapon then
                table.insert(self.mainWeaponPool,temp)
            elseif weaponInstance.tag == eAttackCategory.SubWeapon then
                table.insert(self.subWeaponPool,temp)
            end
            StageMng.layer:addChild(temp.sprite,1)
        end
    end
end

-- 무기 얻어오는 함수
function WeaponMngClass:getWeapon(tag,x,y)
    local tempPool = nil
    if tag == eAttackCategory.MainWeapon then
        tempPool = self.mainWeaponPool
    elseif tag == eAttackCategory.SubWeapon then
        tempPool = self.subWeaponPool
    end

    for i,v in pairs(tempPool) do
        if v.sprite:isVisible() == false then
            table.insert(self.usingWeaponPool,v)
            v.sprite:setPosition(x,y)
            v.sprite:setVisible(true)
            v.sprite:runAction(v.bodyAction)
            do return end
        end
    end
    print("invalid")
end

-- 초기화 함수
function WeaponMngClass:removeAll()
    self.mainWeaponPool  = {}
    self.subWeaponPool   = {}
    self.usingWeaponPool = {}
end