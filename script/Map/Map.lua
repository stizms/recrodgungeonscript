MapClass = {
    minX            = nil,
    maxX            = nil,
    minY            = nil,
    maxY            = nil,
    tileMap         = nil,  -- 맵 스프라이트
    spawnPoint      = nil,  -- 플레이어 스폰 지점
    bossSpawnPoint  = nil,  -- 보스 스폰 지점
    monsterID       = nil   -- 생성할 보스 ID
}

-- 생성자
function MapClass:new(stage)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o:init(stage)
    return o
end

-- 세팅함수 (stage 열거형을 토대로 세팅한다.)
function MapClass:init(stage)
    stageInfo = stageTb:getStageInfo(stage)
    self.minX = stageInfo.minX
    self.maxX = stageInfo.maxX
    self.minY = stageInfo.minY
    self.maxY = stageInfo.maxY
    
    self.monsterID = stageInfo.monsterID

    local mapData = cc.TMXTiledMap:create(stageInfo.mapData)
    self.tileMap = cc.Sprite:create(stageInfo.mapName)
    self.tileMap:setAnchorPoint(0,0)
    self.tileMap:setScaleY(2)
    self.tileMap:setScaleX(2.25)

    -- 몬스터 스폰 포인트 받아오기 ( TMX 파일에 존재 )
    local objects = mapData:getObjectGroup("Objects")
    self.spawnPoint = objects:getObject("SpawnPoint")
    self.bossSpawnPoint = objects:getObject("BossSpawnPoint")

end

-- 맵 충돌 콜라이더 라인 드로우 함수
function MapClass:drawDebugLine()
    local leftDebugLine = cc.DrawNode:create()
    leftDebugLine:drawLine({x = self.minX,y = self.minY},{x = self.minX, y = self.maxY},cc.c4f(255/255,0/255,0/255,1))
    StageMng.layer:addChild(leftDebugLine)

    local rightDebugLine = cc.DrawNode:create()
    rightDebugLine:drawLine({x = self.maxX, y = self.minY},{x = self.maxX, y = self.maxY},cc.c4f(255/255,0/255,0/255,1))
    StageMng.layer:addChild(rightDebugLine)

    local upDebugLine = cc.DrawNode:create()
    upDebugLine:drawLine({x = self.minX, y = self.maxY},{x = self.maxX,y = self.maxY},cc.c4f(255/255,0/255,0/255,1))
    StageMng.layer:addChild(upDebugLine)

    local downDebugLine = cc.DrawNode:create()
    downDebugLine:drawLine({x = self.minX,y = self.minY},{x = self.maxX,y = self.minY},cc.c4f(255/255,0/255,0/255,1))
    StageMng.layer:addChild(downDebugLine)
end