math.randomseed(os.time())

BTState = {
    Success = "Success",
    Failure = "Failure",
    Continue = "Continue",
    Abort = "Abort"
}

BTCategory = {
    BT_NODE = "BTNode",
    BT_BRANCH = "Branch"
}

BT = {}

function BT:new(o)
    o = o or {}
    setmetatable(o,self)
    self.__index = self
    return o
end


function BT:Root()
    return Root:new()
end

function BT:Sequence()
    return Sequence:new()
end

function BT:Selector()
    return Selector:new()
end

-- 횟수만큼 자식에 있는 노드를 실행
function BT:Repeat(count)
    return Repeat:new(count)
end

-- Func의 결과값이 true일 동안 실행
function BT:While(func)
    return While:new(func)
end

-- IF(함수 리턴이 트루라면) 자식에 있는 노드 모두 순차 실행
function BT:If(func)
    return ConditionalBranch:new(func)
end

-- 자식에 속한 브런치 중에 랜덤으로 실행 BT:RandomSequence({1,6,4,4}):OpenBranch(...)
function BT:RandomSequence(percentTable)
    return RandomSequence:new(percentTable)
end

-- 동작 실행
function BT:Call(actionFunc)
    return Action:new(actionFunc)
end

-- true / false 리턴하는 단일 조건
function BT:Condition(func)
    return Condition:new(func)
end

function BT:Wait(seconds)
    return Wait:new(seconds)
end

-- Behaviour Tree 파괴 ( 다신 Tick 안돔 )
function BT:Terminate()
    return Terminate:new()
end


--------------------------------------------------------------------------------------------------------------------------------------------

BTNode = {tag = BTCategory.BT_NODE}

function BTNode:new(o)
    o = o or {}
    setmetatable(o,self)
    self.__index = self
    return o
end

function BTNode:Tick(dt)
end

--------------------------------------------------------------------------------------------------------------------------------------------

Branch = BTNode:new({tag = BTCategory.BT_BRANCH, activeChild = 1, children = {}})

function Branch:new(o)
    o = o or {}
    setmetatable(o,self)
    self.__index = self
    o.children = {}
    return o
end

function Branch:OpenBranch(childList)
    for i,v in ipairs(childList) do
        table.insert(self.children,v)
    end
    return self
end

function Branch:Children()
    return self.children
end

function Branch:ActiveChild()
    return self.activeChild
end

function Branch:ResetChildren()
    self.activeChild = 1
    for i, v in ipairs(self.children) do
        if v ~= nil and v.tag == BTCategory.BT_BRANCH then
            v:ResetChildren()
        end
    end
end

--------------------------------------------------------------------------------------------------------------------------------------------

Sequence = Branch:new()

function Sequence:Tick(dt)
    local childState = self.children[self.activeChild]:Tick()
    if childState == BTState.Success then
        self.activeChild = self.activeChild + 1
        if self.activeChild == #self.children + 1 then
            self.activeChild = 1
            return BTState.Success
        else
            return BTState.Continue
        end
    elseif childState == BTState.Failure then
        self.activeChild = 1
        return BTState.Failure
    elseif childState == BTState.Continue then
        return BTState.Continue
    elseif childState == BTState.Abort then
        self.activeChild = 1
        return BTState.Abort
    end
    print("This should never happen, but clearly it has.")
end

--------------------------------------------------------------------------------------------------------------------------------------------

Selector = Branch:new()

function Selector:Tick(dt)
    local childState = self.children[self.activeChild]:Tick()
    if childState == BTState.Success then
        self.activeChild = 1
        return BTState.Success
    elseif childState == BTState.Failure then
        self.activeChild = self.activeChild + 1
        if self.activeChild == #self.children + 1 then
            self.activeChild = 1
            return BTState.Failure
        else
            return BTState.Continue
        end
    elseif childState == BTState.Continue then
        return BTState.Continue
    elseif childState == BTState.Abort then
        self.activeChild = 1
        return BTState.Abort
    end
    print("This should never happen, but clearly it has.")
end


--------------------------------------------------------------------------------------------------------------------------------------------

Block = Branch:new()

function Block:Tick(dt)
    local childState = self.children[self.activeChild]:Tick(dt)
    if childState == BTState.Continue then
        return BTState.Continue
    else
        self.activeChild = self.activeChild + 1
        if self.activeChild == #self.children + 1 then
            self.activeChild = 1
            return BTState.Success
        end
        return BTState.Continue
    end
end


--------------------------------------------------------------------------------------------------------------------------------------------

Root = Block:new({isTerminated = false})

function Root:Tick(dt)
    if self.isTerminated == true then
        return BTState.Abort
    end
    while true do
        local childState = self.children[self.activeChild]:Tick(dt)
        if childState == BTState.Continue then
            return BTState.Continue
        elseif childState == BTState.Abort then
            self.isTerminated = true
            return BTState.Abort
        else
            self.activeChild = self.activeChild + 1
            if self.activeChild == #self.children + 1 then
                self.activeChild = 1
                return BTState.Success
            end
        end
    end
end


--------------------------------------------------------------------------------------------------------------------------------------------

RandomSequence = Block:new({weight = nil, addedWeight = nil})

function RandomSequence:new(weightList,o)
    o = o or {}
    setmetatable(o,self)
    self.__index = self
    o.weight = weightList
    o.children = {}
    return o
end

function RandomSequence:OpenBranch(childList)
    self.addedWeight = {}

    for i = 1, #childList do
        local weight = 0
        local previousWeight = 0

        if self.weight == nil or #self.weight+1 <= i then
            weight = 1
        else
            weight = self.weight[i]
        end

        if i > 1 then
            previousWeight = self.addedWeight[i-1]
        end
        self.addedWeight[i] = weight + previousWeight
    end

    return Block.OpenBranch(self,childList)
end

function RandomSequence:Tick(dt)
    if self.activeChild == 0 then
        self:PickNewChild()
    end
    
    local result = self.children[self.activeChild]:Tick(dt)

    if result == BTState.Continue then
        return BTState.Continue
    else
        self:PickNewChild()
        return result
    end
end

function RandomSequence:PickNewChild()
    local choice = math.random(1,self.addedWeight[#self.addedWeight])
    for i, v in ipairs(self.addedWeight) do
        if choice - v <= 0 then
            self.activeChild = i
            break
        end
    end 
end


--------------------------------------------------------------------------------------------------------------------------------------------

ConditionalBranch = Block:new({fn = nil, tested = false})

function ConditionalBranch:new(fn,o)
    o = o or {}
    setmetatable(o,self)
    self.__index = self
    o.fn = fn
    o.children = {}
    o.tested = false
    return o
end

function ConditionalBranch:Tick(dt)
    if self.tested == false then
        self.tested = self.fn()
    end
    if self.tested == true then
        local result = Block.Tick(self,dt)
        if result == BTState.Continue then
            return BTState.Continue
        else
            self.tested = false
            return result
        end
    else
        return BTState.Failure
    end
end


--------------------------------------------------------------------------------------------------------------------------------------------

While = Block:new({fn = nil})

function While:new(fn,o)
    o = o or {}
    setmetatable(o,self)
    self.__index = self
    o.fn = fn
    o.children = {}
    return o
end

function While:Tick(dt)
    if self.fn() == true then
        Block.Tick(self,dt)
    else
        self:ResetChildren()
        return BTState.Failure
    end
    return BTState.Continue
end


--------------------------------------------------------------------------------------------------------------------------------------------

Repeat = Block:new({count = 1, currentCount = 0})

function Repeat:new(count,o)
    o = o or {}
    setmetatable(o,self)
    self.__index = self
    o.count = count
    o.children = {}
    return o
end

function Repeat:Tick(dt)
    if self.count > 0 and self.currentCount < self.count then
        local result = Block.Tick(self,dt)
        if result == BTState.Continue then
            return BTState.Continue
        else
            self.currentCount = self.currentCount + 1
            if self.currentCount == self.count then
                self.currentCount = 0
                return BTState.Success
            end
            return BTState.Continue
        end
    end
    return BTState.Success
end


--------------------------------------------------------------------------------------------------------------------------------------------

Action = BTNode:new({fn = nil})

function Action:new(fn,o)
    o = o or {}
    setmetatable(o,self)
    self.__index = self
    o.fn = fn
    return o
end

function Action:Tick(dt)
    if self.fn ~= nil then
        self.fn()
        return BTState.Success
    end
end


--------------------------------------------------------------------------------------------------------------------------------------------

Condition = BTNode:new({fn = nil})

function Condition:new(fn,o)
    o = o or {}
    setmetatable(o,self)
    self.__index = self
    o.fn = fn
    return o
end

function Condition:Tick(dt)
    local result = self.fn()
    if result == true then
        return BTState.Success
    else
        return BTState.Failure        
    end
end


--------------------------------------------------------------------------------------------------------------------------------------------

Wait = BTNode:new({seconds = 0, future = -1,clockTemp = 0})

function Wait:new(seconds,o)
    o = o or {}
    setmetatable(o,self)
    self.__index = self
    if seconds < 0 then
        seconds = 0
    end
    o.seconds = seconds
    o.future = -1
    return o
end

function Wait:Tick(dt)
    if self.future < 0 then
        self.clockTemp = os.clock()
        self.future = self.clockTemp + self.seconds
    end 
    self.clockTemp = self.clockTemp + dt
    if self.clockTemp >= self.future then
        self.future = -1
        return BTState.Success
    else
        return BTState.Continue
    end
end

--------------------------------------------------------------------------------------------------------------------------------------------

Terminate = BTNode:new()

function Terminate:Tick()
    return BTState.Abort
end
