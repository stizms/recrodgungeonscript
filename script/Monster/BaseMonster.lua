BaseMonsterClass = { 
    sprite          = nil,
    state           = eMonsterState.Idle,   -- 현재 상태
    prevState       = nil,  -- 이전 상태
    idleImage       = nil,  -- Idle 액션이 없는경우 Idle 이미지로 대체
    ai              = nil,  -- AI ( BehaviourTree의 Root 객체)
    maxHp           = 0 ,   -- MaxHp
    hp              = 0,    -- 현재 HP
    moveSpeed       = 0,    -- 이동 속도
    pivotY          = nil,  -- 정확한 사이징을 위해 pivot을 넣음 (투명값 범위를 넘기 위해서)
    sizeX           = 0,    -- 플레이어와의 충돌박스
    sizeY           = 0,    -- 플레이어와의 충돌박스
    damagedSizeX    = 0,    -- 폭발로 데미지 받는 범위
    damagedSizeY    = 0,    -- 폭발로 데미지 받는 범위
    subDamagedSizeX = 0,    -- 보조무기와 충돌되는 범위
    subDamagedSizeY = 0,    -- 보조무기와 충돌되는 범위
    attackDelay     = 0,    -- deltaTime 누적 변수
    projectile      = nil,  -- eProjectileID
    projectilePos   = {x = 0, y = 0},  -- 투사체 생성 위치
    roamingList     = {},   -- 이동 경로 리스트
    roamingIndex    = 1,    -- 현재 이동 경로 인덱스
    dirVec          = {x =0, y = 0},    -- 목표 지점까지의 방향
    damagedDuration = 0.5,  -- 공격 받아서 깜빡 거리는 기간
    baseShader      = nil,  -- 하얀색으로 바꾸기 전 셰이더
    vecIndex        = 1,    -- 반복문 도는 인덱스
    actionList      = nil,  -- 애니메이션 리스트
    animInfo        = nil,  -- 애니메이션 정보

    -- 함수 부
    isAlive         = nil,  -- 살아 있는가? (hp체크)
    isIdle          = nil,  -- 상태가 Idle인가?
    isWalk          = nil,  -- 상태가 walk인가?
    isIce           = nil,  -- 상태가 Ice인가?
    setIdle         = nil,  -- Idle 상태로 변이
    setWalk         = nil,  -- walk 상태로 변이
    setAttack       = nil,  -- attack 상태로 변이
    setDeath        = nil,  -- Death 상태로 변이
    actionIdle      = nil,  -- Idle 애니메이션 실행
    actionWalk      = nil,  -- walk 애니메이션 실행
    actionAttack    = nil,  -- attack 애니메이션 실행
    isArrive        = nil,  -- 목표지점에 도착했는가?
    isDifferentToPrevState = nil, -- 이전 상태와 지금 상태가 다른가?

    -- 공격 패턴 부
    circle1Attack   = nil,  
    circle2Attack   = nil,
    circle3Attack   = nil,
    circleAttack    = nil,
    rotateAttack    = nil,
    nWayAttack      = nil
}

-- 생성자
function BaseMonsterClass:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self
    o:init()
    return o
end

-- 초기화
function BaseMonsterClass:init()
    self:createMethod()
end

-- 업데이트 함수 ( StageScene에서 돌아감 )
function BaseMonsterClass:update(dt)
    if StageMng.player.state ~= ePlayerState.Death then
        if self.ai ~= nil then
            self.ai:Tick(dt)
        end
        
        if self.state == eMonsterState.Walk then
            local cX,cY = self.sprite:getPosition()
            self.sprite:setPosition(cX + self.dirVec.x * self.moveSpeed * dt,cY + self.dirVec.y * self.moveSpeed * dt)
        end
    end
end

-- 데미지 받았는지 체크 ( 폭발 범위로 인한 데미지)
function BaseMonsterClass:isDamaged(explosionRect,damage)
    local pX,pY = self.sprite:getPosition()
    if self.pivotY ~= nil then
        pY = pY - self.pivotY
    end
    local rect = cc.rect(pX-self.damagedSizeX/2,pY-self.damagedSizeY/2,self.damagedSizeX,self.damagedSizeY)
    if intersectsRect(explosionRect,rect) and StageMng.player.state ~= ePlayerState.Death then
        self:damaged(damage)
        return true
    else
        return false
    end
end

-- 데미지 받은 경우 (Hp 깎음 여기서 bossInfoUI의 DownHpValue(게이지 다운되는 속도값) 대입)
function BaseMonsterClass:damaged(damage)
    local prevHp = self.hp
    self.hp = self.hp - damage
    if self.hp < 0 then
        self.hp = 0
    end
    changeWhiteShader(self.sprite,0.1)
    inGameUI.bossInfo.downHpValue = inGameUI.bossInfo.gazePerHp*prevHp - inGameUI.bossInfo.gazePerHp*self.hp
    inGameUI.bossInfo.isDamaging = true
end

function BaseMonsterClass:setIce()
    if self.hp > 0 then
        self.state = eMonsterState.Ice
        local spriteToBlue = cc.CallFunc:create(function ()
            self.sprite:setColor(cc.c3b(0,0,230))
        end)
        local delay = cc.DelayTime:create(5)
        local iceBrake = cc.CallFunc:create(function ()
            self.sprite:setColor(cc.c3b(255,255,255))
            self.setIdle()
        end)
        local iceAction = cc.Sequence:create(spriteToBlue,delay,iceBrake)
        self.sprite:runAction(iceAction)
    end
end

-- 함수들을 생성하고 저장하는 함수
function BaseMonsterClass:createMethod()
    -- 살아 있는가?
    self.isAlive = function ()
        if self.hp > 0 then
            return true
        else
            return false
        end
    end

    -- 상태가 Idle인가?
    self.isIdle = function ()
        if self.state == eMonsterState.Idle then
            return true
        else
            return false
        end
    end

    -- 상태가 Walk인가?
    self.isWalk = function ()
        if self.state == eMonsterState.Walk then
            return true
        else
            return false
        end
    end

    -- 상태가 Attack인가?
    self.isAttack = function ()
        if self.state == eMonsterState.Attack then
            return true
        else
            return false
        end
    end

    self.isIce = function ()
        if self.state == eMonsterState.Ice then
            return true
        else
            return false
        end
    end

    -- 상태 변화
    self.setIdle = function ()
        self.state = eMonsterState.Idle
    end
    
    self.setWalk = function()
        local cX,cY = self.sprite:getPosition()
        local targetPos = self.roamingList[self.roamingIndex]
        -- 목적지까지 방향벡터를 구해서 이동
        local distance = math.sqrt((targetPos.x - cX)*(targetPos.x - cX) + (targetPos.y - cY)*(targetPos.y - cY))
        self.dirVec.x = (targetPos.x - cX)/distance
        self.dirVec.y = (targetPos.y - cY)/distance
        self.state = eMonsterState.Walk
    end

    self.setAttack = function ()
        self.state = eMonsterState.Attack
    end

    -- 깜빡이는 작업도 액션으로 하므로 stopAllActions가 되지 않음 일일이 비교후 액션을 제거해주어야 함.
    self.setDeath = function ()
        self.state = eMonsterState.Death
        if self.prevState ~= nil then
            if self.prevState == eMonsterState.Idle then
                if self.actionList.idle ~= nil then
                    self.sprite:stopAction(self.actionList.idle)
                end
            elseif self.prevState == eMonsterState.Walk then
                self.sprite:stopAction(self.actionList.walk)
            elseif self.prevState == eMonsterState.Attack then
                self.sprite:stopAction(self.actionList.attack)
            end
        end
        self.sprite:setTexture(self.idleImage)
        SoundMng:stopBGM()
        self:bossDeathAnim()
    end

    -- 애니메이션 변화
    self.actionIdle = function ()
        if self.prevState ~= nil then
            if self.prevState == eMonsterState.Walk then
                self.sprite:stopAction(self.actionList.walk)
            elseif self.prevState == eMonsterState.Attack then
                self.sprite:stopAction(self.actionList.attack)
            end
        end
        if self.actionList.idle ~= nil then
            self.sprite:stopAction(self.actionList.idle)
            self.sprite:runAction(self.actionList.idle)
        else
            self.sprite:setTexture(self.idleImage)
        end
        self.prevState = self.state
    end

    self.actionWalk = function ()
        if self.prevState ~= nil then
            if self.prevState == eMonsterState.Idle then
                if self.actionList.idle ~= nil then
                    self.sprite:stopAction(self.actionList.idle)
                end
            elseif self.prevState == eMonsterState.Attack then
                self.sprite:stopAction(self.actionList.attack)
            end
        end
        self.sprite:runAction(self.actionList.walk)
        self.prevState = self.state
    end

    self.actionAttack = function ()
        if self.prevState ~= nil then
            if self.prevState == eMonsterState.Idle then
                if self.actionList.idle ~= nil then
                    self.sprite:stopAction(self.actionList.idle)
                end
            elseif self.prevState == eMonsterState.Walk then
                self.sprite:stopAction(self.actionList.walk)
            end
        end
        self.sprite:runAction(self.actionList.attack)
        self.prevState = self.state
    end

    -- prevState와 currentState가 다른가?
    self.isDifferentToPrevState = function ()
        if self.state ~= self.prevState then
            return true
        else
            return false
        end
    end

    -- 도착했는지 확인
    self.isArrive = function ()
        local cX,cY = self.sprite:getPosition()
        local targetPos = self.roamingList[self.roamingIndex]
        if (targetPos.x - cX) * (targetPos.x - cX) + (targetPos.y - cY) * (targetPos.y - cY) < 10*10 then
            self.roamingIndex = self.roamingIndex + 1
            if self.roamingIndex > #self.roamingList then
                self.roamingIndex = self.roamingIndex % #self.roamingList
            end
            return true
        else
            return false
        end
    end

    -- 탄막 패턴
    self.circle1Attack = function ()
        self:createPatternProjectile(eDirection.Down,8,360)
    end

    self.circle2Attack = function ()
        local angle = 22.5
        local rad = angle /360
        local temp = angle*2 / 360
        for i = 1, 8 do
            self:createPatternProjectile(rad+(temp*i),1)
        end
    end

    self.circle3Attack = function ()
        local angle = 11.25
        local rad = angle / 360
        local temp = angle*2 / 360
        for i = 1, 16 do
            self:createPatternProjectile(rad+(temp*i),1)
        end
    end

    self.circleAttack = function ()
        self:createPatternProjectile(eDirection.Down,34,360)
    end
    
    self.rotateAttack = function ()
        local projectileCount = 34
        local eachAngle = 360/34 * self.vecIndex
        self.vecIndex = self.vecIndex + 1
        if self.vecIndex > projectileCount then
            self.vecIndex = 1
        end
        self:createPatternProjectile(eachAngle/340,1)
    end

    self.nWayAttack = function ()
        -- 플레이어와 몬스터의 각도를 구하여 플레이어쪽으로 발사
        local pX,pY = StageMng.player.sprite:getPosition()
        local currentX,currentY = self.sprite:getPosition()
        local rad = math.atan2(pY - currentY,pX - currentX) *180/math.pi
        if rad < 0 then
            rad = rad + 360
        end
        self:createPatternProjectile(rad/360,math.random(2,6),90)
    end
end


-- 패턴 생성 함수
-- 일정 각도 입력받아 특정 범위내에 정해진 개수의 총알 발사 ( 총알이 1개인 경우 해당 방향으로 총알 발사 ) 0~360도로 계산
function BaseMonsterClass:createPatternProjectile(direction,projectileCount,angleRange)
    if direction == eDirection.Down then
        direction = 0.75
    elseif direction == eDirection.Up then
        direction = 0.25
    elseif direction == eDirection.Left then
        direction = 0.5
    elseif direction == eDirection.Right then
        direction = 0
    end

    local startX, startY = self.sprite:getPosition()
    local rad  = nil
    local dirX = nil
    local dirY = nil
    -- 라디안 각도를 통하여 벡터를 구하고 탄환 발사
    if projectileCount == 1 then
        rad = direction * math.pi * 2
        dirY = math.sin(rad)
        dirX = math.cos(rad)
        ProjectileMng:getProjectile(startX+self.projectilePos.x,startY+self.projectilePos.y,dirX, dirY,true)
    else
    -- 전체범위에 발사할 총 탄수를 나눠서 각도 계산
        local eachAngle = ((angleRange/projectileCount)/2)/360
        angleRange = angleRange/360
        for i = 1, projectileCount do
            rad = (direction - (angleRange/2) + (eachAngle*(2*i-1)))* math.pi * 2
            dirY = math.sin(rad)
            dirX = math.cos(rad)
            ProjectileMng:getProjectile(startX+self.projectilePos.x,startY+self.projectilePos.y,dirX, dirY,true)
        end
    end
end

-- 죽는 애니메이션
function BaseMonsterClass:bossDeathAnim()
    SoundMng:playEffect(eEffectSoundID.BossDeathEffect)

    -- 몬스터의 위치를 0~1사이로 바꾸어 기준 점으로 잡고 확대
    local anchorX,anchorY = self.sprite:getPosition()
    anchorX = anchorX/visibleSize.width
    anchorY = anchorY/visibleSize.height
    StageMng.layer:setAnchorPoint(anchorX,anchorY)

    -- 줌 하는 시퀀스
    local scaleToMonster  = cc.ScaleTo:create(5,1.2)
    local delay           = cc.DelayTime:create(1)
    local recoverLayer    = cc.CallFunc:create(function () 
        StageMng.layer:setScale(1)
        StageMng.layer:setAnchorPoint(0.5,0.5)
    end)

    local stageClear = cc.CallFunc:create(function ()
        StageMng:clearStage()
    end)

    local zoomSequence    = cc.Sequence:create(scaleToMonster,delay,recoverLayer,delay,stageClear)

    -- 진동 시퀀스
    local normalShake = cc.CallFunc:create(function() 
        StageMng:shake(30,0.5)
    end)

    local shakeDelay    = cc.DelayTime:create(0.8)
    local startDelay    = cc.DelayTime:create(1)
    local powerfulShake = cc.CallFunc:create(function()
        StageMng:shake(20,3)
    end)

    local shakeSequence = cc.Sequence:create(startDelay,normalShake,shakeDelay,normalShake,shakeDelay,powerfulShake)
    
    -- 페이드인 시퀀스
    local whiteWall = cc.LayerColor:create(cc.c4b(255,255,255,0))
    StageMng.layer:addChild(whiteWall,101)

    local firstFade  = cc.FadeTo:create(2,25)
    local secondFade = cc.FadeTo:create(2,50)
    local finalFade  = cc.FadeTo:create(1,255)
    
    local removeWhiteWall = cc.CallFunc:create(function() 
        StageMng.layer:removeChild(whiteWall)
    end)

    local removeMonster = cc.CallFunc:create(function()
        self:removeMonster()
    end)
    
    local fadeOut = cc.FadeOut:create(1)
    local fadeSequence = cc.Sequence:create(firstFade,secondFade,finalFade,delay,removeMonster,fadeOut,removeWhiteWall)
    
    -- 파티클 시퀀스
    local particleCall = cc.CallFunc:create(function()
        for i = 1, 5 do
            local cX,cY = self.sprite:getPosition()
            if self.pivotY ~= nil then
                cY = cY - self.pivotY
            end
            cX = math.random(cX-self.sizeX/2,cX+self.sizeX/2+1)
            cY = math.random(cY-self.sizeY/2,cY+self.sizeY/2+1)
            ParticleMng:getParticle(eParticleTag.BossDeath,cX,cY)  
        end 
    end)

    local explosionDelay = cc.DelayTime:create(0.7)
    local repeatSequence = cc.Sequence:create(particleCall,explosionDelay)
    local repeatAction   = cc.Repeat:create(repeatSequence,4)

    local bombSequence   = cc.Sequence:create(startDelay,particleCall,shakeDelay,particleCall,shakeDelay,particleCall,repeatAction)

    local spawnAction    = cc.Spawn:create(shakeSequence,zoomSequence,bombSequence)

    -- 스폰으로 하기
    StageMng.layer:runAction(spawnAction)
    whiteWall:runAction(fadeSequence)
end

-- 몬스터 없애기 ( 풀로 돌려보내기 )
function BaseMonsterClass:removeMonster()
    self.sprite:setVisible(false)
    self.sprite:stopAllActions()
    table.remove(MonsterMng.usingMonsterPool,tableFind(MonsterMng.usingMonsterPool,self))
    StageMng.boss = nil
end