GhostBossClass = BaseMonsterClass:new()

-- 초기화 함수
function GhostBossClass:init()
    local monsterInfo    = monsterTb[eMonsterID.GhostBoss]
    self.sprite          = cc.Sprite:create("GhostMonster_Idle_0.png")
    self.idleImage       = "GhostMonster_Idle_0.png"
    self.baseShader      = self.sprite:getGLProgram()
    self.ai              = nil
    self.maxHp           = monsterInfo.maxHp
    self.hp              = self.maxHp
    self.moveSpeed       = monsterInfo.moveSpeed
    self.sizeX           = monsterInfo.sizeX
    self.sizeY           = monsterInfo.sizeY
    self.damagedSizeX    = monsterInfo.damagedSizeX
    self.damagedSizeY    = monsterInfo.damagedSizeY
    self.subDamagedSizeX = monsterInfo.subDamagedSizeX
    self.subDamagedSizeY = monsterInfo.subDamagedSizeY
    self.projectilePos.x = monsterInfo.projectilePos.x
    self.projectilePos.y = monsterInfo.projectilePos.y
    self.projectile      = monsterInfo.projectile
    self.attackDelay     = 1
    self.roamingList[1]  = {x = 200,y = 700}
    self.roamingList[2]  = {x = 550,y = 700}
    self.roamingList[3]  = {x = 375,y = 950}
    self.actionList = {
        idle = nil, walk = nil, attack = nil
    }
    self.animInfo = {
        idle    = { delay = 0.1, path = "GhostMonster_Idle_",      totalIndex = 12},
        walk    = { delay = 0.1, path = "GhostMonster_Idle_",      totalIndex = 12},
        attack  = { delay = 0.07, path = "GhostMonster_Attack_",    totalIndex = 24}
    }
    self:enrollAnimation()
    self:createMethod()
end

-- 애니메이션 등록 함수
function GhostBossClass:enrollAnimation()
    self.actionList.idle   = createAnimation(self.animInfo.idle,true)
    self.actionList.walk   = createAnimation(self.animInfo.walk,true)
    self.actionList.attack = createAnimation(self.animInfo.attack,false)
    for i,v in pairs(self.actionList) do
        v:retain()
    end
end

-- AI 등록 함수 ( MonsterMng에서 실행 됨 )
function GhostBossClass:enrollAI()
    self.ai = BT:Root()
    self.ai:OpenBranch({
        -- 살아 있는가?
        BT:While(self.isAlive):OpenBranch({
            -- 상태 체크
            BT:If(self.isIce):OpenBranch({
                BT:If(self.isDifferentToPrevState):OpenBranch({
                    BT:Call(self.actionIdle)
                })
            }),
            BT:If(self.isWalk):OpenBranch({
                BT:If(self.isDifferentToPrevState):OpenBranch({
                    BT:Call(self.actionWalk)
                })
            }),
            BT:If(self.isAttack):OpenBranch({
                BT:If(self.isDifferentToPrevState):OpenBranch({
                    -- 공격 동작
                    BT:Call(self.actionAttack),
                    BT:Wait(self.attackDelay),
                    -- 2가지 패턴중 랜덤으로 재생
                    BT:RandomSequence({10,10}):OpenBranch({
                        BT:Root():OpenBranch({
                            BT:Repeat(3):OpenBranch({
                                BT:Call(self.circle1Attack),
                                BT:Wait(0.1),
                                BT:Call(self.circle2Attack),
                                BT:Wait(0.1),
                                BT:Call(self.circle3Attack)
                            })                        
                        }),
                        BT:Root():OpenBranch({
                            -- 34는 원 한바퀴 당 탄환 숫자
                            BT:Repeat(34 * 3):OpenBranch({
                                BT:Call(self.rotateAttack),
                            })
                        })
                    }),
                    BT:Selector():OpenBranch({
                        BT:Condition(self.isIce),
                        BT:Call(self.setIdle)
                    })
                })
            }),
            BT:If(self.isIdle):OpenBranch({
                -- 애니메이션
                BT:If(self.isDifferentToPrevState):OpenBranch({
                    BT:Call(self.actionIdle)
                })
                -- 그 외 실행
            }),
            -- 상태 변이
            BT:Selector():OpenBranch({
                BT:If(self.isWalk):OpenBranch({
                    BT:If(self.isArrive):OpenBranch({
                        BT:Call(self.setIdle)    
                    })
                }),
                BT:Selector():OpenBranch({
                    -- 현재 200,10,10의 확률이 가장 이상적인 확률
                    BT:Condition(self.isAttack),
                    BT:Condition(self.isIce),
                    BT:RandomSequence({200,10,10}):OpenBranch({
                        BT:Root():OpenBranch({
                            BT:Call(self.setIdle)
                        }),
                        BT:Root():OpenBranch({
                            BT:Call(self.setWalk)
                        }),
                        BT:Root():OpenBranch({
                            BT:Call(self.setAttack)
                        })
                    })
                })
            })
        }),
        -- isAlive가 아니라면 죽은 것
        BT:Call(self.setDeath),
        -- 파괴
        BT:Terminate()
    })
end