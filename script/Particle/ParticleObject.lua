ParticleObjectClass = {
    sprite          = nil,
    lifeTime        = 0,    -- 생명주기에 관련된 dt를 누적하는 곳
    particleInfo    = nil,  -- 파티클의 정보 ( 프레임당 딜레이, 생명시간, 총 프레임, 이미지 이름)
    particleDelay   = nil,  -- 총 생명 시간
    bodyAction      = nil,  -- 애니메이션
    animInfo        = {delay = nil, path = nil, totalIndex = nil} -- 애니메이션 인포
}

function ParticleObjectClass:new(particleInfo,o)
    o = o or {}
    setmetatable(o,self)
    self.__index = self
    o:init(particleInfo)
    return o 
end

-- 매개변수로 넘어온 파티클 정보를 통해서 초기 세팅
function ParticleObjectClass:init(particleInfo)
    self.sprite = cc.Sprite:create(string.format("%s0.png",particleInfo.path))
    
    self.particleInfo        = particleInfo
    self.particleDelay       = self.particleInfo.particleDelay
    self.animInfo.delay      = self.particleInfo.delay
    self.animInfo.path       = self.particleInfo.path
    self.animInfo.totalIndex = self.particleInfo.totalIndex
    self:enrollAnimation()
end

-- 애니메이션 등록
function ParticleObjectClass:enrollAnimation()
    if self.animInfo.path == nil then
        print("Animation Information is not setting")
        do return end
    else
        self.bodyAction = createAnimation(self.animInfo,false)
        self.bodyAction:retain()
    end
end

-- 파티클 update ( StageScene에서 돌린다. )
function ParticleObjectClass:update(dt)
    self.lifeTime = self.lifeTime + dt
    if self.lifeTime > self.particleDelay then
       self.lifeTime = 0
       self.sprite:setVisible(false)
       self.sprite:stopAction(self.bodyAction)
       table.remove(ParticleMng.usingParticlePool,tableFind(ParticleMng.usingParticlePool,self))
    end
end