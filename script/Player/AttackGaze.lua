AttackGazeClass = {
    gazeLayer       = nil,
    gazeBackGround  = nil,
    gazeProgress    = nil,
}

function AttackGazeClass:new(o)
    o = o or {}
    setmetatable(o,self)
    self.__index = self
    o:init()
    return o
end

-- 세팅
function AttackGazeClass:init()
    self.gazeLayer      = cc.Layer:create()
    self.gazeBackGround = cc.Sprite:create("playerGazeBG.png")
    local gazeSprite    = cc.Sprite:create("playerGaze.png")
    self.gazeProgress   = cc.ProgressTimer:create(gazeSprite)
    self.gazeProgress:setType(cc.PROGRESS_TIMER_TYPE_BAR)
    self.gazeProgress:setBarChangeRate(cc.p(1,0))
    self.gazeProgress:setMidpoint(cc.p(0,0.5))
    self.gazeProgress:setPercentage(0)
    local BGSize = self.gazeBackGround:getContentSize()
    self.gazeProgress:setPosition(BGSize.width/2,BGSize.height/2)
    self.gazeBackGround:addChild(self.gazeProgress)
    self:setVisible(false)
    self.gazeLayer:addChild(self.gazeBackGround,100)
end

-- AttackGaze 가시화 & 비가시화
function AttackGazeClass:setVisible(visibleFlag,pX,pY)
    if visibleFlag == false then
        self.gazeBackGround:setVisible(false)
    else
        self.gazeLayer:setPosition(pX,pY)
        self.gazeBackGround:setVisible(true)
    end
end