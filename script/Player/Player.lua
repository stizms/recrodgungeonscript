require "script.Player.AttackGaze"

PlayerClass = {
    sprite        = nil,
    moveSpeed     = 200,                -- 움직임 속도
    state         = ePlayerState.Idle,  -- 현재 상태
    prevState     = ePlayerState.Idle,              -- 이전 상태
    sizeX         = 25,                 -- 플레이어의 충돌 판정 콜라이더
    sizeY         = 50,                 -- 플레이어의 충돌 판정 콜라이더
    direction     = eDirection.Up,      -- 현재 방향
    prevDirection = direction,          -- 이전 방향
    currentMainWeapon = nil,            -- 현재 착용한 메인 무기
    currentSubWeapon  = nil,            -- 현재 착용한 서브 무기
    attackAnimDelay   = 0,              -- DeltaTime 누적 변수
    
    attackGaze  = nil,          -- AttackGaze 객체
    attackGazeY = 0,
    
    knockbackDir      = nil,    -- 넉백 방향
    knockbackDuration = 0.2,    -- 넉백 시간
    currentDuration   = 0.2,    -- DeltaTime 누적 변수
    knockbackPower    = 2,      -- 넉백 파워

    maxSubBullet = 0,   -- 서브 무기 탄환 개수
    useSubBullet = 0,   -- 서브 무기 탄환 사용 개수
    
    walkSound = nil,

    -- 각 방향공격 액션과 서브공격 용 액션은 다름
    actionList = { upWalk = nil, upAttack = nil, leftUpWalk = nil, leftUpAttack = nil, 
                leftWalk = nil, leftAttack = nil, leftDownWalk = nil, leftDownAttack = nil,
                downWalk = nil, downAttack = nil, rightDownWalk = nil, rightDownAttack = nil,
                rightWalk = nil, rightAttack = nil, rightUpWalk = nil, rightUpAttack = nil,
                subAttack = nil },

    animInfo = { upWalk        = { delay = 0.15, path = "playerUpWalk_",          totalIndex = 4},
                upAttack       = { delay = nil,  path = "playerUpAttack_",        totalIndex = 3},
                leftUpWalk     = { delay = 0.15, path = "playerLeftUpWalk_",      totalIndex = 4},
                leftUpAttack   = { delay = nil,  path = "playerLeftUpAttack_",    totalIndex = 3},
                leftWalk       = { delay = 0.15, path = "playerLeftWalk_",        totalIndex = 4},
                leftAttack     = { delay = nil,  path = "playerLeftAttack_",      totalIndex = 3},
                leftDownWalk   = { delay = 0.15, path = "playerLeftDownWalk_",    totalIndex = 4},
                leftDownAttack = { delay = nil,  path = "playerLeftDownAttack_",  totalIndex = 3},
                downWalk       = { delay = 0.15, path = "playerDownWalk_",        totalIndex = 4},
                downAttack     = { delay = nil,  path = "playerDownAttack_",      totalIndex = 3},
                rightDownWalk  = { delay = 0.15, path = "playerRightDownWalk_",   totalIndex = 4},
                rightDownAttack= { delay = nil,  path = "playerRightDownAttack_", totalIndex = 3},
                rightWalk      = { delay = 0.15, path = "playerRightWalk_",       totalIndex = 4},
                rightAttack    = { delay = nil,  path = "playerRightAttack_",     totalIndex = 3},
                rightUpWalk    = { delay = 0.15, path = "playerRightUpWalk_",     totalIndex = 4},
                rightUpAttack  = { delay = nil,  path = "playerRightUpAttack_",   totalIndex = 3},
                subAttack      = { delay = nil,  path = "playerUpAttack_",        totalIndex = 3}
    },
    currentMap = nil    -- 현재 맵 ( StageMng.map을 사용하면 되어 지워보려했으나, 지우면 버그가 생기며 왜 생기는지 파악이 안되어 놔둠.)
}

-- 생성자
function PlayerClass:new(currentMap,mainWeapon,subWeapon,o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self
    o:init(currentMap,mainWeapon,subWeapon)
    return o
end

-- 초기 세팅
function PlayerClass:init(currentMap,mainWeapon,subWeapon)
    self.sprite             = cc.Sprite:create("playerUpIdle.png")
    self.currentMap         = currentMap
    self.currentMainWeapon  = mainWeapon
    self.currentSubWeapon   = subWeapon

    -- 공격 애니메이션의 프레임당 진행속도를 공격딜레이에 맞추기 위한 작업
    self.animInfo.upAttack.delay        = self.currentMainWeapon.attackDelay / (self.animInfo.upAttack.totalIndex)
    self.animInfo.leftUpAttack.delay    = self.currentMainWeapon.attackDelay / (self.animInfo.leftUpAttack.totalIndex)
    self.animInfo.leftAttack.delay      = self.currentMainWeapon.attackDelay / (self.animInfo.leftAttack.totalIndex)
    self.animInfo.leftDownAttack.delay  = self.currentMainWeapon.attackDelay / (self.animInfo.leftDownAttack.totalIndex)
    self.animInfo.downAttack.delay      = self.currentMainWeapon.attackDelay / (self.animInfo.downAttack.totalIndex)
    self.animInfo.rightDownAttack.delay = self.currentMainWeapon.attackDelay / (self.animInfo.rightDownAttack.totalIndex)
    self.animInfo.rightAttack.delay     = self.currentMainWeapon.attackDelay / (self.animInfo.rightAttack.totalIndex)
    self.animInfo.rightUpAttack.delay   = self.currentMainWeapon.attackDelay / (self.animInfo.rightUpAttack.totalIndex)
    self.animInfo.subAttack.delay       = self.currentSubWeapon.attackDelay  / (self.animInfo.subAttack.totalIndex)
    -- 애니메이션 등록
    self:enrollAnimation()
    
    -- 공격 게이지 객체 생성
    self.attackGaze   = AttackGazeClass:new()
    self.attackGazeY  = self.sprite:getContentSize().height/2 + 10
    
    -- 현재 무기의 최대 탄환 개수 세팅
    self.maxSubBullet = self.currentSubWeapon.maxBullet
    self.useSubBullet = 0

end

-- 애니메이션 세팅
function PlayerClass:enrollAnimation()
    self.actionList.upWalk          = createAnimation(self.animInfo.upWalk,true)
    self.actionList.upAttack        = createAnimation(self.animInfo.upAttack,false)
    self.actionList.leftUpWalk      = createAnimation(self.animInfo.leftUpWalk,true)
    self.actionList.leftUpAttack    = createAnimation(self.animInfo.leftUpAttack,false)
    self.actionList.leftWalk        = createAnimation(self.animInfo.leftWalk,true)
    self.actionList.leftAttack      = createAnimation(self.animInfo.leftAttack,false)
    self.actionList.leftDownWalk    = createAnimation(self.animInfo.leftDownWalk,true)
    self.actionList.leftDownAttack  = createAnimation(self.animInfo.leftDownAttack,false)
    self.actionList.downWalk        = createAnimation(self.animInfo.downWalk,true)
    self.actionList.downAttack      = createAnimation(self.animInfo.downAttack,false)
    self.actionList.rightDownWalk   = createAnimation(self.animInfo.rightDownWalk,true)
    self.actionList.rightDownAttack = createAnimation(self.animInfo.rightDownAttack,false)
    self.actionList.rightWalk       = createAnimation(self.animInfo.rightWalk,true)
    self.actionList.rightAttack     = createAnimation(self.animInfo.rightAttack,false)
    self.actionList.rightUpWalk     = createAnimation(self.animInfo.rightUpWalk,true)
    self.actionList.rightUpAttack   = createAnimation(self.animInfo.rightUpAttack,false)
    self.actionList.subAttack       = createAnimation(self.animInfo.subAttack,false)
    
    for i,v in pairs(self.actionList) do
        v:retain()
    end
end

-- Joystick 객체의 angle을 토대로 방향 계산 ( 8방향 )
function PlayerClass:calcDirection(angle)
    if angle >= 67.5 and angle < 112.5 then
        self.direction = eDirection.Up
    elseif angle >= 112.5 and angle < 157.5 then
        self.direction = eDirection.LeftUp
    elseif angle >= 157.5 or angle < -157.5 then
        self.direction = eDirection.Left
    elseif angle >= -157.5 and angle < -112.5 then
        self.direction = eDirection.LeftDown
    elseif angle >= -112.5 and angle < -67.5 then
        self.direction = eDirection.Down
    elseif angle >= -67.5 and angle < -22.5 then
        self.direction = eDirection.RightDown
    elseif angle < 22.5 and angle >= -22.5 then
        self.direction = eDirection.Right
    elseif angle >= 22.5 and angle < 67.5 then
        self.direction = eDirection.RightUp
    end
end

-- 데미지를 받는 중인지 체크 (intersectsRect 사용하여 충돌처리)
function PlayerClass:isDamaged(explosionRect)
    local pX,pY = self.sprite:getPosition()
    local rect = cc.rect(pX-self.sizeX/2,pY-self.sizeY/2,self.sizeX,self.sizeY)
    if intersectsRect(explosionRect,rect) then
        self:damaged()
    end
end

-- 공격 받으면 호출하는 함수
function PlayerClass:damaged()
    local cX,cY = self.sprite:getPosition()

    ParticleMng:getParticle(eParticleTag.Blood,cX,cY)
    
    -- 사운드 제거
    if inGameUI.gazeSound ~= nil then
        SoundMng:stopEffect(inGameUI.gazeSound)
        inGameUI.gazeSound = nil
    end

    if self.walkSound ~= nil then
        SoundMng:stopEffect(self.walkSound)
    end

    
    -- 첫 죽음만 계산하기 위함
    if self.state ~= ePlayerState.Death then
        self.state = ePlayerState.Death
        self.attackGaze:setVisible(false)
        inGameUI:nonVisualizeInputUI()
        self.sprite:stopAllActions()

        -- 해골로 변화는 애니메이션
        self.sprite:setTexture("skull.png")
        self.sprite:setPosition(cX,cY+20)
        local fallDown = cc.MoveTo:create(0.4,cc.p(cX,cY-30))
        local easeAction = cc.EaseSineIn:create(fallDown)
        local rotateTo1 = cc.RotateTo:create(0.1,-20)
        local rotateTo2 = cc.RotateTo:create(0.1,20)
        local rotateTo3 = cc.RotateTo:create(0.1,-10)
        local rotateTo4 = cc.RotateTo:create(0.1,0)
        local rotateSequence = cc.Sequence:create(rotateTo1,rotateTo2,rotateTo3,rotateTo4)
        local visualizeDeadUI = cc.CallFunc:create(function () inGameUI.deadUI:visualizeUI() end)
        local resultSequence = cc.Sequence:create(easeAction,rotateSequence,visualizeDeadUI)
        self.sprite:runAction(resultSequence)

        -- 사운드 실행
        SoundMng:stopBGM()
        SoundMng:playEffect(eEffectSoundID.DeadEffect)
    end

end

-- 플레이어의 상태를 토대로 animation에 변화를 주는 함수. (StageScene에서 동작)
function PlayerClass:animation(dt)
    if self.state == ePlayerState.Walk then
        if self.state ~= self.prevState then
            self.walkSound = SoundMng:playEffect(eEffectSoundID.FootWalkEffect,true)
            if self.direction == eDirection.Up then
                changeAction(self.sprite,self.actionList.upWalk)
            elseif self.direction == eDirection.LeftUp then
                changeAction(self.sprite,self.actionList.leftUpWalk)
            elseif self.direction == eDirection.Left then
                changeAction(self.sprite,self.actionList.leftWalk)
            elseif self.direction == eDirection.LeftDown then
                changeAction(self.sprite,self.actionList.leftDownWalk)
            elseif self.direction == eDirection.Down then
                changeAction(self.sprite,self.actionList.downWalk)
            elseif self.direction == eDirection.RightDown then
                changeAction(self.sprite,self.actionList.rightDownWalk)
            elseif self.direction == eDirection.Right then
                changeAction(self.sprite,self.actionList.rightWalk)
            elseif self.direction == eDirection.RightUp then
                changeAction(self.sprite,self.actionList.rightUpWalk)  
            end
            self.prevState = self.state
            -- 다른방향과 연계되어 연속적으로 실행 가능한 경우는 Walk뿐이기에 Walk에서만 prevDirection을 수정한다.
            self.prevDirection = self.direction
        -- 같은경우 ( 방향이 전환되는 경우 )
        else
            if self.direction ~= self.prevDirection then
                if self.direction == eDirection.Up then
                    changeAction(self.sprite,self.actionList.upWalk)
                elseif self.direction == eDirection.LeftUp then
                    changeAction(self.sprite,self.actionList.leftUpWalk)
                elseif self.direction == eDirection.Left then
                    changeAction(self.sprite,self.actionList.leftWalk)
                elseif self.direction == eDirection.LeftDown then
                    changeAction(self.sprite,self.actionList.leftDownWalk)
                elseif self.direction == eDirection.Down then
                    changeAction(self.sprite,self.actionList.downWalk)
                elseif self.direction == eDirection.RightDown then
                    changeAction(self.sprite,self.actionList.rightDownWalk)
                elseif self.direction == eDirection.Right then
                    changeAction(self.sprite,self.actionList.rightWalk)
                elseif self.direction == eDirection.RightUp then
                    changeAction(self.sprite,self.actionList.rightUpWalk)      
                end
                self.prevState = self.state
                self.prevDirection = self.direction
            end
        end
    -- 메인 공격중이라면
    elseif self.state == ePlayerState.MainAttack then
        local pX,pY = self.sprite:getPosition()
        if self.state ~= self.prevState then
            -- 첫 메인공격에 온 것이라면?
            if self.walkSound ~= nil then
                SoundMng:stopEffect(self.walkSound)
            end
            if self.direction == eDirection.Up then
                changeAction(self.sprite,self.actionList.upAttack)
            elseif self.direction == eDirection.LeftUp then
                changeAction(self.sprite,self.actionList.leftUpAttack)
            elseif self.direction == eDirection.Left then
                changeAction(self.sprite,self.actionList.leftAttack)
            elseif self.direction == eDirection.LeftDown then
                changeAction(self.sprite,self.actionList.leftDownAttack)
            elseif self.direction == eDirection.Down then
                changeAction(self.sprite,self.actionList.downAttack)
            elseif self.direction == eDirection.RightDown then
                changeAction(self.sprite,self.actionList.rightDownAttack)
            elseif self.direction == eDirection.Right then
                changeAction(self.sprite,self.actionList.rightAttack)
            elseif self.direction == eDirection.RightUp then
                changeAction(self.sprite,self.actionList.rightUpAttack)  
            end
            self.attackGaze:setVisible(true,pX,pY - self.attackGazeY)
            self.prevState = self.state
        else
            -- 지속적으로 메인공격중이라면 게이지의 프로그래스를 채우는 부분
            self.attackAnimDelay = self.attackAnimDelay + dt
            local currentGaze    = (self.attackAnimDelay / self.currentMainWeapon.attackDelay) * 100 -- 공격이 완료되기까지의 시간과 현재 누적된 시간을 나누어 현재 게이지 퍼센테이지를 구함.
            self.attackGaze.gazeProgress:setPercentage(currentGaze)
            -- 모두 차징 되었다면 발사
            if self.attackAnimDelay >= self.currentMainWeapon.attackDelay then
                WeaponMng:getWeapon(self.currentMainWeapon.tag,pX,pY)
                SoundMng:playEffect(eEffectSoundID.MainAttackEffect)
                self.attackGaze:setVisible(false)
                self.attackGaze.gazeProgress:setPercentage(0)
                inGameUI:nonVisualizeCancelBtnUI(eButtonID.MainAttack)
                -- 발사 완료 시 조이스틱을 움직이는 중이면 walk상태로 아니면 idle상태로 전이
                if inGameUI.joystick.isTouching == false then
                    self.state = ePlayerState.Idle
                else
                    self.state = ePlayerState.Walk
                end

                self.attackAnimDelay = 0
            end
        end
    -- 보조 공격중이라면
    elseif self.state == ePlayerState.SubAttack then
        local pX,pY = self.sprite:getPosition()

        -- 처음 보조 공격으로 들어왔을 경우
        if self.state ~= self.prevState then
            if self.walkSound ~= nil then
                SoundMng:stopEffect(self.walkSound)
            end
            changeAction(self.sprite,self.actionList.subAttack)
            self.attackGaze:setVisible(true,pX,pY - self.attackGazeY)
            self.prevState = self.state
        -- 지속적으로 보조 공격 중인 경우
        else
            self.attackAnimDelay = self.attackAnimDelay + dt
            local currentGaze    = (self.attackAnimDelay / self.currentSubWeapon.attackDelay) * 100
            self.attackGaze.gazeProgress:setPercentage(currentGaze)
            -- 게이지가 차징되면 발사!
            if self.attackAnimDelay >= self.currentSubWeapon.attackDelay then
                WeaponMng:getWeapon(self.currentSubWeapon.tag,pX,pY)
                if inGameUI.gazeSound ~= nil then
                    SoundMng:stopEffect(inGameUI.gazeSound)
                    inGameUI.gazeSound = nil
                end

                SoundMng:playEffect(eEffectSoundID.SubAttackBombEffect)
                self.useSubBullet = self.useSubBullet + 1
                -- 사용 탄수 한발 증가
                inGameUI:updateBulletCount()
                
                -- 최대 발사 개수와 사용 개수가 동일하다면 더이상 사용 불가표시
                if self.useSubBullet == self.maxSubBullet then
                    inGameUI:disabledSubBtn()
                end
                
                self.attackGaze:setVisible(false)
                self.attackGaze.gazeProgress:setPercentage(0)
                inGameUI:nonVisualizeCancelBtnUI(eButtonID.SubAttack)
                -- 발사 완료 시 조이스틱을 움직이는 중이면 walk상태로 아니면 idle상태로 전이
                if inGameUI.joystick.isTouching == false then
                    self.state = ePlayerState.Idle
                else
                    self.state = ePlayerState.Walk
                end
                self.attackAnimDelay = 0
            end
        end
    -- 상태가 넉백중이라면
    elseif self.state == ePlayerState.Knockback then
        if self.state ~= self.prevState then
            if self.walkSound ~= nil then
                SoundMng:stopEffect(self.walkSound)
            end
            self.sprite:stopAllActions()
            if self.knockbackDir == eDirection.Up then
                self.sprite:setTexture("playerDownIdle.png")
            elseif self.knockbackDir == eDirection.Down then
                self.sprite:setTexture("playerUpIdle.png")
            elseif self.knockbackDir == eDirection.Left then
                self.sprite:setTexture("playerRightIdle.png")
                self.direction = eDirection.Right
            elseif self.knockbackDir == eDirection.Right then
                self.sprite:setTexture("playerLeftIdle.png")
            end
            self.sprite:setColor(cc.c4b(255,0,0,1))
            self.prevState = self.state
        end
    -- Idle의 경우
    elseif self.state == ePlayerState.Idle then
        if self.state ~= self.prevState then
            if self.walkSound ~= nil then
                SoundMng:stopEffect(self.walkSound)
            end
            self.sprite:stopAllActions()
            if self.direction == eDirection.Up then
                self.sprite:setTexture("playerUpIdle.png")
            elseif self.direction == eDirection.LeftUp then
                self.sprite:setTexture("playerLeftUpIdle.png")
            elseif self.direction == eDirection.Left then
                self.sprite:setTexture("playerLeftIdle.png")
            elseif self.direction == eDirection.LeftDown  then
                self.sprite:setTexture("playerLeftDownIdle.png")
            elseif self.direction == eDirection.Down then
                self.sprite:setTexture("playerDownIdle.png")
            elseif self.direction == eDirection.RightDown then
                self.sprite:setTexture("playerRightDownIdle.png")
            elseif self.direction == eDirection.Right then
                self.sprite:setTexture("playerRightIdle.png")
            elseif self.direction == eDirection.RightUp then
                self.sprite:setTexture("playerRightUpIdle.png")
            end
            self.prevState = self.state
        end
    end
end

-- 넉백인지 체크하는 함수
function PlayerClass:knockbackCheck()
    local pX, pY     = self.sprite:getPosition()
    local size       = self.sprite:getContentSize()
    local boss       = StageMng.boss
    local playerRect = {x = pX - size.width/2, y = pY - size.height/2,width = size.width, height = size.height}

    -- 보스가 nil이 아니라면 ( 죽지 않았다면 )
    if boss ~= nil then
        local bX,bY = boss.sprite:getPosition()

        if boss.pivotY ~= nil then
            bY = bY - boss.pivotY
        end
        -- 보스의 넉백 범위 Rect
        local bossRect = {x = bX - boss.sizeX/2, y = bY - boss.sizeY/2, width = boss.sizeX, height = boss.sizeY}

        -- 두 Rect가 포개어지는지 확인
        if intersectsRect(playerRect,bossRect) then
            if self.state == ePlayerState.MainAttack or self.state == ePlayerState.SubAttack then
                if self.state == ePlayerState.MainAttack then
                    inGameUI:nonVisualizeCancelBtnUI(eButtonID.MainAttack)
                else
                    inGameUI:nonVisualizeCancelBtnUI(eButtonID.SubAttack)
                end
                if inGameUI.gazeSound ~= nil then
                    SoundMng:stopEffect(inGameUI.gazeSound)
                    inGameUI.gazeSound = nil
                end
                self.attackGaze:setVisible(false)
                self.attackGaze.gazeProgress:setPercentage(0)
                self.attackAnimDelay = 0
            end
            -- 넉백으로 전이
            self.state = ePlayerState.Knockback

            -- 보스와 현재 플레이어의 위치를 통해 플레이가 튕겨야하는 방향을 설정.
            local angle = math.atan2(pY - bY,pX - bX)
            angle = angle * 180 / math.pi
            if angle >= -45 and angle < 45 then
                self.knockbackDir = eDirection.Right
                self.direction = eDirection.Left
            elseif angle >= 45 and angle < 135 then
                self.knockbackDir = eDirection.Up
                self.direction = eDirection.Down
            elseif angle >= 135 or angle < -135 then
                self.knockbackDir = eDirection.Left
                self.direction = eDirection.Right
            elseif angle >= -135 and angle < -45 then
                self.knockbackDir = eDirection.Down
                self.direction = eDirection.Up
            end
        end
    end
end

-- 이동 함수 ( StageScene에서 돌아감. )
function PlayerClass:action (horizonVal,verticalVal,dt)
    local pX, pY = self.sprite:getPosition()

    if self.state == ePlayerState.Walk or self.state == ePlayerState.SubAttack or self.state == ePlayerState.Knockback  then
        
        -- 넉백중이라면 이동 값을 knockbackPower로 대입
        if self.state == ePlayerState.Knockback then
            self.currentDuration = self.currentDuration - dt
            if self.knockbackDir == eDirection.Up then
                verticalVal = self.knockbackPower
            elseif self.knockbackDir == eDirection.Down then
                verticalVal = -self.knockbackPower
            elseif self.knockbackDir == eDirection.Left then
                horizonVal = -self.knockbackPower
            elseif self.knockbackDir == eDirection.Right then
                horizonVal = self.knockbackPower
            end
            if self.currentDuration <= 0 then
                self.currentDuration = self.knockbackDuration
                -- 넉백 시 플레이어를 빨갛게 표시한다.
                self.sprite:setColor(cc.c4b(255,255,255,1))
                if inGameUI.joystick.horizonVal ~= 0 or inGameUI.joystick.verticalVal ~= 0 then
                    self.state = ePlayerState.Walk
                else
                    self.state = ePlayerState.Idle
                end
            end
        end

        -- 맵과의 충돌 처리 부분
        if pX - self.sprite:getContentSize().width/2 <= self.currentMap.minX then
            if horizonVal < 0 then
                horizonVal = 0
            end
        end
        if pX + self.sprite:getContentSize().width/2 >= self.currentMap.maxX then
            if horizonVal > 0 then
                horizonVal = 0
            end
        end

        -- 15 = 어느정도 발은 맵에 위치하도록 조정하는 매직넘버
        if pY - self.sprite:getContentSize().height/2 + 15 >= self.currentMap.maxY then
            if verticalVal > 0 then
                verticalVal = 0
            end
        end
        if pY - self.sprite:getContentSize().height/2 <= self.currentMap.minY then
            if verticalVal < 0 then
                verticalVal = 0
            end
        end
        
        -- 보조 공격중에는 보조공격의 무게에 따라 속도가 느려진다.
        local resultSpeed = self.moveSpeed
        if self.state == ePlayerState.SubAttack then
            resultSpeed = resultSpeed / self.currentSubWeapon.weight
            self.attackGaze.gazeLayer:setPosition(pX,pY - self.attackGazeY)
        end
        
        -- 조이스틱이 원점이 아닐 경우에만 움직임.
        self.sprite:setPosition(pX + (horizonVal*resultSpeed*dt), pY + (verticalVal*resultSpeed*dt))
    end
end