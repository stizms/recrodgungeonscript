BaseProjectileClass = {
    sprite       = nil, 
    tag          = nil,           -- 투사체 종류
    moveSpeed    = 0,
    dirVec       = {x= 0, y = 0}, -- 진행 방향
    bodyAction   = nil,
    firstSprite  = nil,
    animInfo     = {delay = nil, path= nil, totalIndex = nil},  -- 투사체 애니메이션 정보
}

function BaseProjectileClass:new(o)
    o = o or {}
    setmetatable(o,self)
    self.__index = self
    o.dirVec = {x = 0, y = 0}
    o.animInfo = {delay = nil, path = nil, totalIndex = nil}
    o.particleInfo = {delay = nil, path = nil, totalIndex = nil}
    o:init()
    return o
end

-- 초기화 함수
function BaseProjectileClass:init()
    print("projectile Parent")
end

-- 애니메이션 등록함수
function BaseProjectileClass:enrollAnimation()
    if self.animInfo.path == nil then
        print("Animation Information is not setting")
        do return end
    else
        if self.tag == eProjectileID.Circle then
            self.bodyAction = createAnimation(self.animInfo,false)
        elseif self.tag == eProjectileID.Cross then
            self.bodyAction = createAnimation(self.animInfo,true)
        end
        self.bodyAction:retain()
    end
end

-- 충돌 체크 함수
function BaseProjectileClass:collisionCheck()
    local cX,cY = self.sprite:getPosition()
    local size  = self.sprite:getContentSize()

    -- 플레이어와 부딪히는 경우
    if StageMng.player.state ~= ePlayerState.Death and StageMng.boss.state ~= eMonsterState.Death then
        local player = StageMng.player
        local pX,pY = player.sprite:getPosition()
        
        local playerRect = {x = pX - player.sizeX/2, y = pY - player.sizeY/2,width = player.sizeX, height = player.sizeY}
        local bulletRect = {x = cX - size.width/2, y = cY - size.height/2, width = size.width, height = size.height }

        if intersectsRect(playerRect,bulletRect) then
            StageMng.player:damaged()
            self:removeProjectile()
        end
    end

    local currentMap = StageMng.map

    -- 맵과 부딪히는 경우
    if cX - size.width/2 <= currentMap.minX or
        cX + size.width/2 >= currentMap.maxX or
        cY - size.height/2 >= currentMap.maxY or
        cY - size.height/2 <= currentMap.minY then
            self:removeProjectile()
    end
end

-- 총알 제거 함수 ( 풀로 돌려 놓는 함수 )
function BaseProjectileClass:removeProjectile()
    self.sprite:setVisible(false)
    self.sprite:stopAction(self.bodyAction)
    self.sprite:setTexture(self.firstSprite)
    table.remove(ProjectileMng.usingProjectilePool,tableFind(ProjectileMng.usingProjectilePool,self))
end