CircleProjectile = BaseProjectileClass:new()

-- 초기화 함수
function CircleProjectile:init()
    self.tag                 = eProjectileID.Circle
    self.sprite              = cc.Sprite:create("circleProjectile_0.png")
    self.firstSprite         = "circleProjectile_0.png"
    self.moveSpeed           = 300
    self.animInfo.delay      = 0.15
    self.animInfo.path       = "circleProjectile_"
    self.animInfo.totalIndex = 6
    self:enrollAnimation()
end

-- 업데이트 함수 (StageScene에서 돌아감)
function CircleProjectile:update(dt)
    local cX,cY = self.sprite:getPosition()
    self.sprite:setPosition(cX + (self.dirVec.x * self.moveSpeed * dt), cY + (self.dirVec.y * self.moveSpeed * dt))
    self:collisionCheck()
end