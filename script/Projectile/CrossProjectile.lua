CrossProjectile = BaseProjectileClass:new()

-- 초기화 함수
function CrossProjectile:init()
    self.tag                 = eProjectileID.Cross
    self.sprite              = cc.Sprite:create("crossProjectile_0.png")
    self.firstSprite         = "crossProjectile_0.png"
    self.moveSpeed           = 300
    self.animInfo.delay      = 0.1
    self.animInfo.path       = "crossProjectile_"
    self.animInfo.totalIndex = 2
    self:enrollAnimation()
end

-- 업데이트 함수 ( StageScene에서 돌아감 )
function CrossProjectile:update(dt)
    local cX,cY = self.sprite:getPosition()
    self.sprite:setPosition(cX + (self.dirVec.x * self.moveSpeed * dt), cY + (self.dirVec.y * self.moveSpeed * dt))
    self:collisionCheck()
end