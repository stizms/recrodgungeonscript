require "script.UI.Lobby.LobbyHome"
require "script.UI.Lobby.LobbyShop"
require "script.UI.Lobby.LobbyEquipment"
require "script.UI.Lobby.LobbySetting"

-- 로비 씬
function createLobbyScene()
    local scene = cc.Scene:create()
    scene:addChild(createLobbyLayer())
    SoundMng:playBGM(eBGMSoundID.LobbyBGM)
    return scene
end

function createLobbyLayer()
    local lobbyLayer   = cc.Layer:create()
    local currentLayer = nil
    local layerTitle   = cc.Label:createWithTTF("스테이지","BMFont.ttf",60)

    -- 로비 백그라운드 생성
    local lobbyBG = cc.Sprite:create("lobbyBackGround.png")
    lobbyBG:setAnchorPoint(0.5,0.5)
    lobbyBG:setScale(1.27)
    lobbyBG:setPosition(visibleSize.width/2,visibleSize.height/2)
    lobbyLayer:addChild(lobbyBG)

    -- 자식 레이어들 생성
    local homeLayer = LobbyHome:new(lobbyLayer)
    homeLayer:visualizeUI()
    currentLayer = homeLayer
    local shopLayer      = LobbyShop:new(lobbyLayer)
    local equipmentLayer = LobbyEquipment:new(lobbyLayer,layerTitle)
    local settingLayer   = LobbySetting:new(lobbyLayer)

    -- 외부 구성요소 생성
    local upTextureBG = cc.Sprite:create("TapBG.png")
    upTextureBG:setAnchorPoint(0.5,1)
    upTextureBG:setPosition(visibleSize.width/2,visibleSize.height)
    lobbyLayer:addChild(upTextureBG)

    local upTextureBorder = cc.Sprite:create("slideFrame.png")
    upTextureBG:addChild(upTextureBorder)
    upTextureBorder:setScaleX(1.5)
    upTextureBorder:setScaleY(-1.5)
    upTextureBorder:setAnchorPoint(0.5,0)
    upTextureBorder:setPosition(upTextureBG:getContentSize().width/2,24)

    local downTextureBorder = cc.Sprite:create("slideFrame.png")
    upTextureBG:addChild(downTextureBorder)
    downTextureBorder:setScale(1.5)
    downTextureBorder:setAnchorPoint(0.5,0)
    downTextureBorder:setPosition(upTextureBG:getContentSize().width/2,0)

    layerTitle:setColor(cc.c3b(255,255,255))
    upTextureBG:addChild(layerTitle)
    local textureSize = upTextureBG:getContentSize()
    layerTitle:setPosition(textureSize.width /2, textureSize.height/2)

    equipmentLayer.equipmentInsideUI:enrollBackBtn(upTextureBG)
    
    local buttonBG = cc.Sprite:create("TapBG.png")
    buttonBG:setAnchorPoint(0.5,0)
    buttonBG:setPosition(visibleSize.width/2,0)
    lobbyLayer:addChild(buttonBG)
    
    local buttonBGUpFrame = cc.Sprite:create("slideFrame.png")
    buttonBGUpFrame:setAnchorPoint(0.5,0)
    buttonBGUpFrame:setScaleX(1.5)
    buttonBGUpFrame:setScaleY(-1.5)
    buttonBGUpFrame:setPosition(visibleSize.width/2,buttonBG:getContentSize().height)
    lobbyLayer:addChild(buttonBGUpFrame)

    local buttonBGDownFrame = cc.Sprite:create("slideFrame.png")
    buttonBGDownFrame:setAnchorPoint(0.5,0)
    buttonBGDownFrame:setScale(1.5)
    buttonBGDownFrame:setPosition(visibleSize.width/2,buttonBG:getContentSize().height-24)
    lobbyLayer:addChild(buttonBGDownFrame)

    local currentLayerBar = cc.Sprite:create("currentLayerBar.png")
    currentLayerBar:setScale(1.2)
    buttonBG:addChild(currentLayerBar)
    currentLayerBar:setPosition(115,10)

    -- 버튼 생성 함수
    local function createBtn(tag,x,y)
        local btn = nil
        if tag == eLobbyLayerID.Home then
            btn = ccui.Button:create("HomeBtn.png","HomeBtn.png")
        else
            local btnName = nil
            if tag == eLobbyLayerID.Shop then
                btn = ccui.Button:create("ShopBtn.png","ShopBtn.png")
                btnName = "상점"
            elseif tag == eLobbyLayerID.Equipment then
                btn = ccui.Button:create("EquipmentBtn.png","EquipmentBtn.png")
                btnName = "장비"
            elseif tag == eLobbyLayerID.Setting then
                btn = ccui.Button:create("SettingBtn.png","SettingBtn.png")
                btnName = "설정"
            end
            local btnLabel = cc.Label:createWithTTF(btnName,"BMFont.ttf",35)
            btnLabel:setColor(cc.c3b(208,104,32))
            btn:addChild(btnLabel)
            if tag == eLobbyLayerID.Shop then
                btnLabel:setPosition(btn:getContentSize().width/2+4,-20)
            else
                btnLabel:setPosition(btn:getContentSize().width/2,-20)
            end
            btn:setScale(0.7)
        end
    
        btn:addTouchEventListener(
            function (sender,type)
                if popUpUI.isVisible == false then
                    if type == ccui.TouchEventType.began then
                        btn:setColor(cc.c3b(150,150,150))
                    elseif type == ccui.TouchEventType.canceled then
                        btn:setColor(cc.c3b(255,255,255))
                    elseif type == ccui.TouchEventType.ended then
                        btn:setColor(cc.c3b(255,255,255))
                        currentLayer:nonVisualizeUI()
                        local btnPosX,btnPosY = btn:getPosition()
                        if tag == eLobbyLayerID.Home then
                            currentLayer = homeLayer 
                            layerTitle:setString("스테이지")
                            btnPosY = btnPosY - btn:getContentSize().height/2
                        else
                            if tag == eLobbyLayerID.Shop then
                                currentLayer = shopLayer
                                layerTitle:setString("상점")
                            elseif tag == eLobbyLayerID.Equipment then
                                currentLayer = equipmentLayer
                                layerTitle:setString("장비")
                            elseif tag == eLobbyLayerID.Setting then
                                currentLayer = settingLayer
                                layerTitle:setString("설정")
                            end 
                            btnPosY = btnPosY - btn:getContentSize().height/2 - 15
                        end

                        if equipmentLayer.isUsingInside == true then
                            equipmentLayer.equipmentInsideUI:nonVisualizeUI()
                        end

                        SoundMng:playEffect(eEffectSoundID.BtnEffect)
                        currentLayerBar:setPosition(btnPosX, btnPosY)
                        currentLayer:visualizeUI()
                    end
                end
            end
        )
        buttonBG:addChild(btn)
        btn:setPosition(x,y)
    end

    -- 버튼 사이에 구분선 생성 함수
    local function createIntersectsBar(x,y)
        local intersectsBar = cc.Sprite:create("IntersectsBar.png")
        intersectsBar:setPosition(x,y)
        intersectsBar:setScale(1.5)
        buttonBG:addChild(intersectsBar)
    end

    local yPos = buttonBG:getContentSize().height/2 - 6
    createBtn(eLobbyLayerID.Home,115,yPos)
    createIntersectsBar(202,yPos)
    createBtn(eLobbyLayerID.Shop,290,yPos+15)
    createIntersectsBar(377,yPos)
    createBtn(eLobbyLayerID.Equipment,465,yPos+15)
    createIntersectsBar(552,yPos)
    createBtn(eLobbyLayerID.Setting,640,yPos+15)

    return lobbyLayer
end