-- 스테이지 씬
function createStageScene(stageID)
    local scene = cc.Scene:create()
    scene:addChild(createStageLayer(stageID))
    SoundMng:stopBGM()
    return scene
end

function stageInit(stageLayer,stageID)
    -- 해당 스테이지 씬에서만 사용되는 싱글톤 초기화
    StageMng      = StageMngClass:new(stageLayer)
    WeaponMng     = WeaponMngClass:new()
    ParticleMng   = ParticleMngClass:new()
    MonsterMng    = MonsterMngClass:new()
    ProjectileMng = ProjectileMngClass:new()

    -- 메인 무기와 보조 무기 인스턴싱
    local mainWeapon = WeaponMng:getWeaponInstance(UserDataMng.currentMainWeapon)
    local subWeapon  = WeaponMng:getWeaponInstance(UserDataMng.currentSubWeapon)

    -- 맵, 플레이어, 보스, mainLayer 등 등록
    StageMng:settingStage(stageID,mainWeapon,subWeapon)

    -- UI 등록
    inGameUI = InGameUIClass:new(StageMng.player)

end

-- 스테이지1 레이어
function createStageLayer(stageID)
    stageLayer = cc.Layer:create()
    stageInit(stageLayer,stageID)

    -- 아래는 이벤트 기능
    -- 터치 이벤트
    local joystickTouchID = nil
    local function onTouchesBegan(touches, event)
        if StageMng.isGaming == true and inGameUI.canInput == true then
            for i,v in pairs(touches) do
                local location = v:getLocation()
                local rect = inGameUI.joystick.joystickBg:getBoundingBox()
                if joystickTouchID == nil then
                    joystickTouchID = v:getId()
                    -- 조이스틱의 위치 수정
    
                    --Start 조이스틱 Func
                    inGameUI.joystick.currentPos.x = location.x
                    inGameUI.joystick.currentPos.y = location.y
                    inGameUI.joystick.joystickBg:setPosition(inGameUI.joystick.currentPos)
                    inGameUI.joystick.joystickCenter:setPosition(inGameUI.joystick.currentPos)
                    inGameUI.joystick.isTouching = true
                end   
            end
        end
    end
    
    local function onTouchesMoved(touches, event)
        if StageMng.isGaming == true and inGameUI.canInput == true then
            if joystickTouchID ~= nil then
                for i,v in pairs(touches) do
                    if v:getId() == joystickTouchID then
                        local location = v:getLocation()
                        inGameUI.joystick:stickMove(location)
                        StageMng.player:calcDirection(inGameUI.joystick.angle)
                        if StageMng.player.state ~= ePlayerState.MainAttack and StageMng.player.state ~= ePlayerState.SubAttack and StageMng.player.state ~= ePlayerState.Knockback then
                            StageMng.player.state = ePlayerState.Walk
                        end
                    end
                end
            end
        end
    end

    local function onTouchesEnded(touches, event)
        if StageMng.isGaming == true and inGameUI.canInput == true then
            if joystickTouchID ~= nil then
                for i,v in pairs(touches) do
                    if v:getId() == joystickTouchID then
                        if StageMng.player.state ~= ePlayerState.MainAttack and StageMng.player.state ~= ePlayerState.SubAttack and StageMng.player.state ~= ePlayerState.Knockback then
                            StageMng.player.state = ePlayerState.Idle
                        end
                        inGameUI.joystick:valueReset()
                        inGameUI.joystick.isTouching = false
                        joystickTouchID = nil
                    end
                end
            end
        end
    end

    -- 스테이지 내에 모든 객체 업데이트 돌려주는 함수
    local function update(dt)
        StageMng.player:animation(dt)
        if StageMng.isGaming == true then
            if StageMng.player.state ~= ePlayerState.Knockback then
                StageMng.player:knockbackCheck()
            end
            StageMng.player:action(inGameUI.joystick.horizonVal,inGameUI.joystick.verticalVal,dt)
            for i,v in pairs(WeaponMng.usingWeaponPool) do
                v:update(dt)
            end
            for i,v in pairs(ParticleMng.usingParticlePool) do
                v:update(dt)
            end
            for i,v in pairs(MonsterMng.usingMonsterPool) do
                v:update(dt)
            end
            for i,v in pairs(ProjectileMng.usingProjectilePool) do
                v:update(dt)
            end

            if inGameUI.bossInfo.isDamaging == true then
                inGameUI.bossInfo:updateBossHpGaze(dt)
            end
        end
    end

    local startDelay    = 1
    local isStart       = false
    local isIntroing    = false
    local introSchedule = nil

    -- 인트로 애니메이션 돌리는 함수
    local function introAnim(dt)
        if isStart == false then
            startDelay = startDelay - dt
            if startDelay < 0 then
                isStart = true  
                inGameUI.introLayer.mainLayer:setVisible(true)  
            end
        else
            local currentOpacity = inGameUI.introLayer.mainLayer:getOpacity()
            currentOpacity = currentOpacity + (200 * dt)
            inGameUI.introLayer.mainLayer:setOpacity(currentOpacity)
            if currentOpacity > 200 then
                inGameUI.introLayer:visualizeUI()
                cc.Director:getInstance():getScheduler():unscheduleScriptEntry(introSchedule)
            end
        end
    end

    introSchedule = cc.Director:getInstance():getScheduler():scheduleScriptFunc(introAnim,0,false)
    StageMng.stageSchedule = cc.Director:getInstance():getScheduler():scheduleScriptFunc(update, 0, false)
    
    local listener = cc.EventListenerTouchAllAtOnce:create()
    listener:registerScriptHandler(onTouchesBegan,cc.Handler.EVENT_TOUCHES_BEGAN )
    listener:registerScriptHandler(onTouchesMoved,cc.Handler.EVENT_TOUCHES_MOVED )
    listener:registerScriptHandler(onTouchesEnded,cc.Handler.EVENT_TOUCHES_ENDED )
    local eventDispatcher = stageLayer:getEventDispatcher()
    eventDispatcher:addEventListenerWithSceneGraphPriority(listener, stageLayer)

    return stageLayer
end