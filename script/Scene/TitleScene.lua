-- 타이틀 씬
function createTitleScene()
    local scene = cc.Scene:create()
    scene:addChild(createTitleLayer())
    SoundMng:playBGM(eBGMSoundID.TitleBGM)
    return scene
end

-- create Layer
function createTitleLayer()
    local titleLayer = cc.Layer:create()

    -- 뒷 배경
    local titleBackground = cc.Sprite:create("Title_Background.png")
    titleBackground:setPosition(visibleSize.width /2, visibleSize.height / 2)
    titleBackground:setScale(2.2)
    titleLayer:addChild(titleBackground)

    -- 타이틀 플레이어 애니메이션
    local titleAnimInfo = { delay = 0.2, path = "playerDownAttack_", totalIndex = 3}
    local titlePlayer = cc.Sprite:create("TitlePlayer0.png")
    titlePlayer:setPosition(visibleSize.width /2, visibleSize.height / 2 )
    titlePlayer:setScale(7)
    local titleAction = createAnimation(titleAnimInfo,true)
    titlePlayer:runAction(titleAction)
    titleLayer:addChild(titlePlayer)

    -- title Label
    local titleLabel1 = cc.Label:createWithTTF("Record","RetroFont2.ttf",110)
    titleLabel1:setPosition(visibleSize.width/2, visibleSize.height/2 +420)
    titleLayer:addChild(titleLabel1)

    local titleLabel2 = cc.Label:createWithTTF("Gungeon","RetroFont2.ttf",130)
    titleLabel2:setPosition(visibleSize.width/2, visibleSize.height/2 +350)
    titleLayer:addChild(titleLabel2)

    -- gameStart Label
    local startLabel = cc.Label:createWithTTF("Game Start !", "RetroFont2.ttf",80)
    startLabel:setPosition(visibleSize.width/2, visibleSize.height/2 -360)
    titleLayer:addChild(startLabel)

    -- Game Start! 로고 관련 변수
    local isFadeIn = false
    local opacity = 255

    -- 업데이트 함수
    local function titleUpdate(dt)
        -- Game Start Logo Fade 함수
        local function logoFade()
            local plusMinusVal = 5
            if isFadeIn == false then
                opacity = opacity - plusMinusVal
                startLabel:setOpacity(opacity)
                if opacity <= 80 then
                    isFadeIn = true
                end
            else
                opacity = opacity + plusMinusVal
                startLabel:setOpacity(opacity)
                if opacity >= 255 then
                    isFadeIn = false
                end
            end
        end
        logoFade()
    end

    local titleSchedule = cc.Director:getInstance():getScheduler():scheduleScriptFunc(titleUpdate, 0, false)

    -- 터치함수
    local function onTouchBegan(touch, event)
        return true
    end
    local function onTouchEnded(touch, event)
        cc.Director:getInstance():getScheduler():unscheduleScriptEntry(titleSchedule)   -- 스케줄 제거
        SoundMng:playEffect(eEffectSoundID.TitleClickEffect)
        SceneMng:changeScene(eScene.Lobby)
    end

    local listener = cc.EventListenerTouchOneByOne:create()
    listener:registerScriptHandler(onTouchBegan,cc.Handler.EVENT_TOUCH_BEGAN )
    listener:registerScriptHandler(onTouchEnded,cc.Handler.EVENT_TOUCH_ENDED )
    listener:setSwallowTouches(true)
    local eventDispatcher = titleLayer:getEventDispatcher()
    eventDispatcher:addEventListenerWithSceneGraphPriority(listener, titleLayer)

    return titleLayer
end