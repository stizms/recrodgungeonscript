monsterTb = {
    GhostBoss = {monsterName = "REAPER", monsterNameH = "사신", monsterIcon = "GhostMonsterIcon.png",
    maxHp = 200, moveSpeed = 200, sizeX = 110, sizeY = 200, damagedSizeX = 180, damagedSizeY = 290, subDamagedSizeX = 150, subDamagedSizeY = 260,
    pivotY = nil, projectilePos = {x = 70, y = 0}, projectile = eProjectileID.Circle},
    GoatBoss = {monsterName = "CAPRICORN", monsterNameH = "마갈궁", monsterIcon = "GoatMonsterIcon.png",
    maxHp = 200, moveSpeed = 200, sizeX = 70, sizeY = 160, damagedSizeX = 160, damagedSizeY = 290, subDamagedSizeX = 120, subDamagedSizeY = 250,
    pivotY = 75, projectilePos = {x = 68, y = 90}, projectile = eProjectileID.Cross}
}