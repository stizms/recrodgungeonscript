stageTb = {
    {stageID = eStageID.S1, stageName = "던전 초입", nextStage = eStageID.S2, monsterID = eMonsterID.GoatBoss, scene = eScene.FirstStage, rewardGold = 20,
     minX = 23, maxX = 705, minY = 18, maxY = 1160, mapData = "firstStageMap.tmx", mapName = "firstStageMap.png", mapSprite = "firstMapIcon.png",stageAcronym = "D-1" },
    {stageID = eStageID.S2, stageName = "유령의 집", nextStage = nil, monsterID = eMonsterID.GhostBoss, scene = eScene.SecondStage, rewardGold = 30,
    minX = 23, maxX = 705, minY = 18, maxY = 1110, mapData = "secondStageMap.tmx", mapName = "secondStageMap.png", mapSprite = "secondMapIcon.png",stageAcronym = "D-2" }
}

function stageTb:getStageInfo(stageID)
    for i = 1, #self do
        if self[i].stageID == stageID then
            return self[i]
        end
    end
    print("Doesn't have stage")
end