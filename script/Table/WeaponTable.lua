weaponTb = {
    NormalBomb      = {weaponName = "폭탄",         iconName = "normalBomb_Icon.png", damage = 25, attackDelay = 0.5, bombAmplitude = 10, bombDelay = 2, attackRange = 70, maxBullet = -1,
                        description = "가장 기본이 되는 폭탄으로 설치 후 일정 시간 뒤에 폭발한다." },
    DynamiteStick   = {weaponName = "다이너마이트",  iconName = "dynamiteStick_Icon.png", damage = 35, attackDelay = 0.5, bombAmplitude = 10, bombDelay = 2, attackRange = 100, maxBullet = -1,
                        description = "설치 후 일정 시간 뒤에 폭발하여 넓은 범위에 큰 피해를 준다." },
    FireBall        = {weaponName = "파이어볼",      iconName = "fireBall_Icon.png", damage = 50, attackDelay = 2, bombAmplitude = 20, attackRange = 100, moveSpeed = 300, weight = 2, maxBullet = 3,
                        description = "불덩어리를 발사하여 거리가 먼 적에게 강력한 피해를 줄 수 있다." },
    IceBall         = {weaponName = "아이스볼",      iconName = "IceBall_Icon.png", damage = 20, attackDelay = 2, bombAmplitude = 20, attackRange = 100, moveSpeed = 300, weight = 2, maxBullet = 3,
                        description = "얼음 덩어리를 발사하여 거리가 먼 적에게 약간의 피해를 주고, 일정 시간 얼어붙게 한다." }
}