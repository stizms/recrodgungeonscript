BaseWeaponSlotClass = {
    tag             = nil, 
    parent          = nil, 
    mainBG          = nil, 
    weaponIcon      = nil, 
    currentWeaponID = nil, 
    equippedIcon    = nil
}

-- 슬롯 생성 함수
function BaseWeaponSlotClass:new(scrollView,tag,weaponID,parent,o)
    o = o or {}
    setmetatable(o,self)
    self.__index = self
    o:init(scrollView,tag,weaponID,parent)
    return o
end

-- 슬롯 세팅 함수
function BaseWeaponSlotClass:init(scrollView,tag,weaponID,parent)
    self.tag             = tag
    self.currentWeaponID = weaponID
    self.parent          = parent
    
    self.mainBG = ccui.Button:create("WeaponSlot.png","WeaponSlot.png")
    self.mainBG:addTouchEventListener(
        function(sender,type)
            if type == ccui.TouchEventType.began then
                self.mainBG:setColor(cc.c3b(150,150,150))
                SoundMng:playEffect(eEffectSoundID.BtnEffect)
            elseif type == ccui.TouchEventType.canceled then
                self.mainBG:setColor(cc.c3b(255,255,255))
            elseif type == ccui.TouchEventType.ended then
                self.mainBG:setColor(cc.c3b(255,255,255))
                if self.tag == eWeaponSlotTag.InGameMain then
                    if UserDataMng.currentMainWeapon ~= self.currentWeaponID then
                        UserDataMng.currentMainWeapon = self.currentWeaponID
                        self.parent:updateMainSlotList()
                        UserDataMng:saveUserData()
                    end
                elseif self.tag == eWeaponSlotTag.InGameSub then
                    if UserDataMng.currentSubWeapon ~= self.currentWeaponID then
                        UserDataMng.currentSubWeapon = self.currentWeaponID
                        self.parent:updateSubSlotList()
                        UserDataMng:saveUserData()
                    end
                elseif self.tag == eWeaponSlotTag.LobbyMain then
                    if UserDataMng.currentMainWeapon ~= self.currentWeaponId then
                        UserDataMng.currentMainWeapon = self.currentWeaponID
                        self.parent:updateMainSlotList()
                        self.parent:updateInfo(self.currentWeaponID)
                        UserDataMng:saveUserData()
                    end
                elseif self.tag == eWeaponSlotTag.LobbySub then
                    if UserDataMng.currentSubWeapon ~= self.currentWeaponId then
                        UserDataMng.currentSubWeapon = self.currentWeaponID
                        self.parent:updateSubSlotList()
                        self.parent:updateInfo(self.currentWeaponID)
                        UserDataMng:saveUserData()
                    end
                end
            end
        end
    )

    self.weaponIcon = cc.Sprite:create(weaponTb[self.currentWeaponID].iconName)
    self.mainBG:addChild(self.weaponIcon)
    local size = self.mainBG:getContentSize()
    self.weaponIcon:setPosition(size.width/2,size.height/2)

    self.equippedIcon = cc.Sprite:create("EquippedCheck.png")
    self.mainBG:addChild(self.equippedIcon)
    self.equippedIcon:setPosition(10,self.mainBG:getContentSize().height-20)
    self.equippedIcon:setVisible(false)

    scrollView:addChild(self.mainBG)
end

-- 슬롯 갱신
function BaseWeaponSlotClass:updateSlot()
    if UserDataMng.currentMainWeapon == self.currentWeaponID or UserDataMng.currentSubWeapon == self.currentWeaponID then
        self:visualizeEquippedIcon()
    else
        self:nonVisualizeEquippedIcon()
    end
end

-- 장비 착용 이미지 키는 함수
function BaseWeaponSlotClass:visualizeEquippedIcon()
    self.equippedIcon:setVisible(true)
end

-- 장비 착용 이미지 끄는 함수
function BaseWeaponSlotClass:nonVisualizeEquippedIcon()
    self.equippedIcon:setVisible(false)
end