BossInfoUIClass = {
    bossInfoLayer    = nil,
    firstHpProgress  = nil,   -- 첫번째 게이지
    secondHpProgress = nil,   -- 두번째 게이지
    bossNameText     = nil,
    isDamaging       = false, -- hp 닳게 하고 있는 중인지 Flag 
    downHpValue      = nil,   -- 목표지점까지 떨어지는 속도 측정을 위함 ( 현재 게이지 - 도착한 뒤의 게이지) [ 보스가 피격 당할때 Hp를 깎는 부분에서 입력 됨.]
    gazePerHp        = nil,   -- 게이지 1당 체력이 차지하는 비율
    currentGaze      = nil,   -- 현재 게이지
    bossHpAnimFunc   = nil,   -- hp깎이는 동작
    bossHpScheduler  = nil 
}

function BossInfoUIClass:new(o)
    o = o or {}
    setmetatable(o,self)
    self.__index = self
    return o
end

-- Hp게이지 세팅
function BossInfoUIClass:createBossInfo()
    self.bossInfoLayer = cc.Layer:create()
   
    local hpGazeBG = cc.Sprite:create("emptyHpGaze.png")
    hpGazeBG:setPosition(visibleSize.width/2,visibleSize.height - 30)
    hpGazeBG:setScaleX(1.3)
    hpGazeBG:setScaleY(1.5)
    local BGSize = hpGazeBG:getContentSize()

    -- 첫번째 게이지
    local firstHpGaze = cc.Sprite:create("firstHpGaze.png")
    firstHpGaze:setScaleX(1.3)
    firstHpGaze:setScaleY(1.5)

    self.firstHpProgress = cc.ProgressTimer:create(firstHpGaze)
    self.firstHpProgress:setType(cc.PROGRESS_TIMER_TYPE_BAR)
    self.firstHpProgress:setBarChangeRate(cc.p(1,0))
    self.firstHpProgress:setMidpoint(cc.p(0,0.5))
    self.firstHpProgress:setPercentage(0)
    self.firstHpProgress:setPosition(BGSize.width/2,BGSize.height/2)
    hpGazeBG:addChild(self.firstHpProgress)

    -- 두번째 게이지
    local secondHpGaze = cc.Sprite:create("secondHpGaze.png")
    secondHpGaze:setScaleX(1.3)
    secondHpGaze:setScaleY(1.5)

    self.secondHpProgress = cc.ProgressTimer:create(secondHpGaze)
    self.secondHpProgress:setType(cc.PROGRESS_TIMER_TYPE_BAR)
    self.secondHpProgress:setBarChangeRate(cc.p(1,0))
    self.secondHpProgress:setMidpoint(cc.p(0,0.5))
    self.secondHpProgress:setPercentage(0)
    self.secondHpProgress:setPosition(BGSize.width/2,BGSize.height/2)
    hpGazeBG:addChild(self.secondHpProgress)

    -- 보스의 최대 체력이 100보다 크면 2개의 게이지 모두 사용
    if StageMng.boss.maxHp > 100 then
        self.currentGaze = 200
        self.gazePerHp = 200/StageMng.boss.maxHp
    -- 100보다 작으면 1개의 게이지로 표현
    else 
        self.currentGaze = 100
        self.gazePerHp = 100/StageMng.boss.maxHp
    end

    -- 외부에서 스케줄을 등록시켜주기 위해 함수를 담아 둠
    self.bossHpAnimFunc = hpAnim

    self.bossInfoLayer:addChild(hpGazeBG)

    -- 보스 이름
    self.bossNameText = cc.Label:createWithTTF("","BMFont.ttf",25)
    self.bossNameText:setAnchorPoint(0,0.5)
    self.bossNameText:setPosition(50,visibleSize.height - 65)
    self.bossNameText:setVisible(false)
    self.bossInfoLayer:addChild(self.bossNameText)

    return self.bossInfoLayer
end

-- HP가 닳는 경우 효과를 내는 함수
function BossInfoUIClass:updateBossHpGaze(dt)
    local currentHp = StageMng.boss.hp
    local maxHp = StageMng.boss.maxHp
    local targetGaze = nil
    -- 현재 Hp를 토대로 게이지가 어디까지 떨어져야하는지 정함
    targetGaze = self.gazePerHp*currentHp 
    -- 100보다 많다면 세컨드 프로그래스에서 깎음
    if self.currentGaze > 100 then
        if self.currentGaze >= targetGaze then
            self.currentGaze = self.currentGaze - (self.downHpValue *dt)          
            if self.currentGaze <= targetGaze then
                self.secondHpProgress:setPercentage(targetGaze%100)
                self.isDamaging = false
            else
                if self.currentGaze <= 100 then
                    self.secondHpProgress:setPercentage(0)
                else
                    self.secondHpProgress:setPercentage(self.currentGaze%100)
                end
            end
        end
    -- 그렇지 않다면 첫 프로그래스에서 깎음
    else
        if self.currentGaze >= targetGaze then
            self.currentGaze = self.currentGaze - (self.downHpValue *dt)   
            if self.currentGaze <= targetGaze then
                self.firstHpProgress:setPercentage(targetGaze)
                self.isDamaging = false
            else
                self.firstHpProgress:setPercentage(self.currentGaze)
            end
        end
    end
end

-- 보스 한글 이름 세팅 및 출력 함수
function BossInfoUIClass:setBossName()
    self.bossNameText:setString(monsterTb[StageMng.map.monsterID].monsterNameH)
    self.bossNameText:setVisible(true)
end


-- 인트로 후 Hp가 차는 효과를 내는 함수
function hpAnim(dt)
    local firstGaze = inGameUI.bossInfo.firstHpProgress
    local firstCurrentGaze = firstGaze:getPercentage()
    local secondGaze = inGameUI.bossInfo.secondHpProgress
    local secondCurrentGaze = secondGaze:getPercentage()
    local bossHp = StageMng.boss.maxHp

    -- 보스의 최대체력이 100이하라면 첫번째 프로그래스로만 채움
    if bossHp <= 100 then
        firstCurrentGaze = firstCurrentGaze + (100/(100/bossHp)) * dt
        if firstCurrentGaze >= 100 then
            firstCurrentGaze = 100
            inGameUI.bossInfo:setBossName()
            inGameUI:visualizeInputUI()
            StageMng.isGaming = true
            StageMng.timeCheckSchedule = cc.Director:getInstance():getScheduler():scheduleScriptFunc(StageMng.timeCheckFunc, 0, false)
            cc.Director:getInstance():getScheduler():unscheduleScriptEntry(inGameUI.bossInfo.bossHpScheduler)
            SoundMng:playBGM(eBGMSoundID.InGameBGM)
        end
            firstGaze:setPercentage(firstCurrentGaze)
    -- 100보다 많다면 하나의 게이지가 100이 되는속도를 100보다 적을때보다 2배로하여 채움
    else
        local halfHp = bossHp/2
        perHp = 200/(100/halfHp)
        if firstCurrentGaze < 100 then
            firstCurrentGaze = firstCurrentGaze + perHp * dt
            if firstCurrentGaze >= 100 then
                firstCurrentGaze = 100
            end
            firstGaze:setPercentage(firstCurrentGaze)
        else
            if secondCurrentGaze < 100 then
                secondCurrentGaze = secondCurrentGaze + perHp * dt
                if secondCurrentGaze >= 100 then
                    secondCurrentGaze = 100
                end
                secondGaze:setPercentage(secondCurrentGaze)
            else
                inGameUI.bossInfo:setBossName()
                inGameUI:visualizeInputUI()
                StageMng.isGaming = true
                StageMng.timeCheckSchedule = cc.Director:getInstance():getScheduler():scheduleScriptFunc(StageMng.timeCheckFunc, 0, false)
                cc.Director:getInstance():getScheduler():unscheduleScriptEntry(inGameUI.bossInfo.bossHpScheduler)
                SoundMng:playBGM(eBGMSoundID.InGameBGM)
            end
        end
    end
end