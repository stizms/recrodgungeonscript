ClearUIClass = {
    mainLayer         = nil,
    subLayer          = nil, 

    clearFrame        = nil,  -- 클리어 UI 부분 
    stageNameLabel    = nil, 
    clearRecordBG     = nil,  -- 클리어 기록 백그라운드
    clearRecordText   = nil, 
    newRecordSprite   = nil,
    newRecordAction   = nil,
    clearRewardBG     = nil,  -- 클리어 보상 백그라운드
    clearRewardText   = nil,

    buttonBG          = nil,
    currentOpacity    = 0,     -- 현재 mainLayer의 Opacity
    deltaContainer    = 0,     -- dt쌓아두는 곳
    canTouch          = false, -- 터치 가능 여부
    fadeInOpacity     = nil,
    fadeSchedule      = nil
}

function ClearUIClass:new(o)
    o = o or {}
    setmetatable(o,self)
    self.__index = self
    o:init()
    return o
end

-- 세팅 함수
function ClearUIClass:init()
    self.mainLayer = nil
    self.mainLayer = cc.LayerColor:create(cc.c4b(0,0,0,0))

    self.mainLayer:setVisible(false)

    -- 점점 mainLayer를 까맣게 하는 함수 
    self.fadeInOpacity = function (dt)
        self.deltaContainer = self.deltaContainer + dt
        if self.deltaContainer > 0.1 then
            self.currentOpacity = self.currentOpacity + (255 * dt)
            self.mainLayer:setOpacity(self.currentOpacity)
            self.deltaContainer = 0
            -- Opacity가 100이 되면 클리어 UI 가시화하고 끝
            if self.currentOpacity > 100 then
                cc.Director:getInstance():getScheduler():unscheduleScriptEntry(self.fadeSchedule)
                StageMng.player.state = ePlayerState.Idle
                inGameUI:nonVisualizeInputUI()
                self.subLayer:setVisible(true)

                local function btnBGAnim()
                    local moveTo3 = cc.MoveTo:create(0.1,cc.p(visibleSize.width/2,visibleSize.height/2 - 300))
                    local callFunc4 = cc.CallFunc:create(function () 
                        self.canTouch = true
                    end)
                    local sequence4 = cc.Sequence:create(moveTo3,callFunc4)
                    self.buttonBG:runAction(sequence4)
                end
                
                local function resultAnim()
                    local moveTo1 = cc.MoveTo:create(0.1,cc.p(0,visibleSize.height/2 + 130))
                    local callFunc2 = cc.CallFunc:create(function () 
                        local moveTo2 = cc.MoveTo:create(0.1,cc.p(0,visibleSize.height/2 + 70))
                        local callFunc3 = cc.CallFunc:create(btnBGAnim)
                        local sequence3 = cc.Sequence:create(moveTo2,callFunc3) 
                        self.clearRewardBG:runAction(sequence3)
                    end)
                    local sequence2 = cc.Sequence:create(moveTo1,callFunc2)
                    self.clearRecordBG:runAction(sequence2)
                end

                SoundMng:playBGM(eBGMSoundID.ClearUIBGM)
                local scaleTo1 = cc.ScaleTo:create(0.1,1)
                local callFunc1 = cc.CallFunc:create(resultAnim)
                local sequence1 = cc.Sequence:create(scaleTo1,callFunc1)
                self.clearFrame:runAction(sequence1)
            end
        end
    end

    self.subLayer = cc.Layer:create()
    self.subLayer:setVisible(false)

    self.clearFrame = cc.Sprite:create("clearFrame.png")
    self.clearFrame:setAnchorPoint(0,0.5)
    self.clearFrame:setPosition(0,visibleSize.height/2 + 300)
    self.subLayer:addChild(self.clearFrame)


    local clearLabel = cc.Label:createWithTTF("STAGE CLEAR","RetroFont2.ttf",90)
    clearLabel:setAnchorPoint(0,0.5)
    clearLabel:setPosition(10,45)
    self.clearFrame:addChild(clearLabel)

    self.stageNameLabel = cc.Label:createWithTTF("D-1","RetroFont2.ttf",70)
    self.stageNameLabel:setColor(cc.c3b(237,116,35))
    self.stageNameLabel:setPosition(self.clearFrame:getContentSize().width-60,15)
    self.clearFrame:addChild(self.stageNameLabel)

    self.clearFrame:setScale(2)

    self.clearRecordBG = cc.Sprite:create("clearInfoFrame.png")
    self.clearRecordBG:setAnchorPoint(0,0.5)
    self.clearRecordBG:setPosition(-self.clearRecordBG:getContentSize().width,visibleSize.height/2 + 130)
    self.subLayer:addChild(self.clearRecordBG)

    local clearRecordTitle = cc.Label:createWithTTF("클리어 기록","BMFont.ttf",25)
    clearRecordTitle:setAnchorPoint(0,0)
    clearRecordTitle:setColor(cc.c3b(35,22,14))
    self.clearRecordBG:addChild(clearRecordTitle)
    clearRecordTitle:setPosition(30,5)

    self.clearRecordText = cc.Label:createWithTTF("00:00","BMFont.ttf",25)
    self.clearRecordText:setColor(cc.c3b(240,190,69))
    self.clearRecordBG:addChild(self.clearRecordText)
    self.clearRecordText:setPosition(self.clearRecordBG:getContentSize().width - 100,17)

    self.newRecordSprite = cc.Sprite:create("new.png")
    self.newRecordSprite:setScale(0.1)
    self.clearRecordBG:addChild(self.newRecordSprite)
    self.newRecordSprite:setPosition(self.clearRecordBG:getContentSize().width-20,self.clearRecordBG:getContentSize().height)
    self.newRecordSprite:setVisible(false)

    self.clearRewardBG = cc.Sprite:create("clearInfoFrame.png")
    self.clearRewardBG:setAnchorPoint(0,0.5)
    self.clearRewardBG:setPosition(-self.clearRewardBG:getContentSize().width,visibleSize.height/2 + 70)
    self.subLayer:addChild(self.clearRewardBG)

    local clearRewardTitle = cc.Label:createWithTTF("클리어 보상","BMFont.ttf",25)
    clearRewardTitle:setAnchorPoint(0,0)
    clearRewardTitle:setColor(cc.c3b(35,22,14))
    self.clearRewardBG:addChild(clearRewardTitle)
    clearRewardTitle:setPosition(30,5)

    local clearRewardSprite = cc.Sprite:create("Gold.png")
    self.clearRewardBG:addChild(clearRewardSprite)
    clearRewardSprite:setPosition(self.clearRewardBG:getContentSize().width - 120,19)

    self.clearRewardText = cc.Label:createWithTTF("","BMFont.ttf",25)
    self.clearRewardText:setColor(cc.c3b(240,190,69))
    self.clearRewardText:setAnchorPoint(0,0.5)
    self.clearRewardBG:addChild(self.clearRewardText)
    self.clearRewardText:setPosition(self.clearRewardBG:getContentSize().width - 100,17)

    self.buttonBG = cc.Sprite:create("ButtonBackGround.png")
    self.buttonBG:setPosition(visibleSize.width/2,-self.buttonBG:getContentSize().height-50)
    self.buttonBG:setScale(2)
    self.subLayer:addChild(self.buttonBG)
    self.buttonBG:addChild(self:createBtn(eButtonID.GoLobby,self.buttonBG:getContentSize().width/2-300,-600))
    self.buttonBG:addChild(self:createBtn(eButtonID.Retry,self.buttonBG:getContentSize().width/2,-600))
    
    local intersectsBar = cc.Sprite:create("IntersectsBar.png")
    self.buttonBG:addChild(intersectsBar)
    intersectsBar:setPosition(self.buttonBG:getContentSize().width/2+10,self.buttonBG:getContentSize().height/2)
    intersectsBar:setScale(0.75)
    self.mainLayer:addChild(self.subLayer)

    StageMng.layer:addChild(self.mainLayer,100)
end

-- 가시화 함수
function ClearUIClass:visualizeUI()
    self:setClearInfo()
    self.mainLayer:setVisible(true)
    self.fadeSchedule = cc.Director:getInstance():getScheduler():scheduleScriptFunc(self.fadeInOpacity,0,false)
end

function ClearUIClass:visualizeNewRecord()
    -- new Action
    local fadeIn = cc.FadeIn:create(0.4)
    local fadeTo = cc.FadeTo:create(0.4,130)
    local fadeSequence = cc.Sequence:create(fadeIn,fadeTo)
    self.newRecordAction = cc.RepeatForever:create(fadeSequence)
    self.newRecordSprite:runAction(self.newRecordAction)
    self.newRecordSprite:setVisible(true)
end

-- 버튼 생성 함수
function ClearUIClass:createBtn(tag,btnX,btnY)
    local btnLayer = cc.Layer:create()
    local btn = nil
    if tag == eButtonID.Retry then
        btn = ccui.Button:create("Retry.png","Retry.png")
    elseif tag == eButtonID.GoLobby then
        btn = ccui.Button:create("GoLobby.png","GoLobby.png")
    elseif tag == eButtonID.WeaponChange then
        btn = ccui.Button:create("WeaponChangeBtn.png","WeaponChangeBtn.png")
    end
    btn:setPosition(btnX,btnY)
    btn:addTouchEventListener(
        function(sender,type)
            if self.canTouch == true then
                if type == ccui.TouchEventType.began then
                    btn:setColor(cc.c3b(150,150,150))
                    SoundMng:playEffect(eEffectSoundID.BtnEffect)
                elseif type == ccui.TouchEventType.canceled then
                    btn:setColor(cc.c3b(255,255,255))
                elseif type == ccui.TouchEventType.ended then
                    btn:setColor(cc.c3b(255,255,255))
                    SoundMng:stopBGM()
                    cc.Director:getInstance():getScheduler():unscheduleScriptEntry(StageMng.stageSchedule)
                    if self.newRecordAction ~= nil then
                        self.newRecordSprite:stopAction(self.newRecordAction)
                    end
                    if tag == eButtonID.Retry then
                        SceneMng:changeScene(stageTb:getStageInfo(StageMng.currentStage).scene)
                    elseif tag == eButtonID.GoLobby then
                        SceneMng:changeScene(eScene.Lobby)
                    end
                end
            end
        end
    )
    btnLayer:addChild(btn)
    btnLayer:setScale(0.5)
    return btnLayer
end

-- 클리어 결과 수치 세팅 함수
function ClearUIClass:setClearInfo()
    local elapsedTime = math.floor(StageMng.elapsedTime)
    local minutes = math.floor(elapsedTime/60)
    local seconds = elapsedTime%60
    self.stageNameLabel:setString(stageTb:getStageInfo(StageMng.currentStage).stageAcronym)
    self.clearRecordText:setString(string.format("%02d:%02d",minutes,seconds))
    self.clearRewardText:setString(StageMng.rewardGold)
end