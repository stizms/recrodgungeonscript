DeadUIClass = {
    mainLayer         = nil,
    subLayer          = nil,

    weaponChangeUI    = nil,    -- InGameWeaponChangeUI 객체

    -- Opactiy를 빨갛게 만드는 요소들
    currentColorIndex = 1,
    colorValue        = 51,
    deltaContainer    = 0,

    fadeInColor       = nil,   -- fade 기능을 담아두는 함수
    fadeSchedule      = nil,
    deathIcon         = nil,
    iconAction        = nil,
    isFocusLayer      = false -- 현재 터치를 방해하는 어떤 것이 있는지 없는지 플래그
}

function DeadUIClass:new(o)
    o = o or {}
    setmetatable(o,self)
    self.__index = self
    o:init()
    return o
end

-- 세팅 함수
function DeadUIClass:init()
    self.mainLayer = nil
    self.mainLayer = cc.LayerColor:create(cc.c4b(0,0,0,100))

    self.mainLayer:setVisible(false)

    -- mainLayer를 점차 빨갛게 만듬
    self.fadeInColor = function (dt)
        self.deltaContainer = self.deltaContainer + dt
        if self.deltaContainer > 0.1 then
            self.mainLayer:setColor(cc.c3b(self.colorValue*self.currentColorIndex,0,0))
            self.currentColorIndex = self.currentColorIndex + 1
            self.deltaContainer = 0
            if self.currentColorIndex == 5 then
                self.currentColorIndex = 1
                cc.Director:getInstance():getScheduler():unscheduleScriptEntry(self.fadeSchedule)
                self.subLayer:setVisible(true)
                self.deathIcon:runAction(self.iconAction)
                SoundMng:playEffect(eEffectSoundID.DeadUIEffect)
            end
        end
    end

    self.subLayer = cc.Layer:create()
    self.subLayer:setVisible(false)

    local deathLabel = cc.Label:createWithTTF("YOU ARE DEAD","RetroFont2.ttf",90)
    deathLabel:setPosition(visibleSize.width/2 + 5,visibleSize.height/2 + 330)
    self.subLayer:addChild(deathLabel)

    self.deathIcon = cc.Sprite:create("deathIcon.png")
    self.deathIcon:setPosition(visibleSize.width/2,visibleSize.height/2 + 95)
    self.deathIcon:setScale(0.7)
    self.subLayer:addChild(self.deathIcon)

    local moveTo1 = cc.MoveTo:create(0.5, cc.p(visibleSize.width/2,visibleSize.height/2 +105))
    local moveTo2 = cc.MoveTo:create(0.5, cc.p(visibleSize.width/2,visibleSize.height/2 +85))
    local sequence1 = cc.Sequence:create(moveTo1,moveTo2)
    self.iconAction = cc.RepeatForever:create(sequence1)
    self.iconAction:retain()

    local buttonBG = cc.Sprite:create("ButtonBackGround.png")
    buttonBG:setPosition(visibleSize.width/2,visibleSize.height/2 - 300)
    buttonBG:setScale(2)
    self.subLayer:addChild(buttonBG)
    self.subLayer:addChild(self:createBtn(eButtonID.GoLobby,visibleSize.width/2-150,visibleSize.height/2-300))
    self.subLayer:addChild(self:createBtn(eButtonID.Retry,visibleSize.width/2+150,visibleSize.height/2-300))
    local intersectsBar = cc.Sprite:create("IntersectsBar.png")
    intersectsBar:setPosition(visibleSize.width/2,visibleSize.height/2 - 300)
    intersectsBar:setScale(1.5)
    self.subLayer:addChild(intersectsBar)
    self.mainLayer:addChild(self.subLayer)

    -- InGameWeaponChangeUI 생성
    self.weaponChangeUI = InGameWeaponChangeUI:new(self)
    self.mainLayer:addChild(self.weaponChangeUI:createLayer())
    StageMng.layer:addChild(self.mainLayer,100)
end

-- 가시화 함수
function DeadUIClass:visualizeUI()
    self.isFocusLayer = true
    self.mainLayer:setVisible(true)
    self.fadeSchedule = cc.Director:getInstance():getScheduler():scheduleScriptFunc(self.fadeInColor,0,false)
end

-- 버튼 생성 함수
function DeadUIClass:createBtn(tag,btnX,btnY)
    local btnLayer = cc.Layer:create()
    local btn = nil
    if tag == eButtonID.Retry then
        btn = ccui.Button:create("Retry.png","Retry.png")
    elseif tag == eButtonID.GoLobby then
        btn = ccui.Button:create("GoLobby.png","GoLobby.png")
    end
    btn:setPosition(btnX,btnY)
    btn:addTouchEventListener(
        function(sender,type)
            if self.isFocusLayer == true then
                if type == ccui.TouchEventType.began then
                    btn:setColor(cc.c3b(150,150,150))
                    SoundMng:playEffect(eEffectSoundID.BtnEffect)
                elseif type == ccui.TouchEventType.canceled then
                    btn:setColor(cc.c3b(255,255,255))
                elseif type == ccui.TouchEventType.ended then
                    btn:setColor(cc.c3b(255,255,255))
                    if tag == eButtonID.Retry then
                        self.isFocusLayer = false
                        popUpUI:setQuestionPopUp(self.mainLayer,"장비를 교체하시겠습니까?",
                        function ()
                            self.weaponChangeUI:visualizeUI()
                        end, 
                        function () 
                            cc.Director:getInstance():getScheduler():unscheduleScriptEntry(StageMng.stageSchedule)
                            if StageMng.shakeSchedule ~= nil then
                                cc.Director:getInstance()getScheduler():unscheduleScriptEntry(StageMng.shakeSchedule)
                            end
                            self.deathIcon:stopAction(self.iconAction)
                            SceneMng:changeScene(stageTb:getStageInfo(StageMng.currentStage).scene)
                        end)
                    elseif tag == eButtonID.GoLobby then
                        cc.Director:getInstance():getScheduler():unscheduleScriptEntry(StageMng.stageSchedule)
                        if StageMng.shakeSchedule ~= nil then
                            cc.Director:getInstance()getScheduler():unscheduleScriptEntry(StageMng.shakeSchedule)
                        end
                        self.deathIcon:stopAction(self.iconAction)
                        SceneMng:changeScene(eScene.Lobby)
                    end
                end
            end
        end
    )
    btnLayer:addChild(btn)
    return btnLayer
end