require "script.UI.InGame.BossInfoUI"
require "script.UI.InGame.Joystick"
require "script.UI.InGame.DeadUI"
require "script.UI.InGame.ClearUI"
require "script.UI.InGame.InGameWeaponChangeUI"
require "script.UI.InGame.IntroUI"

InGameUIClass = {
    introLayer       = nil,  -- introUI 객체
    bossInfo         = nil,  -- 보스 HP에 관련된 객체
    inputLayer       = nil,  -- 조이스틱과 공격 버튼을 포함하는 레이어
    joystick         = nil,  -- 조이스틱 기능 객체
    targetPlayer     = nil,  -- player 객체 참조
    mainWeaponBtn    = nil,
    subWeaponBtn     = nil,
    mainWeaponCancel = nil,
    subWeaponCancel  = nil,
    subWeaponSprite  = nil,
    subBulletText    = nil, 
    deadUI           = nil,  -- 졌을 경우 가시화 되는 UI
    clearUI          = nil,  -- 이겼을 경우 가시화 되는 UI
    canInput         = false,-- 터치 가능한지 여부
    gazeSound        = nil   -- soundID
}

-- InGameUI 생성 함수
function InGameUIClass:new(player,o)
    o = o or {}
    setmetatable(o,self)
    self.__index = self
    o:init(player)
    return o
end

-- InGmaeUI 세팅 함수
function InGameUIClass:init(player)
    -- 보스 HP 관련 객체
    self.bossInfo = BossInfoUIClass:new()
    StageMng.layer:addChild(self.bossInfo:createBossInfo(),100)

    -- 인트로 레이어
    self.introLayer = IntroUIClass:new()
    
    -- 인게임 Input 레이어
    self.inputLayer = cc.Layer:create()
    self.inputLayer:setCascadeOpacityEnabled(true)
    self.inputLayer:setVisible(false)

    -- 조이스틱 객체 및 등록
    self.joystick = JoystickClass:new()
    self.inputLayer:addChild(self.joystick.joystickBg)
    self.inputLayer:addChild(self.joystick.joystickCenter)
    
    -- 플레이어 파싱
    self.targetPlayer = player

    -- 공격 버튼 생성
    self.mainWeaponBtn = self:createBtn(eButtonID.MainAttack,visibleSize.width/2 +150, visibleSize.height/2 - 500)
    self.subWeaponBtn  = self:createBtn(eButtonID.SubAttack,visibleSize.width/2 +150, visibleSize.height/2 - 340)

    self.inputLayer:addChild(self.mainWeaponBtn,100)
    self.inputLayer:addChild(self.subWeaponBtn,100)

    StageMng.layer:addChild(self.inputLayer,100)
    
    -- 결과 UI 생성
    self.deadUI  = DeadUIClass:new()
    self.clearUI = ClearUIClass:new()
end

-- 인풋 레이어 ( 조이스틱 및 공격 버튼 ) 가시화 함수
function InGameUIClass:visualizeInputUI()
    self.inputLayer:setVisible(true)
    self.canInput = true
end

-- 인풋 레이어 ( 조이스틱 및 공격 버튼 ) 비가시화 함수
function InGameUIClass:nonVisualizeInputUI()
    self.inputLayer:setVisible(false)
    self.joystick.horizonVal  = 0
    self.joystick.verticalVal = 0
    self.canInput = false
end

-- 버튼 생성 함수
function InGameUIClass:createBtn(tag,btnX,btnY)
    -- 레이어 생성
    local btnLayer = cc.Layer:create()
    btnLayer:setCascadeOpacityEnabled(true)
    btnLayer:setCascadeColorEnabled(true)

    -- 버튼 생성
    local btn = ccui.Button:create("WeaponBG.png","WeaponSelectedBG.png")
    btn:setOpacity(60)
    btn:setAnchorPoint(0.5,0.5)
    btn:setPosition(btnX, btnY)
    
    -- 무기 아이콘에 btn의 Opacity의 영향을 받지 않기 위해 서브 레이어 생성
    local subBtnLayer = cc.Layer:create()
    btn:addChild(subBtnLayer)
    local currentWeaponSprite = nil
    
    -- 아이콘 등록
    if tag == eButtonID.MainAttack then
        currentWeaponSprite = cc.Sprite:create(weaponTb[UserDataMng.currentMainWeapon].iconName)
        self.mainWeaponCancel = cc.Sprite:create("cancel.png")
        self.mainWeaponCancel:setScale(0.15)
        self.mainWeaponCancel:setPosition(currentWeaponSprite:getContentSize().width/2,currentWeaponSprite:getContentSize().height/2)
        self.mainWeaponCancel:setVisible(false)
        currentWeaponSprite:addChild(self.mainWeaponCancel)
        btn:setScale(1.5)
    elseif tag == eButtonID.SubAttack then
        btn:setScale(0.9)
        currentWeaponSprite = cc.Sprite:create(weaponTb[UserDataMng.currentSubWeapon].iconName)
        self.subWeaponCancel = cc.Sprite:create("cancel.png")
        self.subWeaponCancel:setScale(0.15)
        self.subWeaponCancel:setPosition(currentWeaponSprite:getContentSize().width/2,currentWeaponSprite:getContentSize().height/2)
        self.subWeaponCancel:setVisible(false)
        currentWeaponSprite:addChild(self.subWeaponCancel)
        self.subWeaponSprite = currentWeaponSprite
        self.subBulletText  = cc.Label:createWithTTF(string.format("%d/%d",self.targetPlayer.maxSubBullet-self.targetPlayer.useSubBullet,self.targetPlayer.maxSubBullet),"RetroFont2.ttf",40)
        btn:addChild(self.subBulletText)
        self.subBulletText:setPosition(btn:getContentSize().width/2+40,10)
    end
    subBtnLayer:addChild(currentWeaponSprite)
    currentWeaponSprite:setPosition(btn:getContentSize().width/2,btn:getContentSize().height/2)
    btn:addTouchEventListener(
        function(sender,type)
            -- 터치 통제 상태, 플레이어가 넉백 상태에는 공격 불가 ( 죽을 경우에는 UI 자체가 사라짐 )
            if StageMng.isGaming == true and self.targetPlayer.state ~= ePlayerState.Knockback then
                if type == ccui.TouchEventType.began then
                    if tag == eButtonID.MainAttack then
                        currentWeaponSprite:setColor(cc.c4b(150,150,150,50))
                        -- 상태가 MainAttack으로 변한 시점에는 상태만 바꿔줌 ( 액션 한번 실행을 위함 )
                        if self.targetPlayer.state ~= ePlayerState.MainAttack then
                             if self.gazeSound ~= nil then
                                SoundMng:stopEffect(self.gazeSound)
                                self.gazeSound = nil
                            end
                            self.targetPlayer.state = ePlayerState.MainAttack
                            self:visualizeCancelBtnUI(tag)
                        -- 이전 상태와 동일하다면 공격 취소
                        else
                            self.targetPlayer.attackGaze:setVisible(false)
                            self.targetPlayer.attackGaze.gazeProgress:setPercentage(0)
                            self.targetPlayer.state = ePlayerState.Idle
                            self.targetPlayer.attackAnimDelay = 0
                            self:nonVisualizeCancelBtnUI(tag)
                        end
                    elseif tag == eButtonID.SubAttack then
                        -- 총알 횟수를 통한 공격 가능 여부 판단
                        if self.targetPlayer.maxSubBullet ~= self.targetPlayer.useSubBullet then
                            currentWeaponSprite:setColor(cc.c4b(150,150,150,50))
                            self.subBulletText:setColor(cc.c4b(150,150,150,50))
                            if self.targetPlayer.state ~= ePlayerState.SubAttack then
                                self.targetPlayer.state = ePlayerState.SubAttack
                                self.targetPlayer.direction = eDirection.Up
                                self.gazeSound = SoundMng:playEffect(eEffectSoundID.SubAttackGazeEffect,true)
                                self:visualizeCancelBtnUI(tag)
                            else
                                SoundMng:stopEffect(self.gazeSound)
                                self.gazeSound = nil
                                self.targetPlayer.attackGaze:setVisible(false)
                                self.targetPlayer.attackGaze.gazeProgress:setPercentage(0)
                                self.targetPlayer.state = ePlayerState.Idle
                                self.targetPlayer.attackAnimDelay = 0
                                self:nonVisualizeCancelBtnUI(tag)
                            end
                        end
                    end
                elseif type== ccui.TouchEventType.ended then
                    if tag ~= eButtonID.SubAttack or (tag == eButtonID.SubAttack and self.targetPlayer.maxSubBullet ~= self.targetPlayer.useSubBullet) then
                        currentWeaponSprite:setColor(cc.c4b(255,255,255,255))
                        self.subBulletText:setColor(cc.c4b(255,255,255,255))
                    end
                end
            end
        end
    )
     btnLayer:addChild(btn)
     return btnLayer
end

-- 취소 아이콘 켜기
function InGameUIClass:visualizeCancelBtnUI(tag)
    if tag == eButtonID.MainAttack then
        self.mainWeaponCancel:setVisible(true)
    elseif tag == eButtonID.SubAttack then
        self.subWeaponCancel:setVisible(true)
    end
end

-- 취소 아이콘 끄기
function InGameUIClass:nonVisualizeCancelBtnUI(tag)
    if tag == eButtonID.MainAttack then
        self.mainWeaponCancel:setVisible(false)
    elseif tag == eButtonID.SubAttack then
        self.subWeaponCancel:setVisible(false)
    end
end

-- 서브 무기 사용 불가 상태로 설정
function InGameUIClass:disabledSubBtn()
    self.subWeaponSprite:setColor(cc.c4b(255,0,0,50))
    self.subWeaponBtn:setColor(cc.c3b(255,0,0))
    self.subBulletText:setColor(cc.c4b(150,150,150,50))
end

-- 총알 개수 갱신
function InGameUIClass:updateBulletCount()
    self.subBulletText:setString(string.format("%d/%d",self.targetPlayer.maxSubBullet-self.targetPlayer.useSubBullet,self.targetPlayer.maxSubBullet))
end