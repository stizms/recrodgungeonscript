InGameWeaponChangeUI  = {
    mainLayer         = nil, 
    subLayer          = nil, 
    parentUI          = nil,    -- DeadUI 객체
    mainWeaponScroll  = nil,    -- 주무기 ScrollView
    subWeaponScroll   = nil,    -- 보조무기 ScrollView
    mainWeaponBtnList = nil,    -- 주무기 슬롯 리스트
    subWeaponBtnList  = nil     -- 보조무기 슬롯 리스트
}

-- 생성 함수
function InGameWeaponChangeUI:new(parentUI,o)
    o = o or {}
    setmetatable(o,self)
    self.__index = self
    self.parentUI = parentUI
    return o
end

-- 레이어 세팅
function InGameWeaponChangeUI:createLayer()
    self.mainLayer = cc.LayerColor:create(cc.c4b(0,0,0,160))
    self.subLayer = cc.Layer:create()

    -- 주 무기 부분
    local mainWeaponTitleBG = cc.Sprite:create("clearInfoFrame.png")
    mainWeaponTitleBG:setAnchorPoint(0,0.5)
    mainWeaponTitleBG:setPosition(0,visibleSize.height/2 + 200)
    self.subLayer:addChild(mainWeaponTitleBG)

    local mainWeaponTitle = cc.Label:createWithTTF("주 무기 교체","BMFont.ttf",25)
    mainWeaponTitle:setAnchorPoint(0,0)
    mainWeaponTitle:setColor(cc.c3b(35,22,14))
    mainWeaponTitleBG:addChild(mainWeaponTitle)
    mainWeaponTitle:setPosition(30,5)

    local mainWeaponUpFrame = cc.Sprite:create("slideFrame.png")
    mainWeaponUpFrame:setAnchorPoint(0,0.5)
    mainWeaponUpFrame:setScale(2)
    mainWeaponUpFrame:setPosition(0,visibleSize.height/2 +160)
    self.subLayer:addChild(mainWeaponUpFrame)

    self.mainWeaponScroll = ccui.ScrollView:create()
    self.mainWeaponScroll:setDirection(ccui.ScrollViewDir.horizontal)
    self.mainWeaponScroll:setAnchorPoint(0.5,0.5)
    self.mainWeaponScroll:setContentSize(visibleSize.width,150)
    self.mainWeaponScroll:setBounceEnabled(true)
    self.mainWeaponScroll:setScrollBarEnabled(false)
    self.mainWeaponScroll:setPosition(visibleSize.width/2,visibleSize.height/2 + 75)
    self.subLayer:addChild(self.mainWeaponScroll)
    self.mainWeaponBtnList = {}
    
    local mainWeaponDownFrame = cc.Sprite:create("slideFrame.png")
    mainWeaponDownFrame:setAnchorPoint(0,0.5)
    mainWeaponDownFrame:setScaleY(-2)
    mainWeaponDownFrame:setScaleX(2)
    mainWeaponDownFrame:setPosition(0,visibleSize.height/2 - 10)
    self.subLayer:addChild(mainWeaponDownFrame)

    -- 보조 무기 부분
    local subWeaponTitleBG = cc.Sprite:create("clearInfoFrame.png")
    subWeaponTitleBG:setAnchorPoint(0,0.5)
    subWeaponTitleBG:setPosition(0,visibleSize.height/2 - 50)
    self.subLayer:addChild(subWeaponTitleBG)

    local subWeaponTitle = cc.Label:createWithTTF("보조 무기 교체","BMFont.ttf",25)
    subWeaponTitle:setAnchorPoint(0,0)
    subWeaponTitle:setColor(cc.c3b(35,22,14))
    subWeaponTitleBG:addChild(subWeaponTitle)
    subWeaponTitle:setPosition(30,5)

    local subWeaponUpFrame = cc.Sprite:create("slideFrame.png")
    subWeaponUpFrame:setAnchorPoint(0,0.5)
    subWeaponUpFrame:setScale(2)
    subWeaponUpFrame:setPosition(0,visibleSize.height/2 - 90)
    self.subLayer:addChild(subWeaponUpFrame)

    self.subWeaponScroll = ccui.ScrollView:create()
    self.subWeaponScroll:setDirection(ccui.ScrollViewDir.horizontal)
    self.subWeaponScroll:setAnchorPoint(0.5,0.5)
    self.subWeaponScroll:setContentSize(visibleSize.width,150)
    self.subWeaponScroll:setBounceEnabled(true)
    self.subWeaponScroll:setScrollBarEnabled(false)
    self.subWeaponScroll:setPosition(visibleSize.width/2,visibleSize.height/2 - 175)
    self.subLayer:addChild(self.subWeaponScroll)
    self.subWeaponBtnList = {}

    local subWeaponDownFrame = cc.Sprite:create("slideFrame.png")
    subWeaponDownFrame:setAnchorPoint(0,0.5)
    subWeaponDownFrame:setScaleY(-2)
    subWeaponDownFrame:setScaleX(2)
    subWeaponDownFrame:setPosition(0,visibleSize.height/2 - 260)
    self.subLayer:addChild(subWeaponDownFrame)

    -- 나가는 버튼
    local exitWeaponChangeBtn = ccui.Button:create("OkayBtn.png")
    exitWeaponChangeBtn:addTouchEventListener(
        function(sender,type)
            if type == ccui.TouchEventType.began then
                exitWeaponChangeBtn:setColor(cc.c3b(150,150,150))
            elseif type == ccui.TouchEventType.canceled then
                exitWeaponChangeBtn:setColor(cc.c3b(255,255,255))
            elseif type == ccui.TouchEventType.ended then
                exitWeaponChangeBtn:setColor(cc.c3b(255,255,255))
                self.parentUI.isFocusLayer = true
                self:nonVisualizeUI()
                cc.Director:getInstance():getScheduler():unscheduleScriptEntry(StageMng.stageSchedule)
                self.parentUI.deathIcon:stopAction(self.parentUI.iconAction)
                SceneMng:changeScene(stageTb:getStageInfo(StageMng.currentStage).scene)
            end
        end
    )
    exitWeaponChangeBtn:setScale(2)
    exitWeaponChangeBtn:setPosition(visibleSize.width/2,230)
    self.subLayer:addChild(exitWeaponChangeBtn)

    self.mainLayer:addChild(self.subLayer)
    self.mainLayer:setVisible(false)
    return self.mainLayer
end

-- 슬롯 데이터 세팅
function InGameWeaponChangeUI:setSlotData()
    local weaponSlotSize = 130
    local padding        = 20
    
    -- 주무기의 경우
    if #UserDataMng.haveMainWeaponList ~= #self.mainWeaponBtnList then
        local mainWeaponCount = #UserDataMng.haveMainWeaponList
        self.mainWeaponScroll:setInnerContainerSize(cc.size(padding*(mainWeaponCount+1)+(weaponSlotSize*mainWeaponCount),150))
        for i = #self.mainWeaponBtnList+1, mainWeaponCount do
            local weaponSlot = BaseWeaponSlotClass:new(self.mainWeaponScroll,eWeaponSlotTag.InGameMain,UserDataMng.haveMainWeaponList[i],self)
            table.insert(self.mainWeaponBtnList,weaponSlot)
            weaponSlot.mainBG:setPosition((weaponSlotSize/2+padding)+((weaponSlotSize+padding)*(i-1)),self.mainWeaponScroll:getContentSize().height/2)  
        end
    end

    -- 보조무기의 경우
    if #UserDataMng.haveSubWeaponList ~= #self.subWeaponBtnList then
        local subWeaponCount = #UserDataMng.haveSubWeaponList
        self.subWeaponScroll:setInnerContainerSize(cc.size(padding*(subWeaponCount+1)+(weaponSlotSize*subWeaponCount),150))
        for i = #self.subWeaponBtnList+1, subWeaponCount do
            local weaponSlot = BaseWeaponSlotClass:new(self.subWeaponScroll,eWeaponSlotTag.InGameSub,UserDataMng.haveSubWeaponList[i],self)
            table.insert(self.subWeaponBtnList,weaponSlot)
            weaponSlot.mainBG:setPosition((weaponSlotSize/2+padding)+((weaponSlotSize+padding)*(i-1)),self.subWeaponScroll:getContentSize().height/2)
        end
    end

    -- 현재 착용중인지 갱신
    self:updateMainSlotList()
    self:updateSubSlotList()
end

-- 가시화
function InGameWeaponChangeUI:visualizeUI()
    self:setSlotData()
    self.mainLayer:setVisible(true)
end

-- 비가시화
function InGameWeaponChangeUI:nonVisualizeUI()
    self.mainLayer:setVisible(false)
end

-- 메인 무기 슬롯 리스트들 중에 착용중인 슬롯 찾아서 갱신
function InGameWeaponChangeUI:updateMainSlotList()
    for i = 1, #self.mainWeaponBtnList do
        self.mainWeaponBtnList[i]:updateSlot()
    end
end

-- 서브 무기 슬롯 리스트들 중에 착용중인 슬롯 찾아서 갱신
function InGameWeaponChangeUI:updateSubSlotList()
    for i = 1, #self.subWeaponBtnList do
        self.subWeaponBtnList[i]:updateSlot()
    end
end