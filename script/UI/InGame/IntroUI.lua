IntroUIClass = {
    mainLayer = nil,
    subLayer  = nil
}

function IntroUIClass:new(o)
    o = o or {}
    setmetatable(o,self)
    self.__index = self
    o:setIntro()
    return o
end

-- 레이어 세팅
function IntroUIClass:setIntro()
    self.mainLayer = cc.LayerColor:create(cc.c4b(0,0,0,100))
    self.mainLayer:setVisible(false)
    StageMng.layer:addChild(self.mainLayer,100)
    
    self.subLayer = cc.Layer:create()
    self.subLayer:setPosition(-200,0)
    self.subLayer:setVisible(false)

    local stageInfo = stageTb:getStageInfo(StageMng.currentStage)

    local introUpFrame = cc.Sprite:create("IntroFrame.png")
    self.subLayer:addChild(introUpFrame)
    introUpFrame:setScale(-1)
    introUpFrame:setPosition(visibleSize.width/2,visibleSize.height/2 + 150)

    local stageNameLabel = cc.Label:createWithTTF(stageInfo.stageName,"BMFont.ttf",45)
    stageNameLabel:setAnchorPoint(0,0.5)
    self.subLayer:addChild(stageNameLabel)
    stageNameLabel:setPosition(10,visibleSize.height/2)

    local monsterNameLabel = cc.Label:createWithTTF(monsterTb[stageInfo.monsterID].monsterName,"RetroFont2.ttf",100)
    monsterNameLabel:setAnchorPoint(0,0.5)
    self.subLayer:addChild(monsterNameLabel)
    monsterNameLabel:setPosition(12,visibleSize.height/2-70)

    local monsterIcon = cc.Sprite:create(monsterTb[stageInfo.monsterID].monsterIcon)
    self.subLayer:addChild(monsterIcon)
    monsterIcon:setScale(3)
    monsterIcon:setPosition(visibleSize.width/2+220, visibleSize.height/2 + 30)


    local introDownFrame = cc.Sprite:create("IntroFrame.png")
    self.subLayer:addChild(introDownFrame)
    introDownFrame:setPosition(visibleSize.width/2,visibleSize.height/2 - 150)

    self.mainLayer:addChild(self.subLayer)
end

-- 가시화 ( 가시화와 동시에 액션 실행 )
function IntroUIClass:visualizeUI()
    self.subLayer:setVisible(true)
    local moveZeroPoint = cc.MoveBy:create(0.05,cc.p(200,0))
    local scaleFadeOut  = cc.ScaleTo:create(0.2,4)
    local delay         = cc.DelayTime:create(1)
    local shakeAction   = nil

    -- 좌우로 흔드는 액션
    local shakeLeftAndRight = cc.CallFunc:create(
        function ()
            local moveByRight = cc.MoveBy:create(0.01,cc.p(15,0))
            local moveByLeft  = cc.MoveBy:create(0.01,cc.p(-15,0))
            shakeAction       = cc.Sequence:create(moveByRight,moveByLeft)
            shakeAction       = cc.RepeatForever:create(shakeAction)
            self.subLayer:runAction(shakeAction)    
        end
    )

    -- 액션 멈추기
    local actionStop = cc.CallFunc:create(function () 
        self.subLayer:stopAction(shakeAction)
        self.subLayer:setPosition(0,0)
    end)

    -- 비가시화
    local nonVisualize = cc.CallFunc:create(function () 
        self:nonVisualizeUI()
    end)

    self.subLayer:runAction(cc.Sequence:create(moveZeroPoint,delay,shakeLeftAndRight,delay,actionStop,scaleFadeOut,nonVisualize))
    SoundMng:playEffect(eEffectSoundID.IntroEffect)
end

-- 비가시화 함수
function IntroUIClass:nonVisualizeUI()
    self.mainLayer:setVisible(false)
    inGameUI.bossInfo.bossHpScheduler = cc.Director:getInstance():getScheduler():scheduleScriptFunc(inGameUI.bossInfo.bossHpAnimFunc,0,false)
end