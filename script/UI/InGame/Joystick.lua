JoystickClass = {
    joystickBg      = nil, 
    joystickCenter  = nil,
    mainPos         = {x = 230, y = 170},   -- 가장 기본이 되는 위치
    horizonVal      = 0,                    -- 원점으로부터 멀리 떨어진 만큼의 수평 값
    verticalVal     = 0,                    -- 원점으로부터 멀리 떨어진 만큼의 수직 값
    angle           = 0,                    -- 원점으로부터의 각도
    currentPos      = {x = nil, y = nil},   -- 클릭을 시작한 원점의 좌표 (비고정이기 때문)
    isTouching      = false
}

function JoystickClass:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self
    o:init()
    return o
end

function JoystickClass:init()
    self:createJoystick()
end

-- 값 리셋
function JoystickClass:valueReset()
    self.joystickCenter:setPosition(self.mainPos)
    self.joystickBg:setPosition(self.mainPos)
    self.horizonVal   = 0
    self.verticalVal  = 0
    self.angle        = 0
    self.currentPos.x = nil
    self.currentPos.y = nil
end

-- 조이스틱 등록
function JoystickClass:createJoystick()
    self.joystickBg = cc.Sprite:create("OutLineCircle.png")
    self.joystickBg:setPosition(self.mainPos)
    self.joystickBg:setScale(0.4)

    self.joystickCenter = cc.Sprite:create("FilledCircle.png")
    self.joystickCenter:setPosition(self.mainPos)
    self.joystickCenter:setScale(0.2)
end

-- 스틱움직이는 함수
function JoystickClass:stickMove(touchPos)
    local x = touchPos.x - self.currentPos.x
    local y = touchPos.y - self.currentPos.y

    self:calcAngle(x,y)

    -- 반지름 * Scale 값
    local joystickBgRadius = self.joystickBg:getContentSize().width/2 * 0.4
    -- 거리가 원의 반지름을 넘어서면 원의 끝부분에 있게 만들기
    local distanceOfTouchPointToCenter = math.sqrt((self.currentPos.x-touchPos.x) * (self.currentPos.x-touchPos.x) + 
                                                    (self.currentPos.y - touchPos.y) * ( self.currentPos.y - touchPos.y))
    if  distanceOfTouchPointToCenter >= joystickBgRadius then
        x = x * (joystickBgRadius / distanceOfTouchPointToCenter)
        y = y * (joystickBgRadius / distanceOfTouchPointToCenter)
        self.joystickCenter:setPosition(self.currentPos.x + x, self.currentPos.y + y)
    else
        self.joystickCenter:setPosition(touchPos)
    end
    -- 현재 x와 y값을 0~1사이 값으로 세팅
    self.horizonVal = x / joystickBgRadius
    self.verticalVal = y / joystickBgRadius
end

-- 조이스틱의 원점으로부터의 각도를 구하는 함수 ( 해당 각도로 플레이어의 방향을 결정 )
function JoystickClass:calcAngle(x,y)
    -- Radian To Degree
    self.angle = math.atan2(y,x) * 180 / 3.141592
end