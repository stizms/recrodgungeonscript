EquipmentInsideUIClass = {
    mainLayer        = nil, 
    subLayer         = nil, 
    parent           = nil,     -- LobbyEquipment 객체
    backBtn          = nil,
    mainScroll       = nil,     -- 메인 무기 스크롤 
    mainBtnList      = nil,     -- 메인 슬롯 리스트
    subScroll        = nil,     -- 서브 무기 스크롤
    subBtnList       = nil,     -- 서브 슬롯 리스트
    titleLabel       = nil,
    weaponNameText   = nil, 
    weaponIconSprite = nil, 
    descriptionText  = nil, 
    stateText        = nil
}

function EquipmentInsideUIClass:new(parentLayer,titleLabel,parent,o)
    o = o or {}
    setmetatable(o,self)
    self.__index = self
    o:createInsideEquipmentLayer(parentLayer,titleLabel,parent)
    return o
end

-- 레이어 세팅 함수
function EquipmentInsideUIClass:createInsideEquipmentLayer(parentLayer,titleLabel,parent)
    self.parent     = parent
    self.titleLabel = titleLabel
    self.mainLayer  = cc.LayerColor:create(cc.c4b(0,0,0,160))
    self.mainLayer:setVisible(false)

    self.subLayer = cc.Layer:create()
    self.mainLayer:addChild(self.subLayer)

    self.backBtn  = ccui.Button:create("back.png","back.png")
    self.backBtn:addTouchEventListener(
        function (sender,type)
            if type == ccui.TouchEventType.began then
                self.backBtn:setColor(cc.c3b(150,150,150))
                SoundMng:playEffect(eEffectSoundID.BtnEffect)
            elseif type == ccui.TouchEventType.canceled then
                self.backBtn:setColor(cc.c3b(255,255,255))
            elseif type == ccui.TouchEventType.ended then
                self.backBtn:setColor(cc.c3b(255,255,255))
                self:nonVisualizeUI()
                self.parent:visualizeUI()
            end
        end
    )

    local weaponNameUpBorder = cc.Sprite:create("nameFrame.png")
    self.subLayer:addChild(weaponNameUpBorder)
    weaponNameUpBorder:setPosition(175,visibleSize.height/2+450)

    -- 무기 이름 텍스트
    self.weaponNameText = cc.Label:createWithTTF("","BMFont.ttf",30)
    self.subLayer:addChild(self.weaponNameText)
    self.weaponNameText:setPosition(175,visibleSize.height/2+420)

    local weaponNameDownBorder = cc.Sprite:create("nameFrame.png")
    self.subLayer:addChild(weaponNameDownBorder)
    weaponNameDownBorder:setPosition(175,visibleSize.height/2+390)

    local weaponFrame = cc.Sprite:create("circleFrame.png")
    self.subLayer:addChild(weaponFrame)
    weaponFrame:setPosition(175,visibleSize.height/2+250)

    self.weaponIconSprite = cc.Sprite:create("dynamiteStick_Icon.png")
    weaponFrame:addChild(self.weaponIconSprite)
    self.weaponIconSprite:setScale(1.5)
    local size = weaponFrame:getContentSize()
    self.weaponIconSprite:setPosition(size.width/2,size.height/2)

    local stateBG = cc.Sprite:create("stateFrame.png")
    self.subLayer:addChild(stateBG)
    stateBG:setPosition(visibleSize.width/2+199,visibleSize.height/2+290)

    local stateBGSize = stateBG:getContentSize()
    -- 무기 능력치 텍스트
    self.stateText = cc.Label:createWithTTF("데미지 : 10\n캐스팅 타임 : 2초\n폭발 범위 : 100","BMFont.ttf",33,cc.size(stateBGSize.width,stateBGSize.height))
    self.stateText:setAlignment(cc.TEXT_ALIGNMENT_LEFT)
    self.stateText:setColor(cc.c3b(255,255,255))
    stateBG:addChild(self.stateText)
    self.stateText:setPosition(178,stateBGSize.height/2-90)

    local descriptionBG = cc.Sprite:create("description.png")
    self.subLayer:addChild(descriptionBG)
    descriptionBG:setScaleY(1.7)
    descriptionBG:setScaleX(1.7)
    descriptionBG:setPosition(visibleSize.width/2,visibleSize.height/2-20)

    local descriptionSize = descriptionBG:getContentSize()
    -- 무기 설명 텍스트
    self.descriptionText = cc.Label:createWithTTF("","BMFont.ttf",17,cc.size(descriptionSize.width,descriptionSize.height))
    self.descriptionText:setAlignment(cc.TEXT_ALIGNMENT_CENTER)
    self.descriptionText:setColor(cc.c3b(255,255,255))
    descriptionBG:addChild(self.descriptionText)
    self.descriptionText:setPosition(descriptionSize.width/2,10)

    local scrollUpFrame = cc.Sprite:create("slideFrame.png")
    scrollUpFrame:setAnchorPoint(0,0.5)
    scrollUpFrame:setScale(2)
    scrollUpFrame:setPosition(0,visibleSize.height/2 -215)
    self.subLayer:addChild(scrollUpFrame)

    -- 메인 스크롤
    self.mainScroll = ccui.ScrollView:create()
    self.mainScroll:setDirection(ccui.ScrollViewDir.horizontal)
    self.mainScroll:setAnchorPoint(0.5,0.5)
    self.mainScroll:setContentSize(visibleSize.width,150)
    self.mainScroll:setBounceEnabled(true)
    self.mainScroll:setScrollBarEnabled(false)
    self.mainScroll:setPosition(visibleSize.width/2,visibleSize.height/2 -300)
    self.subLayer:addChild(self.mainScroll)
    self.mainScroll:setVisible(false)
    self.mainBtnList = {}

    -- 서브 스크롤
    self.subScroll = ccui.ScrollView:create()
    self.subScroll:setDirection(ccui.ScrollViewDir.horizontal)
    self.subScroll:setAnchorPoint(0.5,0.5)
    self.subScroll:setContentSize(visibleSize.width,150)
    self.subScroll:setBounceEnabled(true)
    self.subScroll:setScrollBarEnabled(false)
    self.subScroll:setPosition(visibleSize.width/2,visibleSize.height/2 -300)
    self.subLayer:addChild(self.subScroll)
    self.subScroll:setVisible(false)
    self.subBtnList = {}

    local scrollDownFrame = cc.Sprite:create("slideFrame.png")
    scrollDownFrame:setAnchorPoint(0,0.5)
    scrollDownFrame:setScaleY(-2)
    scrollDownFrame:setScaleX(2)
    scrollDownFrame:setPosition(0,visibleSize.height/2 -385)
    self.subLayer:addChild(scrollDownFrame)

    parentLayer:addChild(self.mainLayer)
end

-- 가시화 함수
function EquipmentInsideUIClass:visualizeUI(tag)
    self:settingScrollData(tag)
    self.parent:nonVisualizeUI()
    self.backBtn:setVisible(true)
    self.mainLayer:setVisible(true)
end

-- 비가시화 함수
function EquipmentInsideUIClass:nonVisualizeUI()
    self.mainLayer:setVisible(false)
    self.mainScroll:setVisible(false)
    self.subScroll:setVisible(false)
    self.backBtn:setVisible(false)
    self.parent.isUsingInside = false
end

-- 슬롯 생성 함수 LobbyHome의 스테이지 스크롤과 동일 방식
function EquipmentInsideUIClass:settingScrollData(tag)
    local weaponSlotSize = 130
    local padding        = 20

    -- 메인 무기 스크롤
    if tag == eAttackCategory.MainWeapon then
        if #UserDataMng.haveMainWeaponList ~= #self.mainBtnList then
            local mainWeaponCount = #UserDataMng.haveMainWeaponList
            self.mainScroll:setInnerContainerSize(cc.size(padding*(mainWeaponCount+1)+(weaponSlotSize*mainWeaponCount),150))
            for i = #self.mainBtnList+1, mainWeaponCount do
                local weaponSlot = BaseWeaponSlotClass:new(self.mainScroll,eWeaponSlotTag.LobbyMain,UserDataMng.haveMainWeaponList[i],self)
                table.insert(self.mainBtnList,weaponSlot)
                weaponSlot.mainBG:setPosition((weaponSlotSize/2+padding)+((weaponSlotSize+padding)*(i-1)),self.mainScroll:getContentSize().height/2)  
            end
        end
        self:updateMainSlotList()
        self.mainScroll:setVisible(true)
        self.titleLabel:setString("기본 무기")
        self:updateInfo(UserDataMng.currentMainWeapon)
    -- 서브 무기 스크롤
    elseif tag == eAttackCategory.SubWeapon then
        if #UserDataMng.haveSubWeaponList ~= #self.subBtnList then
            local subWeaponCount = #UserDataMng.haveSubWeaponList
            self.subScroll:setInnerContainerSize(cc.size(padding*(subWeaponCount+1)+(weaponSlotSize*subWeaponCount),150))
            for i = #self.subBtnList+1, subWeaponCount do
                local weaponSlot = BaseWeaponSlotClass:new(self.subScroll,eWeaponSlotTag.LobbySub,UserDataMng.haveSubWeaponList[i],self)
                table.insert(self.subBtnList,weaponSlot)
                weaponSlot.mainBG:setPosition((weaponSlotSize/2+padding)+((weaponSlotSize+padding)*(i-1)),self.subScroll:getContentSize().height/2)
            end
        end
        self:updateSubSlotList()
        self.subScroll:setVisible(true)
        self.titleLabel:setString("보조 무기")
        self:updateInfo(UserDataMng.currentSubWeapon)
    end
end


-- 현재 착용 중인 장비 표시 업데이트
function EquipmentInsideUIClass:updateMainSlotList()
    for i = 1, #self.mainBtnList do
        self.mainBtnList[i]:updateSlot()
    end
end

-- 현재 착용 중인 장비 표시 업데이트
function EquipmentInsideUIClass:updateSubSlotList()
    for i = 1, #self.subBtnList do
        self.subBtnList[i]:updateSlot()
    end
end

-- 무기 능력치 텍스트 갱신
function EquipmentInsideUIClass:updateInfo(currentWeaponID)
    local weaponInfo = weaponTb[currentWeaponID]
    self.weaponNameText:setString(weaponInfo.weaponName)
    self.weaponIconSprite:setTexture(weaponInfo.iconName)
    self.descriptionText:setString(weaponInfo.description)
    self.stateText:setString(string.format("데미지 : %d\n캐스팅 타임 : %0.1f초\n폭발 범위 : %d",weaponInfo.damage,weaponInfo.attackDelay,weaponInfo.attackRange))
end

-- backBtn을 상단바에 등록하는 함수 ( LobbyScene에서 호출 ) [ 가장 상단에 표시되게 하기 위함 ]
function EquipmentInsideUIClass:enrollBackBtn(parent)
    parent:addChild(self.backBtn)
    self.backBtn:setAnchorPoint(0,0.5)
    self.backBtn:setPosition(20,parent:getContentSize().height/2+5)
    self.backBtn:setVisible(false)
end