require "script.UI.Lobby.EquipmentInsideUI"

LobbyEquipment = {
    mainLayer = nil, 
    tag = eLobbyLayerID.Equipment, 
    currentMainWeapon = nil,    -- 메인 무기 스프라이트
    currentSubWeapon  = nil,    -- 보조 무기 스프라이트
    characterSprite   = nil,    -- 캐릭터 스프라이트
    characterAction   = nil, 
    equipmentInsideUI = nil,    -- 내부 UI
    titleLabel        = nil,    -- 타이틀 텍스트
    isUsingInside     = false   -- 내부 클릭 중인지 플래그 ( 백 버튼 클릭 시 돌아 오기 위함 )
}

function LobbyEquipment:new(parentLayer,titleLabel,o)
    o = o or {}
    setmetatable(o,self)
    self.__index = self
    o:createEquipmentLayer(parentLayer,titleLabel)
    return o
end

-- 장비 레이어 세팅 함수
function LobbyEquipment:createEquipmentLayer(parentLayer,titleLabel)
    self.mainLayer  = cc.LayerColor:create(cc.c4b(0,0,0,160))
    self.titleLabel = titleLabel

    -- 버튼누르면 들어가는 내부 레이어
    self.equipmentInsideUI = EquipmentInsideUIClass:new(parentLayer,self.titleLabel,self)


    local halfBorder = cc.Sprite:create("LinearFrame.png")
    self.mainLayer:addChild(halfBorder)
    halfBorder:setScaleX(2)
    halfBorder:setPosition(visibleSize.width/2,visibleSize.height/2+150)

    local currentMainSlot = cc.Sprite:create("CurrentWeaponSlot.png")
    self.mainLayer:addChild(currentMainSlot)
    currentMainSlot:setScale(1.5)
    currentMainSlot:setPosition(visibleSize.width/2-230,visibleSize.height/2+320)
    
    local slotSize = currentMainSlot:getContentSize()

    self.currentMainWeapon = cc.Sprite:create()
    currentMainSlot:addChild(self.currentMainWeapon)
    self.currentMainWeapon:setScale(0.8)
    self.currentMainWeapon:setPosition(slotSize.width/2,slotSize.height/2)

    local currentSubSlot = cc.Sprite:create("CurrentWeaponSlot.png")
    self.mainLayer:addChild(currentSubSlot)
    currentSubSlot:setScale(1.5)
    currentSubSlot:setPosition(visibleSize.width/2+230,visibleSize.height/2+320)

    self.currentSubWeapon = cc.Sprite:create()
    currentSubSlot:addChild(self.currentSubWeapon)
    self.currentSubWeapon:setScale(0.8)
    self.currentSubWeapon:setPosition(slotSize.width/2,slotSize.height/2)

    local characterFrame = cc.Sprite:create("circleFrame.png")
    self.mainLayer:addChild(characterFrame)
    characterFrame:setPosition(visibleSize.width/2,visibleSize.height/2+320)

    self.characterSprite = cc.Sprite:create("playerIdle_0.png")
    self.characterSprite:setScale(2)
    self.mainLayer:addChild(self.characterSprite)
    self.characterSprite:setPosition(visibleSize.width/2,visibleSize.height/2+320)
    self.characterAction = createAnimation({delay = 0.4, path = "playerIdle_", totalIndex = 8},true)
    self.characterAction:retain()

    -- 주 무기 변경 버튼
    local mainWeaponBtn = ccui.Button:create("menuBG.png","menuBG.png")
    self.mainLayer:addChild(mainWeaponBtn)
    mainWeaponBtn:setPosition(visibleSize.width/2,visibleSize.height/2-50)

    local btnSize = mainWeaponBtn:getContentSize()

    local mainWeaponTitle = cc.Label:createWithTTF("기본 무기 변경","BMFont.ttf",30)
    mainWeaponTitle:setColor(cc.c3b(11,0,0))
    mainWeaponBtn:addChild(mainWeaponTitle)
    mainWeaponTitle:setPosition(btnSize.width/2,btnSize.height/2)
    mainWeaponBtn:addTouchEventListener(
        function (sender,type)
            if type == ccui.TouchEventType.began then
                mainWeaponBtn:setColor(cc.c3b(150,150,150))
                SoundMng:playEffect(eEffectSoundID.BtnEffect)
            elseif type == ccui.TouchEventType.canceled then
                mainWeaponBtn:setColor(cc.c3b(255,255,255))
            elseif type == ccui.TouchEventType.ended then
                mainWeaponBtn:setColor(cc.c3b(255,255,255))
                self.equipmentInsideUI:visualizeUI(eAttackCategory.MainWeapon)
                self.isUsingInside = true
            end
        end
    )

    -- 보조 무기 변경 버튼
    local subWeaponBtn = ccui.Button:create("menuBG.png","menuBG.png")
    self.mainLayer:addChild(subWeaponBtn)
    subWeaponBtn:setPosition(visibleSize.width/2,visibleSize.height/2 - 200)

    local subWeaponTitle = cc.Label:createWithTTF("보조 무기 변경","BMFont.ttf",30)
    subWeaponTitle:setColor(cc.c3b(11,0,0))
    subWeaponBtn:addChild(subWeaponTitle)
    subWeaponTitle:setPosition(btnSize.width/2,btnSize.height/2)
    subWeaponBtn:addTouchEventListener(
        function (sender,type)
            if type == ccui.TouchEventType.began then
                subWeaponBtn:setColor(cc.c3b(150,150,150))
                SoundMng:playEffect(eEffectSoundID.BtnEffect)
            elseif type == ccui.TouchEventType.canceled then
                subWeaponBtn:setColor(cc.c3b(255,255,255))
            elseif type == ccui.TouchEventType.ended then
                subWeaponBtn:setColor(cc.c3b(255,255,255))
                self.equipmentInsideUI:visualizeUI(eAttackCategory.SubWeapon)
                self.isUsingInside = true
            end
        end
    )

    self:nonVisualizeUI()
    parentLayer:addChild(self.mainLayer)
end

function LobbyEquipment:visualizeUI()
    self.titleLabel:setString("장비")
    self.characterSprite:runAction(self.characterAction)
    self.currentMainWeapon:setTexture(weaponTb[UserDataMng.currentMainWeapon].iconName)
    self.currentSubWeapon:setTexture(weaponTb[UserDataMng.currentSubWeapon].iconName)
    self.mainLayer:setVisible(true)
end

function LobbyEquipment:nonVisualizeUI()
    self.characterSprite:stopAllActions()
    self.mainLayer:setVisible(false)
end