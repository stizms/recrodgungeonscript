require "script.UI.Lobby.StageSlot"

LobbyHome = {
    parentLayer  = nil,     -- LobbyScene의 메인 레이어
    mainLayer    = nil, 
    subLayer     = nil,
    tag          = eLobbyLayerID.Home, 
    stageScroll  = nil, 
    stageBtnList = nil
}

-- 생성 함수
function LobbyHome:new(parentLayer,o)
    o = o or {}
    setmetatable(o,self)
    self.__index = self
    o:createHomeLayer(parentLayer)
    return o
end

--홈 레이어 생성
function LobbyHome:createHomeLayer(parentLayer)
    self.parentLayer  = parentLayer
    self.mainLayer    = cc.LayerColor:create(cc.c4b(0,0,0,160))
    self.subLayer     = cc.Layer:create()
    self.stageBtnList = {}

    self.stageScroll  = ccui.ScrollView:create()
    self.stageScroll:setDirection(ccui.ScrollViewDir.vertical)
    self.stageScroll:setAnchorPoint(0.5,0.5)
    self.stageScroll:setContentSize(visibleSize.width,visibleSize.height-328)
    self.stageScroll:setBounceEnabled(true)
    self.stageScroll:setScrollBarEnabled(false)
    self.stageScroll:setPosition(visibleSize.width/2,visibleSize.height/2+4)
    self.subLayer:addChild(self.stageScroll)
    
    self:nonVisualizeUI()

    self.mainLayer:addChild(self.subLayer)
    self.parentLayer:addChild(self.mainLayer)
end

-- 스테이지 슬롯 생성 함수
function LobbyHome:setStageSlotData()
    local stageSlotSize = 110
    local padding       = 20

    -- 스테이지의 수가 스테이지 버튼리스트의 수와 다를 때만 생성 & 동적으로 스크롤 범위 넓힘
    if #stageTb ~= #self.stageBtnList then
        local stageCount = #stageTb
        self.stageScroll:setInnerContainerSize(cc.size(visibleSize.width,padding * (stageCount +1)+(stageSlotSize * stageCount)))
        for i = #self.stageBtnList+1, stageCount do
            local stageSlot = StageSlotClass:new(self.stageScroll,stageTb[i],self)
            table.insert(self.stageBtnList,stageSlot)
            stageSlot.mainBG:setPosition(visibleSize.width/2,self.stageScroll:getInnerContainerSize().height - 80 -((stageSlotSize+padding)*(i-1)))
        end
    end
end

-- 가시화 함수
function LobbyHome:visualizeUI()
    self:setStageSlotData()
    self.mainLayer:setVisible(true)
end

-- 비가시화 함수
function LobbyHome:nonVisualizeUI()
    self.mainLayer:setVisible(false)
end