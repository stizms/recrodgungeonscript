LobbySetting = {
    mainLayer = nil,
    subLayer = nil,
    tag = eLobbyLayerID.Setting
}

-- 생성함수
function LobbySetting:new(parentLayer,o)
    o = o or {}
    setmetatable(o,self)
    self.__index = self
    o:createSettingLayer(parentLayer)
    return o
end

-- 설정 레이어 세팅 함수
function LobbySetting:createSettingLayer(parentLayer)
    self.mainLayer = cc.LayerColor:create(cc.c4b(0,0,0,160))
    
    self.subLayer = cc.Layer:create()
    self.mainLayer:addChild(self.subLayer)

    -- 구성요소 세팅 부
    local upBorder = cc.Sprite:create("LinearFrame.png")
    upBorder:setPosition(visibleSize.width/2,visibleSize.height/2+300)
    upBorder:setScale(2)
    self.subLayer:addChild(upBorder)

    local downBorder = cc.Sprite:create("LinearFrame.png")
    downBorder:setPosition(visibleSize.width/2,visibleSize.height/2-300)
    downBorder:setScale(2)
    self.subLayer:addChild(downBorder)

    local BGMTitleBackGround = cc.Sprite:create("clearInfoFrame.png")
    BGMTitleBackGround:setAnchorPoint(0,0.5)
    BGMTitleBackGround:setScaleY(1.3)
    BGMTitleBackGround:setPosition(0,visibleSize.height/2+200)
    self.subLayer:addChild(BGMTitleBackGround)

    local BGMTitle = cc.Label:createWithTTF("배경음 설정","BMFont.ttf",22)
    BGMTitleBackGround:addChild(BGMTitle)
    BGMTitle:setAnchorPoint(0,0.5)
    BGMTitle:setColor(cc.c3b(35,22,14))
    BGMTitle:setPosition(50,18)

    local BGMSlider = ccui.Slider:create()
    BGMSlider:loadBarTexture("sliderBackground.png")
    BGMSlider:loadSlidBallTextureNormal("sliderHandle.png")
    BGMSlider:loadProgressBarTexture("sliderPrograss.png")
    BGMSlider:setPercent(SoundMng.currentBGMVolume)
    BGMSlider:setScale(2)
    BGMSlider:setPosition(visibleSize.width/2,visibleSize.height/2+75)
    BGMSlider:addEventListener(
        function (sender,type)
            if type == ccui.SliderEventType.percentChanged  then
                local percent = sender:getPercent()
                SoundMng.currentBGMVolume = percent
                percent = percent/100
                SoundMng.sound:setMusicVolume(percent)
            end
        end
    )
    self.subLayer:addChild(BGMSlider)
    
    local effectTitleBackGround = cc.Sprite:create("clearInfoFrame.png")
    effectTitleBackGround:setAnchorPoint(0,0.5)
    effectTitleBackGround:setScaleY(1.3)
    effectTitleBackGround:setPosition(0,visibleSize.height/2-50)
    self.subLayer:addChild(effectTitleBackGround)

    local effectTitle = cc.Label:createWithTTF("효과음 설정","BMFont.ttf",22)
    effectTitleBackGround:addChild(effectTitle)
    effectTitle:setAnchorPoint(0,0.5)
    effectTitle:setColor(cc.c3b(35,22,14))
    effectTitle:setPosition(50,18)

    local effectSlider = ccui.Slider:create()
    effectSlider:loadBarTexture("sliderBackground.png")
    effectSlider:loadSlidBallTextureNormal("sliderHandle.png")
    effectSlider:loadProgressBarTexture("sliderPrograss.png")
    effectSlider:setPercent(SoundMng.currentEffectVolume)
    effectSlider:setScale(2)
    effectSlider:setPosition(visibleSize.width/2,visibleSize.height/2-175)
    effectSlider:addEventListener(
        function (sender,type)
            if type == ccui.SliderEventType.percentChanged  then
                local percent = sender:getPercent()
                SoundMng.currentEffectVolume = percent
                percent = percent/100
                SoundMng.sound:setEffectsVolume(percent)
            end
        end
    )
    self.subLayer:addChild(effectSlider)
    
    
    self:nonVisualizeUI()
    parentLayer:addChild(self.mainLayer)
end

function LobbySetting:visualizeUI()
    self.mainLayer:setVisible(true)
end

function LobbySetting:nonVisualizeUI()
    self.mainLayer:setVisible(false)
end