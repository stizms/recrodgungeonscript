LobbyShop = {
    mainLayer   = nil,
    tag = eLobbyLayerID.Shop
}
 
-- 생성 함수
function LobbyShop:new(parentLayer,o)
    o = o or {}
    setmetatable(o,self)
    self.__index = self
    o:createShopLayer(parentLayer)
    return o
end

-- 레이어 세팅 함수
function LobbyShop:createShopLayer(parentLayer)
    self.mainLayer   = cc.LayerColor:create(cc.c4b(0,0,0,160))

    local tempBG = cc.Sprite:create("PopUp.png")
    tempBG:setScale(1.6)
    tempBG:setPosition(visibleSize.width/2,visibleSize.height/2)
    self.mainLayer:addChild(tempBG)

    local size = tempBG:getContentSize()
    local text = cc.Label:createWithTTF("상점은 준비중입니다.","BMFont.ttf",20,cc.size(size.width,size.height))
    text:setAlignment(cc.TEXT_ALIGNMENT_CENTER)
    text:setColor(cc.c3b(255,255,255))
    tempBG:addChild(text)
    text:setPosition(size.width/2,0)

    self:nonVisualizeUI()
    parentLayer:addChild(self.mainLayer)
end

-- 가시화 함수
function LobbyShop:visualizeUI()
    self.mainLayer:setVisible(true)
end

-- 비가시화 함수
function LobbyShop:nonVisualizeUI()
    self.mainLayer:setVisible(false)
end