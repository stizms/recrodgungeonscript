StageSlotClass = {
    parent        = nil, 
    mainBG        = nil, 
    stageIDLayer  = nil,    -- 스테이지 ID만 나오는 레이어
    infoLayer     = nil,    -- 스테이지 정보만 나오는 레이어
    disabledLayer = nil,    -- 불가능 한 곳일 경우 가림막 ( 검은 색 )
    isAccessible  = false,  -- 접근 가능한 스테이지인지 Flag
    stageInfo     = nil     -- stageTb에서 캐싱하는 변수
}

-- 슬롯 생성 함수
function StageSlotClass:new(scrollView,stageInfo,parent,o)
    o = o or {}
    setmetatable(o,self)
    self.__index = self
    o:init(scrollView,stageInfo,parent)
    return o
end

-- 슬롯 초기화 함수
function StageSlotClass:init(scrollView,stageInfo,parent)
    self.parent    = parent
    self.stageInfo = stageInfo

    -- 갈 수 있는 스테이지인지 체크
    if UserDataMng:isAccessibleStage(self.stageInfo.stageID) == true then
        self.isAccessible = true
        self.mainBG = ccui.Button:create("StageSlot.png","StageSlot.png")
    else
        self.isAccessible = false
        self.mainBG = ccui.Button:create("unAccessibleStageSlot.png","unAccessibleStageSlot.png")
    end

    self.mainBG:addTouchEventListener(
        function (sender,type)
            if popUpUI.isVisible == false then
                if type == ccui.TouchEventType.began then
                    self.mainBG:setScale(0.95)
                    self.mainBG:setColor(cc.c3b(150,150,150))
                elseif type == ccui.TouchEventType.canceled then
                    self.mainBG:setScale(1)
                    self.mainBG:setColor(cc.c3b(255,255,255))
                elseif type == ccui.TouchEventType.ended then
                    self.mainBG:setScale(1)
                    self.mainBG:setColor(cc.c3b(255,255,255))
                    if self.isAccessible == true then
                        popUpUI:setQuestionPopUp(self.parent.parentLayer,"스테이지를 진행하시겠습니까?",function () SceneMng:changeScene(self.stageInfo.scene) end,nil,self.parent.stageScroll)
                    else
                        popUpUI:setNotiPopUp(self.parent.parentLayer,"이전 스테이지를 클리어한 후에 진입할 수 있습니다.",nil,self.parent.stageScroll)
                    end
                end
            end
        end
    )

    self.infoLayer = cc.Layer:create()
    self.mainBG:addChild(self.infoLayer)
    local size = self.mainBG:getContentSize()

    local mapSprite = cc.Sprite:create(self.stageInfo.mapSprite)
    mapSprite:setColor(cc.c4b(150,150,150,160))
    mapSprite:setAnchorPoint(0,0)
    mapSprite:setScaleX(2.7)
    mapSprite:setScaleY(1.3)
    mapSprite:setPosition(size.width/2+70,8)
    self.infoLayer:addChild(mapSprite)

    
    local stageTitle = cc.Label:createWithTTF(self.stageInfo.stageName,"BMFont.ttf",40)
    stageTitle:setAnchorPoint(0,0.5)
    stageTitle:setColor(cc.c3b(217,217,217))
    stageTitle:setPosition(150,30)
    self.infoLayer:addChild(stageTitle)

    local monsterIcon = cc.Sprite:create(monsterTb[self.stageInfo.monsterID].monsterIcon)
    monsterIcon:setAnchorPoint(1,0)
    self.infoLayer:addChild(monsterIcon)
    monsterIcon:setPosition(size.width-6,5)

    self.disabledLayer = cc.LayerColor:create(cc.c4b(10,10,10,200))
    self.mainBG:addChild(self.disabledLayer)
    self.disabledLayer:setContentSize(size.width-12,size.height-10)
    self.disabledLayer:setPosition(6,4)

    if self.isAccessible == true then
        self.disabledLayer:setVisible(false)
    else
        self.disabledLayer:setVisible(true) 
    end
    
    self.stageIDLayer = cc.Layer:create()
    self.mainBG:addChild(self.stageIDLayer)

    local stageLabel = cc.Label:createWithTTF(self.stageInfo.stageID,"RetroFont2.ttf",60)
    stageLabel:setAnchorPoint(0,0.5)
    stageLabel:setColor(cc.c3b(225,194,9))
    stageLabel:setPosition(15,70)
    self.stageIDLayer:addChild(stageLabel)

    scrollView:addChild(self.mainBG)
end