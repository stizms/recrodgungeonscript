popUpUI = { 
    mainLayer = nil, 
    currentParentLayer = nil, 

    backGround  = nil ,
    text        = nil, 
    yesBtn      = nil, 
    noBtn       = nil, 
    yesBtnFunc  = nil, 
    noBtnFunc   = nil, 
    isVisible   = false 
}

function popUpUI:init()
    -- 맨 아래의 검은 막 레이어
    self.mainLayer = cc.LayerColor:create(cc.c4b(10,10,10,160))
    self.mainLayer:retain()
    self.mainLayer:setVisible(false)

    -- 실질 컨텐츠 레이어
    local subLayer = cc.Layer:create()
    subLayer:retain()
    self.mainLayer:addChild(subLayer)
    
    self.backGround = cc.Sprite:create("PopUp.png")
    self.backGround:setPosition(visibleSize.width/2,visibleSize.height/2)
    self.backGround:setScale(1.6)
    self.backGround:retain()
    subLayer:addChild(self.backGround)
    local backgroundSize = self.backGround:getContentSize()

    self.text = cc.Label:createWithTTF("","BMFont.ttf",20,cc.size(backgroundSize.width,backgroundSize.height))
    self.text:setAlignment(cc.TEXT_ALIGNMENT_CENTER)
    self.text:setColor(cc.c3b(255,255,255))
    self.backGround:addChild(self.text)
    self.text:setPosition(backgroundSize.width/2,40)
    self.text:retain()
    
    self.yesBtn = ccui.Button:create("OkayBtn.png","OkayBtn.png")
    self.yesBtn:addTouchEventListener(
        function (sender,type)
            if type == ccui.TouchEventType.began then
                self.yesBtn:setColor(cc.c3b(150,150,150))
                SoundMng:playEffect(eEffectSoundID.BtnEffect)
            elseif type == ccui.TouchEventType.canceled then
                self.yesBtn:setColor(cc.c3b(255,255,255))
            elseif type == ccui.TouchEventType.ended then
                self.yesBtn:setColor(cc.c3b(255,255,255))
                if self.yesBtnFunc ~= nil then
                    self.yesBtnFunc()
                end
                self:nonVisualizePopUp()
            end
        end
    )
    self.yesBtn:retain()
    self.backGround:addChild(self.yesBtn)
    self.yesBtn:setPosition(backgroundSize.width/2-50,100)
    self.yesBtn:setVisible(false)

    self.noBtn = ccui.Button:create("ExitBtn.png","ExitBtn.png")
    self.noBtn:addTouchEventListener(
        function (sender,type)
            if type == ccui.TouchEventType.began then
                self.noBtn:setColor(cc.c3b(150,150,150))
                SoundMng:playEffect(eEffectSoundID.BtnEffect)
            elseif type == ccui.TouchEventType.canceled then
                self.noBtn:setColor(cc.c3b(255,255,255))
            elseif type == ccui.TouchEventType.ended then
                self.noBtn:setColor(cc.c3b(255,255,255))
                if self.noBtnFunc ~= nil then
                    self.noBtnFunc()
                end
                self:nonVisualizePopUp()
            end
        end
    )
    self.noBtn:retain()
    self.backGround:addChild(self.noBtn)
    self.noBtn:setPosition(backgroundSize.width/2+50,100)
    self.noBtn:setVisible(false)
end

-- 알림 전용 팝업 세팅자 (scrollView는 호출 할 씬에 scrollView가 있는 경우 터치를 막기 위해 존재)
function popUpUI:setNotiPopUp(layer,mainText,btnFunc,scrollView)
    -- 어느씬에서도 사용가능하도록 레이어를 매개변수로 받고 적용함.
    self.currentParentLayer = layer
    self.text:setString(mainText)
    self.noBtnFunc = btnFunc
    self.isVisible = true
    self.mainLayer:setVisible(true)
    if scrollView ~= nil then
        self.scrollView = scrollView
        self.scrollView:setTouchEnabled(false)
    end
    self.yesBtn:setVisible(false)
    self.noBtn:setPosition(self.backGround:getContentSize().width/2,100)
    self.noBtn:setVisible(true)
    SoundMng:playEffect(eEffectSoundID.PopUpVisualize)
    self.currentParentLayer:addChild(self.mainLayer)
end

-- 질문 전용 팝업 세팅자 (scrollView는 호출 할 씬에 scrollView가 있는 경우 터치를 막기 위해 존재)
function popUpUI:setQuestionPopUp(layer,mainText,yesBtnFunc,noBtnFunc,scrollView)
    self.currentParentLayer = layer
    self.text:setString(mainText)
    self.yesBtnFunc = yesBtnFunc
    self.noBtnFunc  = noBtnFunc
    if scrollView ~= nil then
        scrollView = scrollView
        scrollView:setTouchEnabled(false)
    end
    self.isVisible = true
    self.mainLayer:setVisible(true)
    self.yesBtn:setVisible(true)
    self.noBtn:setPosition(self.backGround:getContentSize().width/2+50,100)
    self.noBtn:setVisible(true)
    SoundMng:playEffect(eEffectSoundID.PopUpVisualize)
    self.currentParentLayer:addChild(self.mainLayer)
end

-- 팝업 제거
function popUpUI:nonVisualizePopUp()
    self.isVisible = false
    self.yesBtnFunc = nil
    self.noBtnFunc = nil
    if self.scrollView ~= nil then
        self.scrollView:setTouchEnabled(true)
    end
    self.yesBtn:setVisible(false)
    self.noBtn:setVisible(false)
    self.mainLayer:setVisible(false)
    SoundMng:playEffect(eEffectSoundID.PopUpNonVisualize)
    if self.currentParentLayer ~= nil then
        self.currentParentLayer:removeChild(self.mainLayer)
        self.currentParentLayer = nil
    end
end