BaseMainWeaponClass = BaseWeaponClass:new({lifeTime = 0, bombDelay = nil})
-- bombDelay 폭발까지 시간
-- deltaTime을 담는 변수
function BaseMainWeaponClass:update(dt)
    self.lifeTime = self.lifeTime + dt
    if self.lifeTime > self.bombDelay then
        local cX,cY = self.sprite:getPosition()
        self.lifeTime = 0
        self:removeWeapon(cX,cY)
    end
end