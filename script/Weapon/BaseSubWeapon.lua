BaseSubWeaponClass = BaseWeaponClass:new({moveSpeed = 0, weight = 0, maxBullet= nil})
-- weight은 차징시 느려지게하는 변수
-- maxBullet은 최대 탄환 개수
function BaseSubWeaponClass:update(dt)
    local cX,cY = self.sprite:getPosition()
    local nextPosY = cY + (self.moveSpeed * dt)
    self.sprite:setPosition(cX,nextPosY)

    local pivotY = nextPosY+self.sprite:getContentSize().width/2
    if collisionCheckToMonster(cX,pivotY) == true then
        self:removeWeapon(cX,pivotY)
        if self.weaponID == eWeaponID.IceBall then
            StageMng.boss:setIce()
        end
        do return end
    end

    if nextPosY+self.sprite:getContentSize().width/2 >= StageMng.map.maxY then
        self:removeWeapon(cX,pivotY)
    end
end