BaseWeaponClass = {
    sprite          = nil, 
    weaponID        = nil,  -- 무기 ID
    tag             = nil,  -- 무기 종류 eAttackCategory ( MainAttack, SubAttack)
    attackDelay     = nil, 
    damage          = nil,  -- 무기 공격력
    attackRange     = nil,  -- 폭발 범위
    weaponIconPath  = nil,  -- 아이콘 경로 (이름)
    bodyAction      = nil,  
    animInfo        = nil,  -- 애니메이션 정보
    particleTag     = nil,  -- 파티클 종류 eParticleTag ( MainAttack, SubAttack )
    particleInfo    = nil,  -- 파티클 애니메이션 정보
    bombSound       = nil,
    bombAmplitude   = 0     -- 진동크기
}

-- 생성자
function BaseWeaponClass:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self
    o:init()
    return o
end

-- 초기화
function BaseWeaponClass:init()
    print("Weapon Parent")
end

-- 애니메이션 등록 함수
function BaseWeaponClass:enrollAnimation()
    if self.animInfo == nil then
        print("Animation Information is not setting")
        return
    else
        self.bodyAction = createAnimation(self.animInfo,false)
        self.bodyAction:retain()
    end
end

-- 폭발시 공격 체크
function BaseWeaponClass:explosionAttack(cX,cY)
    if self.attackRange ~= nil then
        local explosionRect = cc.rect(cX-self.attackRange.width/2,cY-self.attackRange.height/2,self.attackRange.width,self.attackRange.height)
        local isAttack = false
        -- 플레이어가 죽어있다면 플레이어와 충돌처리하지 않음
        if StageMng.player.state ~= ePlayerState.Death then
            StageMng.player:isDamaged(explosionRect)
        end
        -- 몬스터가 죽어있다면 몬스터와 충돌처리 하지 않음
        for i,v in pairs(MonsterMng.usingMonsterPool) do
            if v.state ~= eMonsterState.Death then
                if v:isDamaged(explosionRect,self.damage) == true then
                    isAttack = true
                end
            end
        end
        -- 몬스터와 피격되면 더욱 큰 진동 , 그렇지 않다면 그냥 진동
        if isAttack == true then
            StageMng:shake(self.bombAmplitude*1.5,0.5)
        else
            StageMng:shake(self.bombAmplitude,0.5)
        end
    end
end

-- 맵 충돌 콜라이더 라인 드로우 함수
function BaseWeaponClass:drawExplosionRange(layer,cX,cY)
    local rect = cc.rect(cX-self.attackRange.width/2,cY-self.attackRange.height/2,self.attackRange.width,self.attackRange.height)

    local leftDebugLine = cc.DrawNode:create()
    leftDebugLine:drawLine({x = rect.x,y = rect.y},{x = rect.x, y = rect.y + rect.height},cc.c4f(255/255,0/255,0/255,1))
    layer:addChild(leftDebugLine)

    local rightDebugLine = cc.DrawNode:create()
    rightDebugLine:drawLine({x = rect.x + rect.width, y = rect.y},{x = rect.x + rect.width, y = rect.y + rect.height},cc.c4f(255/255,0/255,0/255,1))
    layer:addChild(rightDebugLine)
 
    local upDebugLine = cc.DrawNode:create()
    upDebugLine:drawLine({x = rect.x, y = rect.y + rect.height},{x = rect.x + rect.width,y = rect.y + rect.height},cc.c4f(255/255,0/255,0/255,1))
    layer:addChild(upDebugLine)

    local downDebugLine = cc.DrawNode:create()
    downDebugLine:drawLine({x = rect.x,y = rect.y},{x = rect.x + rect.width,y = rect.y},cc.c4f(255/255,0/255,0/255,1))
    layer:addChild(downDebugLine)
end

-- 무기 제거 ( 풀로 돌려보내기 )
function BaseWeaponClass:removeWeapon(currentX,currentY)
    self:explosionAttack(currentX,currentY)
    SoundMng:playEffect(self.bombSound)
    self.sprite:setVisible(false)
    self.sprite:stopAction(self.bodyAction);
    table.remove(WeaponMng.usingWeaponPool,tableFind(WeaponMng.usingWeaponPool,self))
    ParticleMng:getParticle(self.particleTag,currentX,currentY)
    --self:drawExplosionRange(StageMng.layer,currentX,currentY)
end