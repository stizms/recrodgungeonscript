DynamiteStickClass = BaseMainWeaponClass:new()

function DynamiteStickClass:init()
    self.sprite = cc.Sprite:create("dynamiteStick_0.png")
    self.sprite:retain()
    self.weaponID       = eWeaponID.DynamiteStick
    self.tag            = eAttackCategory.MainWeapon
    local weaponInfo    = weaponTb[self.weaponID]
    self.bombAmplitude  = weaponInfo.bombAmplitude
    self.attackDelay    = weaponInfo.attackDelay
    self.bombDelay      = weaponInfo.bombDelay
    self.damage         = weaponInfo.damage
    self.bombSound      = eEffectSoundID.DynamiteBombEffect
    self.attackRange    = {width = weaponInfo.attackRange, height = weaponInfo.attackRange}
    self.animInfo       = { delay = 0.4, path = "dynamiteStick_", totalIndex = 5} -- delay는 bombDelay / totalIndex
    self.particleTag    = eParticleTag.MainWeapon
    self.particleInfo   = { delay = 0.015, particleDelay = 1, path = "dynamiteStickParticle_", totalIndex = 71 } -- delay는 particleDelay / totalIndex
    self:enrollAnimation()
end