IceBallClass = BaseSubWeaponClass:new()

function IceBallClass:init()
    self.sprite = cc.Sprite:create("IceBall_0.png")
    self.sprite:setRotation(90)
    self.sprite:retain()
    self.weaponID       = eWeaponID.IceBall
    local weaponInfo    = weaponTb[self.weaponID]
    self.tag            = eAttackCategory.SubWeapon
    self.maxBullet      = weaponInfo.maxBullet
    self.bombSound      = eEffectSoundID.IceBallEffect
    self.bombAmplitude  = weaponInfo.bombAmplitude
    self.attackDelay    = weaponInfo.attackDelay
    self.damage         = weaponInfo.damage
    self.weight         = weaponInfo.weight
    self.moveSpeed      = weaponInfo.moveSpeed
    self.attackRange    = {width = weaponInfo.attackRange, height = weaponInfo.attackRange}
    self.animInfo       = { delay = 0.15, path = "IceBall_", totalIndex = 12}
    self.particleTag    = eParticleTag.SubWeapon
    self.particleInfo   = { delay = 0.03, particleDelay = 1, path = "IceBallParticle_", totalIndex = 33 }
    self:enrollAnimation()
end

function IceBallClass:enrollAnimation()
    if self.animInfo == nil then
        print("Animation Information is not setting")
        return
    else
        self.bodyAction = createAnimation(self.animInfo,true)
        self.bodyAction:retain()
    end
end