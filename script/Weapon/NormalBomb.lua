NormalBombClass = BaseMainWeaponClass:new()

function NormalBombClass:init()
    self.sprite = cc.Sprite:create("normalBomb_0.png")
    self.sprite:retain()
    self.weaponID       = eWeaponID.NormalBomb
    local weaponInfo    = weaponTb[self.weaponID]
    self.tag            = eAttackCategory.MainWeapon
    self.bombAmplitude  = weaponInfo.bombAmplitude
    self.attackDelay    = weaponInfo.attackDelay
    self.bombSound      = eEffectSoundID.NormalBombEffect
    self.bombDelay      = weaponInfo.bombDelay
    self.damage         = weaponInfo.damage
    self.attackRange    = {width = weaponInfo.attackRange, height = weaponInfo.attackRange}
    self.animInfo       = { delay = 0.66, path = "normalBomb_", totalIndex = 3} -- delay는 bombDelay / totalIndex
    self.particleTag    = eParticleTag.MainWeapon
    self.particleInfo   = { delay = 0.015, particleDelay = 1, path = "normalBombParticle_", totalIndex = 65 } -- delay는 particleDelay / totalIndex
    self:enrollAnimation()
end